package plug.verifiers.predicates;

public class PredicatesVerifiedEvent {

	private PredicateVerifier<?> source;
	public PredicatesVerifiedEvent(PredicateVerifier<?> source) {
		this.setSource(source);
	}
	public PredicateVerifier<?> getSource() {
		return source;
	}
	public void setSource(PredicateVerifier<?> source) {
		this.source = source;
	}

}
