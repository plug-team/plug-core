package plug.verifiers.predicates;

import announce4j.Announcer;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import plug.events.ExecutionEndedEvent;
import plug.events.ExecutionStartedEvent;
import plug.events.FiredEvent;


public class PredicateVerifier<C> {
	public Announcer announcer = new Announcer(true);
	public List<Predicate<C>> predicates = new LinkedList<Predicate<C>>();
	public Map<Predicate<C>, C> violations = new IdentityHashMap<>();

	public PredicateVerifier(Announcer announcer) {
		announcer.when(ExecutionStartedEvent.class, this::onExecutionStartedEvent);
		announcer.when(ExecutionEndedEvent.class, this::onExecutionEndedEvent);
		announcer.when(FiredEvent.class, this::onFiredEvent);
	}

	protected void onExecutionStartedEvent(Announcer ann, ExecutionStartedEvent<C> event) {
		for (C configuration : event.getInitialConfigurations()) {
			verify(configuration);
		}
	}

	protected void onExecutionEndedEvent(Announcer ann, ExecutionEndedEvent event) {
		if (violations.isEmpty()) {
			announcer.announce(new PredicatesVerifiedEvent(this));
		} else {
			announcer.announce(new PredicatesViolatedEvent(this));
		}
	}

	protected void onFiredEvent(Announcer ann, FiredEvent<C, ?> event) {
		verifyAll(event.getFiredTransition().getTargets());
	}

	protected void verifyAll(Collection<C> configurations) {
		for (C configuration : configurations) {
			verify(configuration);
		}
	}

	protected void verify(C configuration) {
		for (int index = 0; index < predicates.size(); index++) {
			if (!verify(predicates.get(index), configuration)) {
				Predicate<C> violated = predicates.remove(index);
				violations.put(violated, configuration);

				announcer.announce(new PredicateViolationEvent<C>(this, violated, configuration));
			}
		}
	}

	protected boolean verify(Predicate<C> predicate, C configuration) {
		return predicate.test(configuration);
	}

}
