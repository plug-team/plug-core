package plug.verifiers.predicates;

import java.util.function.Predicate;

import plug.core.IConfiguration;
import plug.verifiers.predicates.PredicateVerifier;

public class PredicateViolationEvent<C> {
	private PredicateVerifier<C> source;
	Predicate<C> violated;
	private C configuration;
	
	public PredicateViolationEvent(PredicateVerifier<C> source, Predicate<C> violated, C configuration) {
		this.source = source;
		this.violated = violated;
		this.configuration = configuration;
		
	}

	public Predicate<C> getViolated() {
		return violated;
	}

	public C getConfiguration() {
		return configuration;
	}

	public PredicateVerifier<?> getSource() {
		return source;
	}
}
