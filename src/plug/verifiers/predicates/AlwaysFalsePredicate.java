package plug.verifiers.predicates;

import java.util.function.Predicate;

public class AlwaysFalsePredicate<T> implements Predicate<T> {

	@Override
	public boolean test(T t) {
		return false;
	}

	@Override
	public String toString() {
		return "predicate(FALSE)";
	}
	

}
