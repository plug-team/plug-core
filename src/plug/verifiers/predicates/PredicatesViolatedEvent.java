package plug.verifiers.predicates;

public class PredicatesViolatedEvent {
	private PredicateVerifier<?> source;
	public PredicatesViolatedEvent(PredicateVerifier<?> source) {
		this.setSource(source);
	}
	public PredicateVerifier<?> getSource() {
		return source;
	}
	public void setSource(PredicateVerifier<?> source) {
		this.source = source;
	}
}
