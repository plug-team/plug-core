package plug.verifiers.old_verifiers;

import plug.core.IFiredTransition;

/**
 * @author Ciprian Teodorov (ciprian.teodorov@ensta-bretagne.fr)
 *         Created on 14/03/16.
 */
public class DeadlockVerifier<C, A> implements IPropertyVerifier<C, A> {
    public boolean deadlockFree = true;
    @Override
    public C initialConfiguration(C initial) {
        return initial;
    }

    @Override
    public void verify(IFiredTransition<C, A> transition) {}

    @Override
    public void deadlock(C configuration) {
        deadlockFree = false;
    }
}
