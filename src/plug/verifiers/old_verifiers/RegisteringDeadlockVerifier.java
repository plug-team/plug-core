package plug.verifiers.old_verifiers;

import plug.core.IConfiguration;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Ciprian Teodorov (ciprian.teodorov@ensta-bretagne.fr)
 *         Created on 14/03/16.
 */
public class RegisteringDeadlockVerifier<C, A> extends DeadlockVerifier<C, A> {
    public Set<C> deadlockStates = new HashSet<>();

    @Override
    public void deadlock(C configuration) {
        super.deadlock(configuration);
        deadlockStates.add(configuration);
    }
}
