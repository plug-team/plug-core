package plug.verifiers.old_verifiers;

import plug.core.IFiredTransition;

public interface IPropertyVerifier<C, A> {
	C initialConfiguration(C initial);
	void verify(IFiredTransition<C, A> transition);
	void deadlock(C configuration);
}
