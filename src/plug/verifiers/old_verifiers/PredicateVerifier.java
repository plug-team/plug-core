package plug.verifiers.old_verifiers;

import plug.core.IFiredTransition;

import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class PredicateVerifier<C, A> implements IPropertyVerifier<C, A> {
	public List<Predicate<C>> predicates;
	public Map<Predicate<C>, C> violations = new IdentityHashMap<>();
	
	@Override
	public C initialConfiguration(C initial) {
		verify(initial);
		return initial;
	}

	/**
	 * we assert only previously true predicates.
	 * the others are already violated
	 * */
	@Override
	public void verify(IFiredTransition<C,A> transition) {
		for (int i = 0; i < transition.getTargets().size(); i++) {
			verify(transition.getTarget(i));
		}
	}

	private void verify(C configuration) {
		for (int index = 0; index < predicates.size(); index++) {
			if (!verify(predicates.get(index), configuration)) {
				Predicate violated = predicates.remove(index);
				violations.put(violated, configuration);
			}
		}
	}

	private boolean verify(Predicate<C> predicate, C configuration) {
		return predicate.test(configuration);
	}

	@Override
	public void deadlock(C configuration) {}
}
