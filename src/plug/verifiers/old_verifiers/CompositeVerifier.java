package plug.verifiers.old_verifiers;

import plug.core.IFiredTransition;

import java.util.ArrayList;
import java.util.List;

public class CompositeVerifier<C, A> implements IPropertyVerifier<C, A> {
	List<IPropertyVerifier<C, A>> verifiers = new ArrayList<>();
	
	public void addVerifier(IPropertyVerifier<C, A> verifier) {
		verifiers.add(verifier);
	}
	
	@Override
	public C initialConfiguration(C initial) {
		C configuration = initial;
		for (IPropertyVerifier<C, A> ver : verifiers) {
			configuration = ver.initialConfiguration(configuration);
		}
		return configuration;
	}

	@Override
	public void verify(IFiredTransition<C, A> transition) {
		for (IPropertyVerifier<C, A> ver : verifiers) {
			ver.verify(transition);
		}
	}

	@Override
	public void deadlock(C configuration) {
		for (IPropertyVerifier<C, A> ver : verifiers) {
			ver.deadlock(configuration);
		}
	}
}
