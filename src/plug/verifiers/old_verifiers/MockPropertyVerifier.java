package plug.verifiers.old_verifiers;

import plug.core.IFiredTransition;

public class MockPropertyVerifier<C, A> implements IPropertyVerifier<C, A> {

	@Override
	public C initialConfiguration(C initial) {
		return initial;
	}

	@Override
	public void verify(IFiredTransition<C, A> transition) {
		//no properties are verified here
	}

	@Override
	public void deadlock(C configuration) {}
}
