package plug.verifiers.deadlock;

import plug.core.IConfiguration;

public class FinalStateDetected<C extends IConfiguration> {
	protected final C finalState;

	public FinalStateDetected(C configuration) {
		this.finalState = configuration;
	}

	public C getFinalState() {
		return finalState;
	}
}
