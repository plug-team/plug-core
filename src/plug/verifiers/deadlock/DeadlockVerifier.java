package plug.verifiers.deadlock;

import announce4j.Announcer;
import plug.core.IConfiguration;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.events.CloseConfigurationEvent;
import plug.events.ExecutionEndedEvent;
import plug.events.FiredEvent;
import plug.events.OpenConfigurationEvent;
import plug.events.PropertyEvent;
import plug.explorer.BFSExplorer;
import plug.explorer.DFSExplorer;

public class DeadlockVerifier {

	public final Announcer announcer;

	protected boolean foundDeadlock = false;
	protected boolean currentConfigurationIsDeadlock = false;
	
	public DeadlockVerifier(Announcer announcer) {
		this.announcer = announcer;
		announcer.when(OpenConfigurationEvent.class, this::onOpenConfigurationEvent);
		announcer.when(CloseConfigurationEvent.class, this::onCloseConfigurationEvent);
		announcer.when(FiredEvent.class, this::onFiredEvent);
		announcer.when(ExecutionEndedEvent.class, this::onEnd);

	}

	protected void onOpenConfigurationEvent(Announcer ann, OpenConfigurationEvent<?> event) {
		currentConfigurationIsDeadlock = true;
	}

	protected void onFiredEvent(Announcer ann, FiredEvent<?, ?> event) {
		currentConfigurationIsDeadlock &= false;
	}

	protected void onCloseConfigurationEvent(Announcer ann, CloseConfigurationEvent<IConfiguration> event) {
		if (currentConfigurationIsDeadlock) {
			announcer.announce(new FinalStateDetected<>(event.getConfiguration()));
			foundDeadlock = true;
		}
	}

	protected void onEnd(Announcer announcer, Object o) {
		if (!foundDeadlock) {
			announcer.announce(new PropertyEvent<>( null,true, "deadlocks", null));
		}
	}


	public static <C extends IConfiguration, A> BFSExplorer<C, A> bfs(ITransitionRelation relation, IStateSpaceManager<C, A> stateSpaceManager) {
		BFSExplorer explorer = new BFSExplorer(relation, stateSpaceManager);
		new DeadlockVerifier(explorer.getAnnouncer());
		return explorer;
	}

	public static <C extends IConfiguration, A> DFSExplorer<C, A> dfs(ITransitionRelation relation, IStateSpaceManager<C, A> stateSpaceManager) {
		DFSExplorer explorer = new DFSExplorer(relation, stateSpaceManager);
		new DeadlockVerifier(explorer.getAnnouncer());
		return explorer;
	}
}
