package plug.core;

import java.util.Collection;

public interface IAtomicPropositionsProvider {
    Collection<String> getAtomicPropositions();
}
