package plug.core;

public interface ITransitionGeneralizedBuchiConfiguration {
    int getAcceptanceSetIndex();
    int getNumberOfAccetanceSets();
}
