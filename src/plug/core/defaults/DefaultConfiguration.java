package plug.core.defaults;

import plug.core.IConfiguration;

public abstract class DefaultConfiguration<T extends IConfiguration> implements IConfiguration<T> {
    private Object metadata;

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }
}
