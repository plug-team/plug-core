package plug.core;

import java.util.List;
import java.util.Objects;
import plug.core.view.ConfigurationItem;

/**
 * {@link IRuntimeView} provides methods to construct representation for a given {@link ITransitionRelation}.
 */
public interface IRuntimeView<C, T> {

	/** {@link ITransitionRelation} described the runtime view */
    ITransitionRelation<C, T> getRuntime();

    /** Constructs internal tree description for a configuration
	 * TODO: 1. Make this aligned with https://microsoft.github.io/debug-adapter-protocol/
	 * TODO: 2. Add the possibility to complement the default with a SVG image
	 * */
	List<ConfigurationItem> getConfigurationItems(C value);

	/** Returns the configuration short description used for quick visual identification */
	default String getConfigurationDescription(C value) {
		return Integer.toHexString(value.hashCode());
	}

	/** Returns the fireable transition description */
    default String getFireableTransitionDescription(T transition) {
    	return transition.toString();
	}

    /** Returns the action (fired transition) description */
    default String getActionDescription(Object action) {
    	return Objects.toString(action);
	}
}
