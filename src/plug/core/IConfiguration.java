package plug.core;


public interface IConfiguration<T extends IConfiguration> {
	T createCopy();
	int hashCode();
	boolean equals(Object other);

	Object getMetadata();
	void setMetadata(Object metadata);

//	default <D extends Object> D getMetadata(Class<D> type) {
//		Object data = getMetadata();
//		if (data == null) return null;
//		if (type.isAssignableFrom(data.getClass())) {
//			return type.cast(data);
//		}
//		return null;
//	}
}
