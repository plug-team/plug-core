package plug.core;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public interface IFiredTransition<C, A> {
	C getSource();
	void setSource(C configuration);
	Collection<C> getTargets();
	void setTargets(Collection<C> configuration);
	A getAction();
	void setAction(A action);
	Object getPayload();
	void setPayload(Object payload);

	default C getTarget(int index) {
		Iterator<C> iterator = getTargets().iterator();
		for (int i=0; i<index && iterator.hasNext();i++) {
			iterator.next();
		}
		return iterator.hasNext() ? iterator.next() : null;
	}

	default boolean equals(IFiredTransition<C, A> obj) {
		if (this == obj) return true;
		if (! this.getSource().equals(obj.getSource())) return false;
		if (! this.getAction().equals(obj.getAction())) return false;
		Collection<C> otherTargets = obj.getTargets();
		for (C thisTarget : this.getTargets()) {
			if (!otherTargets.contains(thisTarget)) return false;
		}
		return true;
	}
}
