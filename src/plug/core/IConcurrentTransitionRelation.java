package plug.core;

/**
 * Created by ciprian on 02/05/15.
 */
public interface IConcurrentTransitionRelation<T extends IConcurrentTransitionRelation<T, C, A>, C extends IConfiguration, A> extends ITransitionRelation<C,A> {
    T createCopy();
}
