package plug.core;

import java.util.*;

import plug.core.execution.ControllerProviderFunction;
import plug.core.view.DefaultRuntimeView;
import plug.explorer.BFSExplorer;
import plug.explorer.DFSExplorer;
import plug.verifiers.deadlock.DeadlockVerifier;

/**
 * Module for a runtime language.
 */
public interface ILanguagePlugin<T extends ITransitionRelation> {
    /** Module name */
    String getName();

    /** File extensions for the models read by the module */
    String[] getExtensions();

    /** Module loader */
    ILanguageLoader<T> getLoader();

    /** Runtime view for a given runtime */
    default IRuntimeView getRuntimeView(T runtime) {
        return new DefaultRuntimeView(runtime);
    }

    /** Returns modules for language */
    default Map<String, ControllerProviderFunction> executions() {
        return defaultExecutions();
    }

    static Map<String, ControllerProviderFunction> defaultExecutions() {
        return new HashMap() {{
            put("BFS Explorer", (ControllerProviderFunction) BFSExplorer::new);
            put("Deadlock Verifier", (ControllerProviderFunction) DeadlockVerifier::bfs);
            put("DFS Explorer", (ControllerProviderFunction) DFSExplorer::new);
        }};
    }

}
