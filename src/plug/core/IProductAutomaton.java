package plug.core;

public interface IProductAutomaton<C, F> {
    Iterable<C> initialConfigurationsIterable();

    Iterable<F> fireableTransitionsIterable(C source);

    Iterable<C> getPostIterable(C source);
}
