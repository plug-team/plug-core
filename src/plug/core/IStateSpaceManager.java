package plug.core;

import plug.statespace.IGraphAccess;

import java.util.Set;
import java.util.function.Consumer;

public interface IStateSpaceManager<C, A> {
	default IStateSpaceManager<C,A> noConfigurationStorage() {return this;}
	default IStateSpaceManager<C,A> countingConfigurationStorage() {return this;}
	default IStateSpaceManager<C,A> fullConfigurationStorage() {return this;}
	default IStateSpaceManager<C,A> noTransitionStorage() {return this;}
	default IStateSpaceManager<C,A> countingTransitionStorage() {return this;}
	default IStateSpaceManager<C,A> parentTransitionStorage() {return this;}
	default IStateSpaceManager<C,A> fullTransitionStorage() {return this;}

	Set<C> initialConfigurations();

	IFiredTransition putTransition(IFiredTransition<C, A> transition);
	C get(C state);
	C put(C state);
	C putInitial(C state);

	int size();
    int transitionCount();

	IGraphAccess<C, A> getGraphView();
}