package plug.core.view;

import plug.core.IRuntimeView;
import plug.core.ITransitionRelation;

import java.util.Collections;
import java.util.List;

public class DefaultRuntimeView<C, T> implements IRuntimeView<C, T> {
    private final ITransitionRelation<C, T> runtime;

    public DefaultRuntimeView(ITransitionRelation<C, T> runtime) {
        this.runtime = runtime;
    }

    @Override
    public ITransitionRelation getRuntime() {
        return runtime;
    }

    @Override
    public List<ConfigurationItem> getConfigurationItems(C value) {
        return Collections.singletonList(new ConfigurationItem("configuration", value.toString(), null, null));
    }

    @Override
    public String getFireableTransitionDescription(T transition) {
        return transition.toString();
    }
}
