package plug.core.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class ConfigurationItem implements Iterable<ConfigurationItem> {

	protected final String name;

	protected final String type;

	protected final String icon;

	protected final List<ConfigurationItem> children = new ArrayList<>();

	protected boolean different = false;

	protected boolean expanded = false;

	public ConfigurationItem(String type, String name, String icon, List<ConfigurationItem> children) {
		this.name = name;
		this.type = type;
		this.icon = icon;
		if (children != null) {
			this.children.addAll(children);
		}
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getIcon() {
		return icon;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public boolean isDifferent() {
		return different;
	}

	public void setDifferent(boolean different) {
		this.different = different;
	}

	@Override
	public Iterator<ConfigurationItem> iterator() {
		return children.iterator();
	}

	public List<ConfigurationItem> getChildren() {
		return Collections.unmodifiableList(children);
	}

	/** Returns true if the other config is different from this.
	  * It only checks local attributes, it doesn't test the children. */
	public boolean isDifferentFrom(ConfigurationItem other) {
		return !Objects.equals(name, other.name) || !Objects.equals(type, other.type);
	}

	public void updateExpanded(ConfigurationItem other) {
		if (other == null) return;
		setExpanded(other.expanded);
		List<ConfigurationItem> myChildren = getChildren();
		List<ConfigurationItem> otherChildren = other.getChildren();

		for (int i = 0; i < myChildren.size(); i++) {
			ConfigurationItem myChild = myChildren.get(i);
			ConfigurationItem otherChild = i < otherChildren.size() ? otherChildren.get(i) : null;
			myChild.updateExpanded(otherChild);
		}
	}

	/** Computes the different flag for this item and its children. */
	public boolean updateDifferentStatus(ConfigurationItem other) {
		if (other == null) {
			for (ConfigurationItem children : getChildren()) {
				children.updateDifferentStatus(other);
			}
			return different = true;
		}
		List<ConfigurationItem> otherChildren = other.getChildren();
		different = isDifferentFrom(other);
		different |= children.size() != otherChildren.size();
		for (int i = 0; i < children.size(); i++) {
			ConfigurationItem confChild = children.get(i);
			ConfigurationItem otherChild = i < otherChildren.size() ? otherChildren.get(i) : null;
			different |= confChild.updateDifferentStatus(otherChild);
		}

		return different;
	}

	@Override
	public String toString() {
		return name;
	}
}
