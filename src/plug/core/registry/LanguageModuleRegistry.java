package plug.core.registry;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;
import plug.core.ILanguagePlugin;

public class LanguageModuleRegistry {

    private final Map<String, ILanguagePlugin> languageNameMap = new HashMap<>();
    private final Map<String, ILanguagePlugin> extensionMap = new HashMap<>();

    private LanguageModuleRegistry() {
    }

    public ILanguagePlugin getModuleByName(String name) {
        return languageNameMap.get(name);
    }

    public ILanguagePlugin getModuleByExtension(URI fileURI) {
        return getModuleByExtension(Paths.get(fileURI.getPath()));
    }

    public ILanguagePlugin getModuleByExtension(Path filePath) {
        if (filePath == null) return null;
        String fileName = filePath.getFileName().toString();
        int dotIndex = fileName.lastIndexOf(".");
        if (dotIndex < 1) return null;
        String fileExtension = fileName.substring(dotIndex);
        return extensionMap.get(fileExtension);
    }

    public Collection<ILanguagePlugin> getModules() {
        return Collections.unmodifiableCollection(languageNameMap.values());
    }

    private static LanguageModuleRegistry instance = null;

    public static LanguageModuleRegistry getInstance() {
        if (instance == null) {
            // thread protection to load the instance.
            synchronized (LanguageModuleRegistry.class) {
                if (instance == null) {
                    instance = new LanguageModuleRegistry();
                    Iterator<ILanguagePlugin> iterator = ServiceLoader.load(ILanguagePlugin.class).iterator();
                    while (iterator.hasNext()) {
                        ILanguagePlugin ilm = iterator.next();
                        System.out.println("Loading " + ilm.getName() + " language plugin");
                        instance.languageNameMap.put(ilm.getName(), ilm);
                        for (String extension : ilm.getExtensions()) {
                            instance.extensionMap.put(extension, ilm);
                        }
                    }
                }
            }
        }
        return instance;
    }
}
