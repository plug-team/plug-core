package plug.core;

import java.nio.file.Path;
import java.util.Map;
import java.util.function.Supplier;
import plug.core.registry.LanguageModuleRegistry;

/**
 * Description for a runtime. It can be described with a path and a module or a
 * already constructed runtime.
 */
@Deprecated
public class RuntimeDescription {

	/** Path for model */
	public final Path modelPath;

	/** Module to load model */
	protected final ILanguagePlugin module;

	/** Options to use */
	protected final Map<String, Object> options;

	/** Runtime supplier */
	protected Supplier<ITransitionRelation> supplier;

	public RuntimeDescription(Path modelPath) {
		this(modelPath, null);
	}

	public RuntimeDescription(Path modelPath, Map<String, Object> options) {
		this.modelPath = modelPath;
		this.module = LanguageModuleRegistry.getInstance().getModuleByExtension(modelPath);
		this.options = options;
		this.supplier = null;
	}

	public RuntimeDescription(ILanguagePlugin module, Supplier<ITransitionRelation> supplier) {
		this.modelPath = null;
		this.options = null;
		this.supplier = supplier;
		this.module = module;
	}

	public ITransitionRelation<IConfiguration, Object> getRuntime() throws Exception {
		if (supplier != null) return supplier.get();
		return module.getLoader().getRuntime(modelPath.toUri(), options);
	}

	public ILanguagePlugin getModule() {
		return module;
	}

}
