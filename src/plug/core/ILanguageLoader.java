package plug.core;

import java.io.File;
import java.net.URI;
import java.util.Map;

/**
 * Created by Ciprian TEODOROV on 02/03/17.
 */
public interface ILanguageLoader<R extends ITransitionRelation> {

    R getRuntime(URI modelURI, Map<String, Object> options) throws Exception;

    default R getRuntime(File file, Map<String, Object> options) throws Exception {
        if (file == null) return getRuntime((URI)null, options);
        return getRuntime(file.toURI(), options);
    }
    default R getRuntime(String fileName, Map<String, Object> options) throws Exception {
        if (fileName == null) return getRuntime((URI)null, options);
        return getRuntime(new File(fileName), options);
    }

    default R getRuntime(URI modelURI) throws Exception {
        if (modelURI == null) return getRuntime((URI) null, null);
        return getRuntime(modelURI, null);
    }

    default R getRuntime(File file) throws Exception {
        if (file == null) return getRuntime((URI) null);
        return getRuntime(file.toURI());
    }

    default R getRuntime(String fileName) throws Exception {
        if (fileName == null) return getRuntime((URI) null);
        return getRuntime(new File(fileName).toURI());
    }
}
