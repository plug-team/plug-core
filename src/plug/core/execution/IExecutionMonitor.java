package plug.core.execution;

import java.util.concurrent.Semaphore;

public interface IExecutionMonitor {

	void pause();

	void resume();

	void hasToFinish();

	class Simple implements IExecutionMonitor {

		protected final Semaphore semaphore = new Semaphore(1);

		protected boolean atEnd = false;

		@Override
		public void pause() {
			try {
				semaphore.acquire();
			} catch (InterruptedException e) {
				// nothing to do here
			}
		}

		@Override
		public void resume() {
			semaphore.release();
		}

		@Override
		public void hasToFinish() {
			atEnd = true;
			resume();
		}

		public boolean atEnd() {
			try {
				semaphore.acquire();
			} catch (InterruptedException e) {
				// nothing to do here
			}
			semaphore.release();
			return atEnd;
		}
	}
}
