package plug.core.execution;


import announce4j.Announcer;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;

public interface IExecutionController<C, A> {
	Announcer getAnnouncer();
	IStateSpaceManager<C, A> getStateSpaceManager();
	ITransitionRelation getRuntime();

	//IProductAutomaton getProductAutomaton();
	IExecutionMonitor getMonitor();

	void execute();


}
