package plug.core.execution;

public class ExecutionException extends RuntimeException {

	protected final boolean model;

	public ExecutionException(String message) {
		this(message, null, true);
	}

	public ExecutionException(String message, Throwable cause) {
		this(message, cause, true);
	}

	public ExecutionException(String message, Throwable cause, boolean model) {
		super(message, cause);
		this.model = model;
	}

	public boolean isModel() {
		return model;
	}
}
