package plug.core.execution;

import announce4j.Announcer;
import org.apache.commons.lang3.reflect.FieldUtils;
import plug.core.*;
import plug.events.ExecutionEndedEvent;
import plug.events.PropertyEvent;
import plug.simulation.trace_storage.TraceStore;
import plug.statespace.IGraphAccess;
import plug.statespace.SimpleStateSpaceManager;
import plug.statespace.graph.FullTransitionStorage;
import plug.statespace.graph.ParentTransitionStorage;
import plug.utils.graph.algorithms.DijkstraShortestPath;
import plug.verifiers.deadlock.FinalStateDetected;

import java.lang.reflect.Field;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * An execution contains all the information about an execution. Its
 * description, its status and its results if any. It allows to run, pause,
 * cancel the exploration.
 *
 * The execution uses a thread to run the exploration. The command methods are
 * thread safe.
 */
public class Execution {

    public enum TransitionStorageType { NONE, COUNTING, PARENT, FULL}
    public enum Status { CREATED, INITIALIZED, RUNNING, PAUSED, STOPPED, FINISHED, FAILED }
    public enum VerificationStatus {SATISFIED, VIOLATED, UNKNOWN}
    public enum CompletenessStatus {COMPLETE, INCOMPLETE}

    /** The status of an Execution */
    public Status status = Status.CREATED;

    /** Execution name */
    protected final String name;

    ILanguagePlugin languagePlugin;
    Path modelPath;

    /** The description of the execution */
    ControllerProviderFunction description;
    ITransitionRelation transitionRelation;
    IRuntimeView runtimeView;

    IExecutionController controller;
    TransitionStorageType transitionStorageType;
    Set<Object> acceptAllSet;

    public TraceStore traceStore = new TraceStore();

    protected Thread thread;

    Throwable exception;
    PropertyEvent violation;

    public Execution(
            String name,
            Path modelPath,
            ILanguagePlugin languagePlugin,
            ControllerProviderFunction description) {
        this(name, modelPath, languagePlugin, description, TransitionStorageType.PARENT, Collections.emptySet());
    }

    public Execution(
            String name,
            Path modelPath,
            ILanguagePlugin languagePlugin,
            ControllerProviderFunction description,
            Set<Object> acceptAllStates) {
        this(name, modelPath, languagePlugin, description, TransitionStorageType.PARENT, acceptAllStates);
    }

    public Execution(
            String name,
            Path modelPath,
            ILanguagePlugin languagePlugin,
            ControllerProviderFunction description,
            TransitionStorageType transitionStorageType,
            Set<Object> acceptAllStates) {
        this.name = name;
        this.modelPath = modelPath;
        this.languagePlugin = languagePlugin;
        this.description = description;
        this.transitionStorageType = transitionStorageType;
        this.acceptAllSet = acceptAllStates;
    }

    @Override
    protected synchronized void finalize() throws Throwable {
        super.finalize();
        if (transitionRelation != null)
            transitionRelation.close();
    }

    public synchronized String getName() {
        return name;
    }
    public synchronized Status status() {
        return status;
    }
    public synchronized PropertyEvent getViolation() {
        return violation;
    }
    public synchronized Throwable getException() {
        return exception;
    }

    public ITransitionRelation getTransitionRelation() {
        if (status != Status.INITIALIZED) {
            initialize();
        }
        return transitionRelation;
    }

    public synchronized IRuntimeView getRuntimeView() {
        if (status != Status.INITIALIZED) {
            initialize();
        }
        return runtimeView;
    }

    protected long loadingTime = 0;

    public synchronized void initialize() {
        if (!(status == Status.CREATED || status == Status.FAILED)) {
            return;
        }
        try {
            long startLoading = System.currentTimeMillis();
            transitionRelation = languagePlugin.getLoader().getRuntime(modelPath != null ? modelPath.toAbsolutePath().toString() : null, null);
            runtimeView = languagePlugin.getRuntimeView(transitionRelation);
            IStateSpaceManager stateSpaceManager = new SimpleStateSpaceManager<>();
            switch (transitionStorageType) {
                case FULL:
                    stateSpaceManager.fullTransitionStorage();
                    break;
                case PARENT:
                    stateSpaceManager.parentTransitionStorage();
                    break;
                case COUNTING:
                    stateSpaceManager.countingTransitionStorage();
                    break;
                case NONE: //nothing
                    break;
                default: //nothing
            }
            controller = description.createController(transitionRelation, stateSpaceManager);
            loadingTime = System.currentTimeMillis() - startLoading;
        } catch (Throwable e) {
            exception = e;
            status = Status.FAILED;
            return;
        }
        
        if (transitionRelation == null || controller == null) {
            status = Status.FAILED;
        } else {
        	status = Status.INITIALIZED;
        }
    }

    public synchronized Thread run() {
        if (!(status == Status.INITIALIZED)) {
            return null;
        }

        try {
            thread = new Thread(this::runnerThread);
            status = Status.RUNNING;
            thread.start();
        } catch (Throwable e) {
            exception = e;
            if (transitionRelation!=null) transitionRelation.close();
            status = Status.FAILED;
            return null;
        }
        return thread;
    }

    /** Pauses the execution if running */
    public synchronized void pause() {
        if (! (status == Status.RUNNING)) {
            return;
        }
        controller.getMonitor().pause();
        status = Status.PAUSED;
    }

    /** Resumes the execution if paused */
    public synchronized void resume() {
        if (! (status == Status.PAUSED)) {
            return;
        }
        controller.getMonitor().resume();
        status = Status.RUNNING;
    }

    /** Stops the executions */
    public synchronized void stop() {
        if (!(     status == Status.PAUSED
                || status == Status.RUNNING)) {
            return;
        }
        controller.getMonitor().hasToFinish();
        status = Status.STOPPED;
    }

    public synchronized void reset() {
        if (!(     status == Status.STOPPED
                || status == Status.FAILED
                || status == Status.FINISHED)) {
            return;
        }
        if (transitionRelation!=null) transitionRelation.close();
        controller = null;
        traceStore = new TraceStore();
        startTime = endTime = 0;
        exception = null;
        violation = null;
        thread = null;
        status = Status.CREATED;
    }

    public synchronized void hardReset() {
        try {
            controller.getMonitor().hasToFinish();
            if (transitionRelation!=null) transitionRelation.close();
            controller = null;
            traceStore = new TraceStore();
            startTime = endTime = 0;
            exception = null;
            violation = null;
            thread = null;
        } catch (Throwable e) {
            exception = e;
            status = Status.FAILED;
            return;
        }
        status = Status.CREATED;
    }

    boolean firstAcceptAllOK = false;
    //This is the filter that removes all buchi-configurations that are accepts only with one always true fanout
    boolean notInAcceptAll(Object c) {
        if (c.getClass().getCanonicalName().equals("plug.language.buchikripke.runtime.KripkeBuchiConfiguration")) {
            Field buchiField = FieldUtils.getField(c.getClass(), "buchiState");
            try {
                Object buchiConfiguration = buchiField.get(c);
                Field stateField = FieldUtils.getField(buchiConfiguration.getClass(), "buchiState");
                Object stateObject = stateField.get(buchiConfiguration);
                if (acceptAllSet.contains(stateObject)) {
                    if (firstAcceptAllOK) {
                        return false;
                    } else {
                        firstAcceptAllOK = true;
                        return true;
                    }
                }
            } catch (IllegalAccessException e) {
                return true;
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    void runnerThread() {
        Announcer announcer = controller.getAnnouncer();

        // @end --> FINISHED
        announcer.when(ExecutionEndedEvent.class, (a, event) -> {
            endTime = System.currentTimeMillis();
            status = Status.FINISHED;
            traceStore.noCounterExample();
        });

        // @violation --> FINISHED
        announcer.when(PropertyEvent.class, (a, event) -> {
            //TODO: listen for PropertyViolation event, which should be a subclass of property event
            if (!event.isVerified()) {
                controller.getMonitor().hasToFinish();
                violation = event;
                endTime = System.currentTimeMillis();
                status = Status.FINISHED;
                firstAcceptAllOK = false;
                traceStore.addSteps(
                        Collections.emptyList(),
                        (List)(event.getCounterExample()
                                .stream()
                                .filter(this::notInAcceptAll)
                                .collect(Collectors.toList())),
                        transitionRelation::actionFromSourceToTarget);
                traceStore.setHasCounterExample();
            }
        });

        // when FinalStateDetected send the property event which will do @violation --> FINISHED
        announcer.when(FinalStateDetected.class, (a, event) -> {
            //TODO: FinalStateDetected should be a subclass of PropertyViolation event
            //deadlock reached stop the execution
            controller.getMonitor().hasToFinish();
            endTime = System.currentTimeMillis();
            //build the counter example
            SimpleStateSpaceManager stateSpaceManager = (SimpleStateSpaceManager) controller.getStateSpaceManager();
            List<IConfiguration> counterExample = null;
            if (stateSpaceManager.transitionStorage instanceof FullTransitionStorage) {
                counterExample = new DijkstraShortestPath<>()
                        .getShortestPath(
                                stateSpaceManager.getGraphView(),
                                stateSpaceManager.initialConfigurations(),
                                event.getFinalState());
            } else if (stateSpaceManager.transitionStorage instanceof ParentTransitionStorage) {
                counterExample = new ArrayList<>();
                counterExample.add(event.getFinalState());

                IGraphAccess<IConfiguration, Object> graph = stateSpaceManager.getGraphView();

                IConfiguration current = event.getFinalState();
                while (current != null && !stateSpaceManager.initialConfigurations().contains(current)) {
                    IConfiguration parent = graph.getParent(current);

                    if (parent != null) {
                        counterExample.add(parent);
                    }
                    current = parent;
                }
                Collections.reverse(counterExample);
            }
            PropertyEvent deadlock = new PropertyEvent<>(controller, false, "deadlock", counterExample);
            controller.getAnnouncer().announce(deadlock);
        });

        // @exception --> Failed
        try {
            startTime = System.currentTimeMillis();
            controller.execute();

            //StateSpace2TGF.toTGF(controller.getStateSpaceManager().getGraphView(), "/Users/ciprian/Downloads/gg.tgf");
        } catch (Throwable e) {
            exception = e;
            status = Status.FAILED;
            return;
        }
    }

    /** Start time used in {@link #getElapsedTime()} */
    protected long startTime = 0;

    /** End time used in {@link #getElapsedTime()} */
    protected long endTime = 0;

    /** Returns the duration of the execution or the time since it started if no yet ended. */
    public synchronized long getElapsedTime() {
        if (startTime == 0) {
            return 0;
        }
        if (endTime == 0) {
            return System.currentTimeMillis() - startTime;
        }
        return endTime - startTime;
    }

    public synchronized long getLoadingTime() {
        return loadingTime;
    }

    public synchronized int configurationCount() {
        if (controller == null) return 0;
        return controller.getStateSpaceManager().size();
    }

    public synchronized int transitionCount() {
        if (controller == null) return 0;
        return controller.getStateSpaceManager().transitionCount();
    }

    public synchronized String getDetails() {
        if (status == Status.CREATED) {
            return "initializing";
        }
        if (status == Status.INITIALIZED) {
            return "no results";
        }

        StringBuilder result = new StringBuilder();
        if (status == Status.FAILED || status == Status.STOPPED) {
            result.append("Incomplete ");
        }
        if (status == Status.FINISHED) {
            result.append("Finished ");
        }

        int size = configurationCount();
        if (size > 0) {
            result.append(size + " configurations ");
            int transitionCount = transitionCount();
            if (transitionCount > 0) {
                result.append(transitionCount + " transitions ");
            }
            result.append("in " + getElapsedTime() + " ms");
        }
        return result.toString();
    }

    /** General verification status for the execution */
    public synchronized VerificationStatus verificationStatus() {
        if (status != Status.FINISHED) return VerificationStatus.UNKNOWN;
        if (violation == null) return VerificationStatus.SATISFIED;
        return VerificationStatus.VIOLATED;
    }

    public synchronized CompletenessStatus resultStatus() {
        if (status == Status.FINISHED) return CompletenessStatus.COMPLETE;
        return CompletenessStatus.INCOMPLETE;
    }

    public synchronized IGraphAccess getGraphView() {
        if (controller == null || controller.getStateSpaceManager() == null) {
            return null;
        }
        return controller.getStateSpaceManager().getGraphView();
    }
}
