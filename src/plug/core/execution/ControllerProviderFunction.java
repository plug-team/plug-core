package plug.core.execution;

import plug.core.IConfiguration;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;

@FunctionalInterface
public interface ControllerProviderFunction {
	IExecutionController<? extends IConfiguration, Object> createController(ITransitionRelation transitionRelation, IStateSpaceManager stateSpaceManager);
}
