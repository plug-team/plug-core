package plug.statespace;

import java.util.Collection;

/**
 * Created by ciprian on 13/02/17.
 */
public interface IConfigurationStorage<C> {
    C get(C configuration);
    C put(C configuration);
    int size();
    Collection<C> getConfigurations();
}
