package plug.statespace.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import plug.core.IConfiguration;
import plug.core.IFiredTransition;
import plug.statespace.ITransitionStorage;

public class FullTransitionStorage<C extends IConfiguration, A> implements ITransitionStorage<C, A> {
	Map<C, List<IFiredTransition<C, A>>> adjacencyList = new HashMap<>();
	int size = 0;
	int edgeCount = 0;

	@Override
	public synchronized IFiredTransition put(IFiredTransition<C, A> transition) {
		size++;
		edgeCount += transition.getTargets().size();
		List<IFiredTransition<C, A>> fanout = adjacencyList.get(transition.getSource());

		if (fanout == null) {
			fanout = new ArrayList<>();
			adjacencyList.put(transition.getSource(), fanout);
		}

		Optional<IFiredTransition<C,A>> alreadyPresent = fanout.stream()
				.filter(t -> Objects.equals(t.getAction(),transition.getAction()) && transition.getTargets().containsAll(t.getTargets()))
				.findAny();

		if (!alreadyPresent.isPresent()) {
			fanout.add(transition);
			return transition;
		}
		return alreadyPresent.get();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public C getParent(C configuration) {
		for (Map.Entry<C, List<IFiredTransition<C, A>>> entry : adjacencyList.entrySet()) {
			for (IFiredTransition<C,A> transition : entry.getValue()) {
				for (C target : transition.getTargets()) {
					if (target == configuration) {
						return transition.getSource();
					}
				}
			}
		}
		//no parent was found for the configuration
		return null;
	}

	@Override
	public synchronized Collection<IFiredTransition<C, A>> getFiredFrom(C configuration) {
		Collection<IFiredTransition<C, A>> result = adjacencyList.get(configuration);
		if (result == null) return Collections.emptyList();
		return result;
	}

	public synchronized Collection<IFiredTransition<C, A>> getFiredTo(C configuration) {
		Collection<IFiredTransition<C, A>> result = new ArrayList<>();

		for (Map.Entry<C, List<IFiredTransition<C, A>>> entry : adjacencyList.entrySet()) {
			for (IFiredTransition<C,A> transition : entry.getValue()) {
				for (C target : transition.getTargets()) {
					if (target.equals(configuration)) {
						result.add(transition);
						break;
					}
				}
			}
		}
		return result;
	}

	@Override
	public int getEdgeCount() {
		return edgeCount;
	}
}
