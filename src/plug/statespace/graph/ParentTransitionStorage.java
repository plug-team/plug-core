package plug.statespace.graph;

import java.util.*;

import plug.core.IConfiguration;
import plug.core.IFiredTransition;
import plug.statespace.ITransitionStorage;

public class ParentTransitionStorage<C extends IConfiguration, A> implements ITransitionStorage<C, A> {
	Map<C, IFiredTransition<C, A>> parent = new HashMap<>();

	@Override
	public IFiredTransition put(IFiredTransition<C, A> transition) {
		IFiredTransition v = null;
		for (C target : transition.getTargets()) {
			v = parent.putIfAbsent(target, transition);
		}
		return v;
	}
	@Override
	public int size() {
		return parent.size();
	}

	@Override
	public C getParent(C configuration) {
		IFiredTransition<C, A> theParentTransition = parent.get(configuration);
		if (theParentTransition == null) return null;
		return theParentTransition.getSource();
	}

	@Override
	public Collection<IFiredTransition<C, A>> getFiredFrom(C configuration) {
		//The worst case cost is O(size) : prefer using another storage if many calls needed
		List<IFiredTransition<C, A>> result = new ArrayList<>();
		for (Map.Entry<C, IFiredTransition<C, A>> entry : parent.entrySet()) {
			if (entry.getValue().getSource() == configuration) {
				result.add(entry.getValue());
			}
		}
		return result;
	}

	@Override
	public Collection<IFiredTransition<C, A>> getFiredTo(C configuration) {
		IFiredTransition<C, A> transition = parent.get(configuration);
		return transition != null ? Collections.singleton(transition) : Collections.emptyList();
	}

	@Override
	public int getEdgeCount() {
		return size();
	}
}
