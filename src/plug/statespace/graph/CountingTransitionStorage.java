package plug.statespace.graph;

import plug.core.IConfiguration;
import plug.core.IFiredTransition;
import plug.statespace.ITransitionStorage;

public class CountingTransitionStorage<C extends IConfiguration, A> extends MockTransitionStorage<C, A> {
	int size = 0;
	int edgeCount = 0;
	@Override
	public IFiredTransition put(IFiredTransition<C, A> transition) {
		size++;
		edgeCount += transition.getTargets().size();
		return transition;
	}
	@Override
	public int size() {
		return size;
	}

	@Override
	public int getEdgeCount() {
		return edgeCount;
	}
}
