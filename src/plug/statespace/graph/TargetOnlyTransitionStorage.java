package plug.statespace.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import plug.core.IConfiguration;
import plug.core.IFiredTransition;
import plug.statespace.ITransitionStorage;
import plug.statespace.transitions.FiredTransition;

/**
 * Created by ciprian on 17/02/17.
 */
public class TargetOnlyTransitionStorage<C extends IConfiguration, A> implements ITransitionStorage<C, A> {
    Map<C, Set<C>> adjacencyList = new HashMap<>();
    int size = 0;
    int edgeCount = 0;

    @Override
    public IFiredTransition put(IFiredTransition<C, A> transition) {
        size++;
        edgeCount += transition.getTargets().size();
        Set<C> fanout = adjacencyList.get(transition.getSource());

        if (fanout == null) {
            fanout = new HashSet<>();
            adjacencyList.put(transition.getSource(), fanout);
        }

        for (C target : transition.getTargets()) {
            fanout.add(target);
        }
        return transition;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public C getParent(C configuration) {
        for (Map.Entry<C, Set<C>> entry : adjacencyList.entrySet()) {
            for (C target : entry.getValue()) {
                if (target == configuration) {
                    return entry.getKey();
                }
            }
        }
        //no parent was found for the configuration
        return null;
    }

    @Override
    public Collection<IFiredTransition<C, A>> getFiredFrom(C configuration) {
        Set<C> result = adjacencyList.get(configuration);
        if (result == null) return Collections.emptyList();
        List<IFiredTransition<C, A>> fired = new ArrayList<>(result.size());
        for (C target : result) {
            fired.add(new FiredTransition<>(configuration, target, null));
        }
        return fired;
    }

    public Collection<IFiredTransition<C, A>> getFiredTo(C configuration) {
        return Collections.emptyList();
    }

    @Override
    public int getEdgeCount() {
        return edgeCount;
    }
}
