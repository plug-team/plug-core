package plug.statespace.graph;

import java.util.Collection;
import java.util.Collections;
import plug.core.IConfiguration;
import plug.core.IFiredTransition;
import plug.statespace.ITransitionStorage;

public class MockTransitionStorage<C extends IConfiguration, A> implements ITransitionStorage<C, A> {

	@Override
	public IFiredTransition put(IFiredTransition<C, A> transition) {
		//does not do anything
		return transition;
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public int getEdgeCount() {
		return 0;
	}

	@Override
	public C getParent(C configuration) {
		//doesn't store anything so null is the return
		return null;
	}

	@Override
	public Collection<IFiredTransition<C, A>> getFiredFrom(C configuration) {
		//doesn't store anything so emptyList is the return
		return Collections.emptyList();
	}

	@Override
	public Collection<IFiredTransition<C, A>> getFiredTo(C configuration) {
		//doesn't store anything so emptyList is the return
		return Collections.emptyList();
	}


}
