package plug.statespace.configurations;

import plug.statespace.IConfigurationStorage;

import java.util.Collection;
import java.util.Collections;

public class MockConfigurationStorage<C> implements IConfigurationStorage<C> {

	@Override
	public C get(C configuration) {
		//return always true meaning that the configuration has been added.
		//it does not add it but enables system execution without trace storage
		return configuration;
	}

	public C put(C configuration) {
		return configuration;
	}


	public int size() {
		return 0;
	}

	@Override
	public Collection<C> getConfigurations() {
		//does not store any configuration so it returns an empty collection
		return Collections.emptySet();
	}
}
