package plug.statespace.configurations;

import plug.statespace.IConfigurationStorage;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class FullConfigurationStorage<C> implements IConfigurationStorage<C> {
	public Map<C, C> storageBackend;

	public boolean contains(C configuration) {
		return get(configuration) != null;
	}

	@Override
	public C get(C configuration) {
		return storageBackend.get(configuration);
	}

	@Override
	public C put(C configuration) {
		storageBackend.put(configuration, configuration);
		return configuration;
	}

	@Override
	public int size() {
		return storageBackend.size();
	}
	
	public void setStorageBackend(Map<C, C> map) {
		storageBackend = map;
	}

	@Override
	public Collection<C> getConfigurations() {
		return storageBackend.values();
	}
}
