package plug.statespace.configurations;

import plug.statespace.IConfigurationStorage;

import java.util.Collection;
import java.util.Collections;

public class CountingConfigurationStorage<C> extends MockConfigurationStorage<C> {
	int size = 0;
	
	@Override
	public C get(C configuration) {
		return configuration;
	}

	@Override
	public C put(C configuration) {
		size++;
		return configuration;
	}


	@Override
	public int size() {
		return size;
	}

	@Override
	public Collection<C> getConfigurations() {
		//does not store any configuration so it returns an empty collection
		return Collections.emptySet();
	}
}
