package plug.statespace;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import plug.core.IConfiguration;
import plug.core.IFiredTransition;
import plug.core.IStateSpaceManager;
import plug.statespace.configurations.CountingConfigurationStorage;
import plug.statespace.configurations.FullConfigurationStorage;
import plug.statespace.configurations.MockConfigurationStorage;
import plug.statespace.graph.CountingTransitionStorage;
import plug.statespace.graph.FullTransitionStorage;
import plug.statespace.graph.MockTransitionStorage;
import plug.statespace.graph.ParentTransitionStorage;

/**
 * Created by ciprian on 12/02/17.
 */
public class SimpleStateSpaceManager<C extends IConfiguration, A> implements IStateSpaceManager<C, A>, IGraphAccess<C, A> {
    Set<C> initialConfigurations = new HashSet<>();
    IConfigurationStorage<C> configurationStorage;
    public ITransitionStorage<C, A> transitionStorage;

    public SimpleStateSpaceManager() {
        this(false);
    }

    public SimpleStateSpaceManager(boolean fullTransition) {
       fullConfigurationStorage();
       if (fullTransition) {
           fullTransitionStorage();
       } else {
            noTransitionStorage();
       }
    }

    @Override
    public IStateSpaceManager<C,A> noConfigurationStorage() {
        configurationStorage = new MockConfigurationStorage<>();
        return this;
    }

    @Override
    public IStateSpaceManager<C,A> countingConfigurationStorage() {
        configurationStorage = new CountingConfigurationStorage<>();
        return this;
    }

    @Override
    public IStateSpaceManager<C,A> fullConfigurationStorage() {
        configurationStorage = new FullConfigurationStorage<>();
        ((FullConfigurationStorage<C>)configurationStorage).setStorageBackend(new HashMap<>());
        return this;
    }

    @Override
    public IStateSpaceManager<C,A> noTransitionStorage() {
        transitionStorage = new MockTransitionStorage<>();
        return this;
    }

    @Override
    public IStateSpaceManager<C,A> countingTransitionStorage() {
        transitionStorage = new CountingTransitionStorage<>();
        return this;
    }

    @Override
    public IStateSpaceManager<C,A> parentTransitionStorage() {
        transitionStorage = new ParentTransitionStorage<>();
        return this;
    }

    @Override
    public IStateSpaceManager<C,A> fullTransitionStorage() {
        transitionStorage = new FullTransitionStorage<>();
        return this;
    }

    @Override
    public Set<C> initialConfigurations() {
        return initialConfigurations;
    }

    @Override
    public IFiredTransition putTransition(IFiredTransition<C, A> transition) {
        assert(transition.getTargets() != null && !transition.getTargets().isEmpty());
        //TODO: the transition storage should not store a pointer to the states, but an index. This way we can remove the configurations from memory
        return transitionStorage.put(transition);
    }

    @Override
    public synchronized C get(C state) {
        return configurationStorage.get(state);
    }

    @Override
    public synchronized C put(C state) {
        return configurationStorage.put(state);
    }

    @Override
    public synchronized C putInitial(C state) {
        initialConfigurations.add(state);
        return configurationStorage.put(state);
    }

    @Override
    public int size() {
        return configurationStorage.size();
    }

    @Override
    public int transitionCount() {return transitionStorage.size();}

    @Override
    public Collection<C> getInitialVertices() {
        //this makes sense in the context of model-checking: the initial configurations
        return initialConfigurations;
    }

    @Override
    public Collection<C> getSources() {
        //TODO: this should return only nodes without incomming transitions
        //the initial states could have incoming transitions
        return initialConfigurations;
    }

    @Override
    public Collection<C> getSinks() {
        //The worst case cost is :
        // O(size^2) -- with the ParentTransitionStorage
        // O(size) -- with the FullTransitionStorage
        Collection<C> result = new ArrayList<>();
        for (C reached : getVertices()) {
            if (getOutgoingEdges(reached).isEmpty()) {
                result.add(reached);
            }
        }
        return result;
    }

    @Override
    public Collection<C> getVertices() {
        return configurationStorage.getConfigurations();
    }

    @Override
    public C getParent(C vertex) {
        return transitionStorage.getParent(vertex);
    }

    @Override
    public Collection<C> fanout(C vertex) {
        Set<C> targets = new HashSet<>();
        for (IFiredTransition<C, A> fired : transitionStorage.getFiredFrom(vertex)) {
           targets.addAll(fired.getTargets());
        }
        return targets;
    }

    @Override
    public Collection<IFiredTransition<C, A>> getOutgoingEdges(C vertex) {
        return transitionStorage.getFiredFrom(vertex);
    }

    @Override
    public Collection<IFiredTransition<C, A>> getIncomingEdges(C vertex) {
        return transitionStorage.getFiredTo(vertex);
    }

    @Override
    public IGraphAccess<C, A> getGraphView() {
        return this;
    }

    @Override
    public int getVertexCount() {
        return size();
    }

    @Override
    public int getEdgeCount() {
        return transitionStorage.getEdgeCount();
    }
}


