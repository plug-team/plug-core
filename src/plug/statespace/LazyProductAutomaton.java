package plug.statespace;

import java.util.*;
import java.util.function.Supplier;

import plug.core.*;
import plug.core.IConcurrentTransitionRelation;

public class LazyProductAutomaton<C extends IConfiguration, F> implements IConcurrentTransitionRelation<LazyProductAutomaton<C, F>, C, F>, IProductAutomaton<C, F> {
    private final ITransitionRelation<C, F> runtime;
    private final IStateSpaceManager<C, F> stateSpaceManager;
    protected final Supplier metadataSupplier;

    public LazyProductAutomaton(ITransitionRelation<C, F> runtime, IStateSpaceManager<C, F> stateSpaceManager, Supplier metadataSupplier) {
        this.runtime = runtime;
        this.stateSpaceManager = stateSpaceManager;
        this.metadataSupplier = metadataSupplier;
    }

    @Override
    public LazyProductAutomaton<C, F> createCopy() {
        return new LazyProductAutomaton<>(((IConcurrentTransitionRelation<LazyProductAutomaton<C,F>,C,F>)runtime).createCopy(), stateSpaceManager, metadataSupplier);
    }

    @Override
    public boolean hasBlockingTransitions() {
        return runtime.hasBlockingTransitions();
    }

    @Override
    public Set<C> initialConfigurations() {
        Set<C> set = new HashSet<>();
        for (C c : initialConfigurationsIterable()) {
            set.add(c);
        }
        return set;
    }

    @Override
    public Collection<F> fireableTransitionsFrom(C source) {
        Collection<F> col = new ArrayList<>();
        for (F f : fireableTransitionsIterable(source)) {
            col.add(f);
        }
        return col;
    }

    @Override
    public IFiredTransition<C, ?> fireOneTransition(C source, F transition) {
        return runtime.fireOneTransition(source, transition);
    }

    @Override
    public Iterable<C> initialConfigurationsIterable() {
        return () -> new InitialConfigurationIterator(runtime.initialConfigurations());
    }

    @Override
    public Iterable<F> fireableTransitionsIterable(C source) {
        return () ->runtime.fireableTransitionsFrom(source).iterator();
    }

    @Override
    public Iterable<C> getPostIterable(C source) {
        return () -> new PostIterator(source);
    }

    class InitialConfigurationIterator implements Iterator<C> {
        Iterator<C> runtimeInitialConfigurationIterator;

        InitialConfigurationIterator(Set<C> initialConfigurations) {
            this.runtimeInitialConfigurationIterator = initialConfigurations.iterator();
        }

        @Override
        public boolean hasNext() {
            return runtimeInitialConfigurationIterator.hasNext();
        }

        @Override
        public C next() {
            C newNext = runtimeInitialConfigurationIterator.next();
            C next = stateSpaceManager.get(newNext);
            if (next == null) {
                stateSpaceManager.putInitial(next = newNext);
                next.setMetadata(metadataSupplier.get());
            }
            return next;
        }
    }

    public class PostIterator implements Iterator<C> {
        final C source;

        private Iterator<F> fireables;
        private Iterator<C> postIterator;
        public IFiredTransition currentFiredTransition;

        PostIterator(C source) {
            this.source = source;
            this.fireables = fireableTransitionsIterable(source).iterator();
        }

        @Override
        public boolean hasNext() {
            if (postIterator != null) {
                if (postIterator.hasNext()) {
                    return true;
                } else {
                    stateSpaceManager.putTransition(currentFiredTransition);
                }
            }
            if (fireables != null) {
                while (fireables.hasNext()) {
                    currentFiredTransition = fireOneTransition(source, fireables.next());
                    if (currentFiredTransition == null) continue;
                    postIterator = currentFiredTransition.getTargets().iterator();
                    if (postIterator.hasNext()) {
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public C next() {
            C newNext = postIterator.next();
            C next = stateSpaceManager.get(newNext);
            if (next == null) {
                newNext.setMetadata(metadataSupplier.get());
                stateSpaceManager.put(next = newNext);
            }
            return next;
        }
    }

    @Override
    public IAtomicPropositionsEvaluator getAtomicPropositionEvaluator() {
        return runtime.getAtomicPropositionEvaluator();
    }
}
