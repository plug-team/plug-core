package plug.statespace.transitions;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import plug.core.IFiredTransition;

public class FiredTransition<C, A> implements IFiredTransition<C, A> {
	C source;
	Collection<C> targets;
	A action;
	Object payload;
	
	public FiredTransition(C source, Collection<C> targets, A fired) {
		this(source, targets, fired, null);
	}

	public FiredTransition(C source, C target, A fired) {
		this(source, Collections.singleton(target), fired, null);
	}

	public FiredTransition(C source, C target, A fired, Object payload) {
		this(source, Collections.singleton(target), fired, payload);
	}

	public FiredTransition(C source, Collection<C> targets, A fired, Object payload) {
		this.source = source;
		this.targets = targets;
		this.action = fired;
		this.payload = payload;
	}
	
	@Override
	public C getSource() {
		return source;
	}

	@Override
	public A getAction() {
		return action;
	}

	@Override
	public Collection<C> getTargets() {
		return targets;
	}

	@Override
	public void setSource(C c) {
		source = c;
	}

	@Override
	public void setAction(A f) {
		action = f;
	}

	@Override
	public void setTargets(Collection<C> c) {
		targets = c;
	}

	@Override
	public Object getPayload() {
		return payload;
	}

	@Override
	public void setPayload(Object payload) {
		this.payload = payload;
	}
}
