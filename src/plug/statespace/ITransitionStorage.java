package plug.statespace;

import java.util.Collection;
import plug.core.IConfiguration;
import plug.core.IFiredTransition;

public interface ITransitionStorage<C extends IConfiguration, A> {
	/**
	 * This method is responsible for adding a tuple (source, action, target) to the transition storage backend
	 * */
	IFiredTransition put(IFiredTransition<C, A> transition);

	//this method returns the number of transitions in the graph which is smaller than number of simple edges
	int size();
	//This method return the number of simple edges in the graph
	int getEdgeCount();

	C getParent(C configuration);
	Collection<IFiredTransition<C, A>> getFiredFrom(C configuration);
	Collection<IFiredTransition<C, A>> getFiredTo(C configuration);
}
