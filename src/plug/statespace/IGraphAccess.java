package plug.statespace;

import plug.core.IFiredTransition;
import plug.utils.graph.IGraph;

/**
 * Created by ciprian on 16/02/17.
 */
public interface IGraphAccess<N, T> extends IGraph<N, IFiredTransition<N, T>>{
}
