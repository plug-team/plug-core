package plug.events;

import java.util.Set;
import plug.core.execution.IExecutionController;

public class ExecutionStartedEvent<C> extends AbstractEvent {
	Set<C> initialConfigurations;
	public ExecutionStartedEvent(IExecutionController source, Set<C> initialConfigurations) {
		super(source);
		this.initialConfigurations = initialConfigurations;
	}
	
	public Set<C> getInitialConfigurations() {
		return initialConfigurations;
	}
}
