package plug.events;

import plug.core.IFiredTransition;
import plug.core.execution.IExecutionController;


public class FiredEvent<C, A> extends AbstractEvent {
	private IFiredTransition<C, A> fired;
	
	public FiredEvent(IExecutionController source, IFiredTransition<C, A> fired) {
		super(source);
		this.fired = fired;
	}

	public IFiredTransition<C, A> getFiredTransition() {
		return fired;
	}
}
