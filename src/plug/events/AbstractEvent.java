package plug.events;

import plug.core.execution.IExecutionController;

public class AbstractEvent<C, A> {

	protected final IExecutionController source;
	
	public AbstractEvent(IExecutionController<C, A> source) {
		this.source = source;
	}
	
	public IExecutionController<C, A> getSource() {
		return source;
	}
}
