package plug.events;

import plug.core.execution.IExecutionController;

public class CloseConfigurationEvent<C> extends ConfigurationEvent<C> {

	public CloseConfigurationEvent(IExecutionController source, C configuration) {
		super(source, configuration);
	}
}
