package plug.events;

import plug.core.execution.IExecutionController;

public class OpenConfigurationEvent<C> extends ConfigurationEvent<C> {
	
	public OpenConfigurationEvent(IExecutionController source, C configuration) {
		super(source, configuration);
	}
}
