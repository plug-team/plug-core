package plug.events;

import plug.core.execution.IExecutionController;


public class ExecutionEndedEvent<C, A> extends AbstractEvent<C, A> {

	public ExecutionEndedEvent(IExecutionController source) {
		super(source);
	}
}
