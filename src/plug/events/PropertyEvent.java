package plug.events;

import java.util.List;
import plug.core.IConfiguration;
import plug.core.execution.IExecutionController;

public class PropertyEvent<C, A> extends AbstractEvent<C, A> {

	private final boolean verified;

	private final String property;

	private final List<IConfiguration> counterExample;

	public PropertyEvent(IExecutionController<C, A> source, boolean verified, String property, List<IConfiguration> counterExample) {
		super(source);
		this.verified = verified;
		this.property = property;
		this.counterExample = counterExample;
	}

	public boolean isVerified() {
		return verified;
	}

	public String getProperty() {
		return property;
	}

	public List<IConfiguration> getCounterExample() {
		return counterExample;
	}
}
