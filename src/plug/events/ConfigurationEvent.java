package plug.events;

import plug.core.execution.IExecutionController;

public class ConfigurationEvent<C> extends AbstractEvent {
	C configuration;

	public ConfigurationEvent(IExecutionController source, C configuration) {
		super(source);
		this.configuration = configuration;
	}
	
	public C getConfiguration() {
		return configuration;
	}
}
