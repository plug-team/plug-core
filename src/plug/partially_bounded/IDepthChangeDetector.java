package plug.partially_bounded;

import plug.core.IFiredTransition;

/**
 * Created by Ciprian TEODOROV on 03/04/17.
 */
public interface IDepthChangeDetector {
    boolean depthChanges(IFiredTransition transition);
}
