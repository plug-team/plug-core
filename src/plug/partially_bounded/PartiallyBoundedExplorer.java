package plug.partially_bounded;

import announce4j.Announcer;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import plug.core.IFiredTransition;
import plug.core.IProductAutomaton;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.core.execution.IExecutionMonitor;
import plug.events.CloseConfigurationEvent;
import plug.events.ExecutionEndedEvent;
import plug.events.ExecutionStartedEvent;
import plug.events.FiredEvent;
import plug.events.OpenConfigurationEvent;
import plug.statespace.configurations.FullConfigurationStorage;

/**
 * Created by Ciprian TEODOROV on 03/04/17.
 */
public class PartiallyBoundedExplorer<C, A> implements IExecutionController {

    protected final IExecutionMonitor.Simple monitor = new IExecutionMonitor.Simple();

    protected final FullConfigurationStorage stateSpaceManager;
    protected final ITransitionRelation runtime;

    public final Announcer announcer = new Announcer(true);

    protected final int targetDepth;
    protected int currentDepth;

    protected Queue toSee;
    protected FullConfigurationStorage nextStage;

    public PartiallyBoundedExplorer(ITransitionRelation runtime, int targetDepth) {
        this.runtime = runtime;
        this.stateSpaceManager = new FullConfigurationStorage();
        stateSpaceManager.setStorageBackend(new HashMap());

        this.targetDepth = targetDepth;
    }

    @Override
    public Announcer getAnnouncer() {
        return announcer;
    }

    @Override
    public IStateSpaceManager<C, A> getStateSpaceManager() {
        return null;
    }

    public ITransitionRelation getRuntime() {
        return runtime;
    }

    public IProductAutomaton getProductAutomaton() {
        return null;
    }

    @Override
    public IExecutionMonitor.Simple getMonitor() {
        return monitor;
    }

    void initializeExploration() {
        nextStage = new FullConfigurationStorage();
        nextStage.setStorageBackend(new HashMap());
        Set initialConfigurations = getRuntime().initialConfigurations();
        announcer.announce(new ExecutionStartedEvent<>(this, initialConfigurations));
        //stateSpaceManager[0].addIfAbsent(initialConfigurations);

        currentDepth = 0;
        toSee = new LinkedList<C>();
        toSee.addAll(initialConfigurations);

    }

    @Override
    public void execute() {
        initializeExploration();

        while (!atEnd()) {
            explorationStep();
        }

        announcer.announce(new ExecutionEndedEvent(this));
    }

    void explorationStep() {
        Object source = nextConfiguration();
        announcer.announce(new OpenConfigurationEvent<>(this, source));

        stateSpaceManager.put(source);

        Collection<?> fireable = getFireableTransitions(source);
        fire(fireable, source);
        announcer.announce(new CloseConfigurationEvent<>(this, source));
    }


    void fire(Collection<?> fireable, Object source) {
        for (Object transition : fireable) {
            if (monitor.atEnd()) return;
            IFiredTransition fired = getRuntime().fireOneTransition(source, transition);
            if (fired == null || fired.getTargets().isEmpty()) {
                //if the transition was blocked, go to the next
                continue;
            }
            //this announce should be before adding to the state-space because the verifiers could modify the targets
            announcer.announce(new FiredEvent<>(this, fired));
            if (!depthChanges(fired)) {
                if (!stateSpaceManager.contains(fired.getTarget(0))) {
                    scheduleNow(fired.getTarget(0));
                }
            } else {
                scheduleNext(fired.getTarget(0));
            }
        }
    }

    boolean depthChanges(IFiredTransition transition) {
        return ((IDepthChangeDetector)getRuntime()).depthChanges(transition);
    }

    Collection<?> getFireableTransitions(Object source) {
        return getRuntime().fireableTransitionsFrom(source);
    }

    public boolean atEnd() {
        if (toSee.isEmpty()) {
            //delete the state-space associated to the current step
            stateSpaceManager.setStorageBackend(new HashMap());

            if ( nextStage.size() == 0 ) {
                return true;
            }

            toSee.addAll(nextStage.getConfigurations());

            System.out.println("depth " + currentDepth);
            currentDepth++;
        }
        return currentDepth >= targetDepth;
    }

    public Object nextConfiguration() {
        return toSee.remove();
    }

    void scheduleNow(Object conf) {
        toSee.add(conf);
    }
    void scheduleNext(Object conf) {
        nextStage.put(conf);
    }
}
