package plug.folding.runtime;

import plug.core.IAtomicPropositionsEvaluator;
import plug.core.IFiredTransition;
import plug.core.ITransitionRelation;

import java.util.Collection;
import java.util.Set;

//This enables the exploration of a model starting from a set of given configurations
public class PreinitializedRuntime<C, A> implements ITransitionRelation<C, A> {
    private Set<C> initialConfigurations;
    private ITransitionRelation<C, A> baseRuntime;

    public PreinitializedRuntime(ITransitionRelation<C, A> baseRuntime, Set<C> initialConfigurations) {
        this.baseRuntime = baseRuntime;
        this.initialConfigurations = initialConfigurations;
    }

    @Override
    public Set<C> initialConfigurations() {
        return initialConfigurations;
    }

    @Override
    public Collection<A> fireableTransitionsFrom(C source) {
        return baseRuntime.fireableTransitionsFrom(source);
    }

    @Override
    public IFiredTransition<C, ?> fireOneTransition(C source, A transition) {
        return baseRuntime.fireOneTransition(source, transition);
    }

    @Override
    public boolean hasBlockingTransitions() {
        return baseRuntime.hasBlockingTransitions();
    }

    @Override
    public IAtomicPropositionsEvaluator getAtomicPropositionEvaluator() {
        return baseRuntime.getAtomicPropositionEvaluator();
    }
}
