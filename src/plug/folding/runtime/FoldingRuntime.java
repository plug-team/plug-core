package plug.folding.runtime;

import plug.core.IAtomicPropositionsEvaluator;
import plug.core.IFiredTransition;
import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.statespace.transitions.FiredTransition;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

public class FoldingRuntime<C, A extends Object> implements ITransitionRelation<C, A> {
    public ITransitionRelation<C, A> baseRuntime;
    Predicate<C> foldingPredicate;
    Function<ITransitionRelation<C,A>, IExecutionController<C, A>> controllerSupplier;

    public FoldingRuntime(ITransitionRelation<C, A> baseRuntime,
                          Predicate<C> foldingPredicate,
                          Function<ITransitionRelation<C,A>, IExecutionController<C, A>> controllerSupplier) {
        this.baseRuntime = baseRuntime;
        this.foldingPredicate = foldingPredicate;
        this.controllerSupplier = controllerSupplier;
    }

    @Override
    public Set<C> initialConfigurations() {
        return baseRuntime.initialConfigurations();
    }

    @Override
    public Collection<A> fireableTransitionsFrom(C source) {
//        if (foldingPredicate.test(source)) {
//            FoldingExecutionController controller = new FoldingExecutionController<>(source, baseRuntime, foldingPredicate, controllerSupplier);
//            controller.execute();
//            if (controller.getStateSpaceManager().size() > 50) {
//                System.out.println("Fold size : " + controller.getStateSpaceManager().size());
//            }
//            return Collections.singleton((A)new FoldedTransition<C>(source, controller.foldingOutputs));
//        }
        return baseRuntime.fireableTransitionsFrom(source);
    }

    @Override
    public IFiredTransition<C, ?> fireOneTransition(C source, A transition) {
//        if (transition instanceof FoldedTransition) {
//            return (IFiredTransition<C, ?>)transition;
//        }
//        return baseRuntime.fireOneTransition(source, transition);
        IFiredTransition<C, ?> fired = baseRuntime.fireOneTransition(source, transition);
        if (fired == null) return null;
        Set<C> targets = new HashSet<>();
        for (C target : fired.getTargets()) {
            if (foldingPredicate.test(target)) {
                FoldingExecutionController controller = new FoldingExecutionController<>(target, baseRuntime, foldingPredicate, controllerSupplier);
                controller.execute();
                if (controller.getStateSpaceManager().size() > 50) {
                    System.out.println("Fold size : " + controller.getStateSpaceManager().size());
                }
                targets.addAll(controller.foldingOutputs);
                //return Collections.singleton((A)new FoldedTransition<C>(source, controller.foldingOutputs));
            }
            else {
                targets.add(target);
            }
        }
        return new FiredTransition<>(source, targets, transition);
    }

    @Override
    public boolean hasBlockingTransitions() {
        return baseRuntime.hasBlockingTransitions();
    }

    @Override
    public IAtomicPropositionsEvaluator getAtomicPropositionEvaluator() {
        return baseRuntime.getAtomicPropositionEvaluator();
    }
}
