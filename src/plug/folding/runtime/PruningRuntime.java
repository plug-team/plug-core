package plug.folding.runtime;

import plug.core.IAtomicPropositionsEvaluator;
import plug.core.IFiredTransition;
import plug.core.ITransitionRelation;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.function.Predicate;

//this runtime prunes the state-space if the pruningPredicate evaluates to true on a configuration
//this prunes all children of a configuration
//TODO: a pruning runtime that prunes the targets of the fired transitions (fireOneTransition results)
public class PruningRuntime<C, A> implements ITransitionRelation<C, A> {
    private ITransitionRelation<C, A> baseLanguageRuntime;
    private Predicate<C> pruningPredicate;

    public PruningRuntime(ITransitionRelation<C, A> baseLanguageRuntime, Predicate<C> pruningPredicate) {
        this.baseLanguageRuntime = baseLanguageRuntime;
        this.pruningPredicate = pruningPredicate;
    }

    @Override
    public Set<C> initialConfigurations() {
        return baseLanguageRuntime.initialConfigurations();
    }

    @Override
    public Collection<A> fireableTransitionsFrom(C source) {
        if (pruningPredicate.test(source)) {
            return Collections.emptySet();
        }
        return baseLanguageRuntime.fireableTransitionsFrom(source);
    }

    @Override
    public IFiredTransition<C, ?> fireOneTransition(C source, A transition) {
        return baseLanguageRuntime.fireOneTransition(source, transition);
    }

    @Override
    public boolean hasBlockingTransitions() {
        return baseLanguageRuntime.hasBlockingTransitions();
    }

    @Override
    public IAtomicPropositionsEvaluator getAtomicPropositionEvaluator() {
        return baseLanguageRuntime.getAtomicPropositionEvaluator();
    }
}
