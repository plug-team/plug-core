package plug.folding.runtime;

import announce4j.Announcer;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import plug.core.IProductAutomaton;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.core.execution.IExecutionMonitor;
import plug.events.OpenConfigurationEvent;

public class FoldingExecutionController<C, A> implements IExecutionController<C, A> {

    protected final IExecutionMonitor.Simple monitor = new IExecutionMonitor.Simple();

    ITransitionRelation<C,A> baseRuntime;
    Function<ITransitionRelation<C,A>, IExecutionController<C, A>> controllerSupplier;
    IExecutionController<C, A> currentExecutionControler;
    Predicate<C> foldingPredicate;

    Set<C> foldingOutputs;

    public FoldingExecutionController(C configuration,
                                      ITransitionRelation<C, A> baseRuntime,
                                      Predicate<C> foldingPredicate,
                                      Function<ITransitionRelation<C,A>, IExecutionController<C, A>> controllerSupplier) {
        this.baseRuntime = baseRuntime;
        this.foldingPredicate = foldingPredicate;

        ITransitionRelation<C, A> initializedRuntime = new PreinitializedRuntime<>(baseRuntime, Collections.singleton(configuration));
        ITransitionRelation<C, A> prunningRuntime = new PruningRuntime<>(initializedRuntime, foldingPredicate.negate());
        currentExecutionControler = controllerSupplier.apply(prunningRuntime);
        currentExecutionControler.getAnnouncer().when(OpenConfigurationEvent.class, this::addFoldingOutput);
        foldingOutputs = new HashSet<>();
    }

    private void addFoldingOutput(Announcer ann, OpenConfigurationEvent<C> oce) {
        C config = oce.getConfiguration();
        if (foldingPredicate.negate().test(config)) {
            foldingOutputs.add(config);
        }
    }

    @Override
    public void execute() {
        currentExecutionControler.execute();
    }

    @Override
    public Announcer getAnnouncer() {
        return currentExecutionControler.getAnnouncer();
    }

    @Override
    public IStateSpaceManager<C, A> getStateSpaceManager() {
        return currentExecutionControler.getStateSpaceManager();
    }

    @Override
    public ITransitionRelation getRuntime() {
        return currentExecutionControler.getRuntime();
    }

    public IProductAutomaton getProductAutomaton() {
        return null;
    }

    @Override
    public IExecutionMonitor.Simple getMonitor() {
        return monitor;
    }
}
