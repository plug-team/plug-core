package plug.folding.runtime;

import plug.core.IFiredTransition;

import javax.lang.model.type.NullType;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class FoldedTransition<C> implements IFiredTransition<C, NullType>{
    C source;
    Collection<C> targets;

    public FoldedTransition(C source, Set<C> targets) {
        this.source = source;
        this.targets = targets;
    }

    @Override
    public C getSource() {
        return source;
    }

    @Override
    public void setSource(C configuration) {
        this.source = configuration;
    }

    @Override
    public Collection<C> getTargets() {
        return targets;
    }

    @Override
    public void setTargets(Collection<C> configuration) {
        this.targets = configuration;
    }

    @Override
    public NullType getAction() {
        return null;
    }

    @Override
    public void setAction(NullType action) { }

    @Override
    public Object getPayload() {
        return null;
    }

    @Override
    public void setPayload(Object payload) { }
}
