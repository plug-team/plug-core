package plug.disk_saving;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;

/**
 * Created by ciprian on 14/02/17.
 */
public class ConfigurationDiskStore {

    long PAGE_SIZE = 1<<20; //1 mb

    RandomAccessFile file;
    FileChannel fileChannel;
    MappedByteBuffer mem;

    byte content[] = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9};

    ConfigurationDiskStore(String filepath) {
        try {
            file = new RandomAccessFile(filepath, "rw");
            fileChannel = file.getChannel();
            remap(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    MappedByteBuffer remap(int startPosition) {
        try {
            mem = fileChannel.map(FileChannel.MapMode.READ_WRITE, startPosition, PAGE_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mem;
    }
    int baseAddress = 0;

    int write(byte[] content) {
        int offset;
        if (mem.capacity() - mem.position() > content.length + 4) {
            offset = mem.position();

        }
        else {
            baseAddress += mem.position();
            remap(baseAddress);

            offset = mem.position();
        }
        mem.putInt(content.length);
        mem.put(content);

        return offset;
    }

    Map<byte[], Integer> map = new LinkedHashMap<>();
    void writeMany () {
        for (int i = 0; i<100000; i++) {
            int offset = write(content);
            map.put(content.clone(), baseAddress + offset);
        }
        System.out.println("Content size: " + content.length);
        //System.out.println(map);
        int i = 0;
        for (Map.Entry<byte[], Integer> entry : map.entrySet()) {
            if (i == 120) {
                System.out.println(entry.getValue());
                try {
                    mem = fileChannel.map(FileChannel.MapMode.READ_ONLY, entry.getValue(), 1024);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println(mem.getInt());
                byte[] m = new byte[9];
                mem.get(m);
                System.out.println(Arrays.toString( m))
                ;
            }
            i++;
        }
    }

    public static void main(String args[]) {
        ConfigurationDiskStore cds = new ConfigurationDiskStore("test.bin");
        cds.writeMany();
    }
}
