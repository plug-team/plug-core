package plug.visualisation;

import plug.core.IFiredTransition;
import plug.statespace.IGraphAccess;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ciprian on 17/02/17.
 */
public class StateSpace2TGF<N, T> {

    public static final StateSpace2TGF instace = new StateSpace2TGF();

    public static void toTGF(IGraphAccess graphView, String filename) {
        try {
            instace.toTGF(graphView, false, false, filename);
        } catch (IOException e) {e.printStackTrace();}

    }

    public static void toTGF(IGraphAccess graphView, boolean vertexDetails, String filename) {
        try {
            instace.toTGF(graphView, vertexDetails, false, filename);
        } catch (IOException e) {e.printStackTrace();}

    }

    public void toTGF(IGraphAccess<N, T> graphView, boolean vertexDetails, boolean edgeDetails, String filename) throws IOException {
        BufferedWriter br = new BufferedWriter(new FileWriter(filename));
        toTGF(graphView, vertexDetails, edgeDetails, br);
    }

    public void toTGF(IGraphAccess<N, T> graphView, boolean vertexDetails, boolean edgeDetails, Writer writer) throws IOException {
        Map<N, Integer> idMap = new HashMap<>();
        writer.write("0 i\n");

        for (N vertex : graphView.getVertices()) {
            int id = getID(vertex, idMap);
            writer.write(id + " " + (vertexDetails?vertex.toString():"") + "\n");
        }
        writer.write("#\n");

        for (N vertex : graphView.getInitialVertices()) {
            writer.write("0 " + getID(vertex, idMap) + "\n");
        }

        for (N source : graphView.getVertices()) {
            int sourceID = getID(source, idMap);
            for (IFiredTransition<N, T> transition : graphView.getOutgoingEdges(source)) {
                String actionString = transition.getAction() == null ? "" : transition.getAction().toString();
                for (N target : transition.getTargets()) {
                    writer.write(sourceID + " " + getID(target, idMap) + " " + (edgeDetails ? actionString : "") + "\n");
                }
            }
        }
        writer.close();
    }

    int getID(N vertex, Map<N, Integer> idMap) {
        Integer id = idMap.get(vertex);
        if (id == null) {
            idMap.put(vertex, id = idMap.size() + 1); //start at 1; since we have a synthetic source with id 0
        }
        return id;
    }
}
