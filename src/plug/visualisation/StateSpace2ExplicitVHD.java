package plug.visualisation;

import org.apache.commons.lang3.reflect.FieldUtils;
import plug.core.IFiredTransition;
import plug.statespace.IGraphAccess;
import plug.utils.marshaling.Marshaller;

import java.io.*;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

public class StateSpace2ExplicitVHD<N, T> {
    public static final StateSpace2ExplicitVHD instace = new StateSpace2ExplicitVHD<>();

    public static void toVHD(IGraphAccess graphView, String modelName, String filename) {
        try {
            instace.toVHDL(graphView, modelName, filename);
        } catch (IOException e) {e.printStackTrace();}

    }

    public void toVHDL(IGraphAccess<N, T> graphView, String modelName, String filename) throws IOException {
        BufferedWriter br = new BufferedWriter(new FileWriter(filename));
        toVHDL(graphView, modelName, br);
    }

    void generateModelStructure(int nbStates, int nbTransitions, int nbInitial, int configurationSize, Writer writer) throws IOException {
        String structureString =
                "library IEEE;\n" +
                "use IEEE.STD_LOGIC_1164.ALL;\n" +
                "use WORK.explicit_params.ALL;\n" +
                "package model_structure is\n" +
                "    constant CONFIG_WIDTH : integer := "+configurationSize+";\n" +
                "    constant AB_PARAMS : T_MODEL_PARAMS := ("+nbStates+", "+nbTransitions+", "+nbInitial+", CONFIG_WIDTH);\n" +
                "\n" +
                "    subtype T_CONFIGURATION is std_logic_vector(CONFIG_WIDTH-1 downto 0);\n" +
                "    pure function config2lv(c : T_CONFIGURATION) return std_logic_vector;\n" +
                "    pure function lv2config(lv : std_logic_vector) return T_CONFIGURATION;\n" +
                "end package;\n" +
                "\n" +
                "package body model_structure is\n" +
                "    pure function config2lv(c : T_CONFIGURATION) return std_logic_vector is\n" +
                "    begin\n" +
                "    \treturn std_logic_vector(c);\n" +
                "    end function;\n" +
                "\n" +
                "    pure function lv2config(lv : std_logic_vector) return T_CONFIGURATION is\n" +
                "    begin\n" +
                "        return T_CONFIGURATION(lv);\n" +
                "    end function;\n" +
                "end package body;";

        writer.write(structureString);
    }

    public void toVHDL(IGraphAccess<N, T> graphView, String modelName, Writer inWriter) throws IOException {
        int nbStates = graphView.getVertices().size();
        int nbTransitions = 0;
        int nbInitial = graphView.getInitialVertices().size();
        int configurationSize = 0;

        File modelFile = File.createTempFile("model", Long.toString(System.currentTimeMillis()));
        BufferedWriter writer = new BufferedWriter(new FileWriter(modelFile));

        Map<N, Integer> idMap = new HashMap<>();
        writer.write("-- " + modelName + " state-space\n");
        writer.write("library IEEE;\n");
        writer.write("use IEEE.STD_LOGIC_1164.ALL;\n");
        writer.write("use WORK.explicit_structure.ALL;\n");
        writer.write("use WORK.model_structure.ALL;\n");

        writer.write("package model is\n");
        writer.write("\tconstant AB_MODEL : T_EXPLICIT := (\n");
        writer.write("\t\tstates => (\n");

        boolean first = true;
        for (N vertex : graphView.getVertices()) {
            int id = getID(vertex, idMap);
            if (first) {
                first = false;
            } else {
                writer.write(",\n");
            }
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                BufferedOutputStream bos = new BufferedOutputStream(baos);
                toBinary(vertex, 0, bos);
                bos.flush();

                String bits = "";
                byte bytes[] = (baos.toByteArray());
                for (int i = 0; i<bytes.length; i++) {
                    String s = String.format("%8s", Integer.toBinaryString(bytes[i] & 0xFF)).replace(' ', '0');
                    bits += s + "_";
                }
                int size = bytes.length * 8;
                if (configurationSize != 0 && configurationSize != size) {
                    System.err.println("VHDL generation does not support variable configuration size");
                    return;
                }
                configurationSize = bytes.length * 8;
                writer.write("\t\t\t" + id + " => B\"" + bits.substring(0, bits.length()-1) + "\"");
            } catch (IllegalAccessException iae) {}
        }
        writer.write("\t\t),\n");

        writer.write("\t\tinitial => (");
        int idx = 0;
        for (N vertex : graphView.getInitialVertices()) {
            if (idx != 0) {
                writer.write(", ");
            }
            writer.write(idx + " => " + getID(vertex, idMap));
            idx++;
        }
        writer.write("),\n");

        writer.write("\t\tfanout => (");
        first = true;
        String fanoutBaseString = "";
        int source_idx = 0;
        idx = 0;
        for (N source : graphView.getVertices()) {
            writer.write("\n\t\t\t");
            int sourceID = getID(source, idMap);
            if (sourceID == source_idx) {
                source_idx ++;
                if (!first) {
                    fanoutBaseString += ", ";
                }
                fanoutBaseString += idx;
            } else {
                throw new RuntimeException("Source nodes should be handled using the ID order, found " + sourceID + " expected " + source_idx);
            }
            for (IFiredTransition<N, T> transition : graphView.getOutgoingEdges(source)) {
                for (N target : transition.getTargets()) {
                    if (first) {
                        first = false;
                    } else {
                        writer.write(", ");
                    }
                    writer.write("" + getID(target, idMap));
                    idx++;
                }
            }
            writer.write("\t\t\t-- fanout(" +sourceID+ ")");
        }
        writer.write("\n\t\t),\n");
        writer.write("\t\tfanout_base => (" + fanoutBaseString + ", " + idx + ")\n");

        writer.write(");\n");
        writer.write("end package;\n");

        nbTransitions = idx;

        writer.close();
        BufferedReader br = new BufferedReader(new FileReader(modelFile));
        generateModelStructure(nbStates, nbTransitions, nbInitial, configurationSize, inWriter);

        inWriter.write("\n\n");
        br.lines().forEach(line -> {try {inWriter.write(line+"\n");} catch (IOException ignored) {}});

        inWriter.close();
    }

    int getID(N vertex, Map<N, Integer> idMap) {
        Integer id = idMap.get(vertex);
        if (id == null) {
            idMap.put(vertex, id = idMap.size());
        }
        return id;
    }

    Map<Class, Byte> classIntegerMap = new IdentityHashMap<>();
    String tab(int n) {
        String s = "";
        for (int i=0; i<n; i++) {
            s += "\t";
        }
        return s;
    }
    String toBinary(Object o, int level, OutputStream os) throws IllegalAccessException, IOException {
        Byte classID = classIntegerMap.get(o.getClass());
        if (classID == null) {
            if (classIntegerMap.size() > 127) {
                throw new RuntimeException("VHDL serialization does not support more than 127 classes");
            }
            classID = (byte)classIntegerMap.size();
            classIntegerMap.put(o.getClass(), classID);
        }

        //do not serialize the ID of the configuration class, nor for the Fiacre BehaviorConfiguration
        if (level != 0 && !o.getClass().getSuperclass().getSimpleName().equals("BehaviorConfiguration")) {
            Marshaller.writeByte(classID, os);
        }
//        System.out.println(tab(level) + "(" + o.getClass().getSimpleName() +" "+ classID + ")");
        level++;
        for (Field field : FieldUtils.getAllFields(o.getClass())) {
            if (field.getName().equals("metadata")) {
                continue;
            }
            if (level == 1 && o.getClass().getSimpleName().equals("FiacreConfiguration") && field.getName().equals("id")) {
                continue;
            }

            if (field.getType().isPrimitive()) {
                getPrimitive(field, o, level, os);
            } else if (field.getType().isArray()) {
                Object arrO = field.get(o);
                if (arrO == null) {
                    Marshaller.writeBoolean(false, os);
//                    System.out.println(tab(level) + "(" + field.getName() +  " null)");
                    continue;
                }
                int length = Array.getLength(arrO);
                Marshaller.writeBoolean(true, os);
                Marshaller.writeInt(length, os);
//                System.out.print(tab(level) + "(" + field.getName() + " " + length + " [");

                boolean isPrimitiveArray = true;
                if (arrO.getClass().getTypeName().equals("int[]")) {
                    for (int i = 0; i < length; i++) {
                        int element = Array.getInt(arrO, i);
                        Marshaller.writeInt(element, os);
//                        System.out.print(element + " ");
                    }
                } else if (arrO.getClass().getTypeName().equals("boolean[]")) {
                    for (int i = 0; i < length; i++) {
                        boolean element = Array.getBoolean(arrO, i);
                        Marshaller.writeBoolean(element, os);
//                        System.out.print(element + " ");
                    }
                } else if (arrO.getClass().getTypeName().equals("short[]")) {
                    for (int i = 0; i < length; i++) {
                        short element = Array.getShort(arrO, i);
                        Marshaller.writeShort(element, os);
//                        System.out.print(element + " ");
                    }
                } else if (arrO.getClass().getTypeName().equals("byte[]")) {
                    for (int i = 0; i < length; i++) {
                        byte element = Array.getByte(arrO, i);
                        Marshaller.writeByte(element, os);
//                        System.out.print(element + " ");
                    }
                } else if (arrO.getClass().getTypeName().equals("char[]")) {
                    for (int i = 0; i < length; i++) {
                        char element = Array.getChar(arrO, i);
                        Marshaller.writeChar(element, os);
//                        System.out.print(element + " ");
                    }
                } else if (arrO.getClass().getTypeName().equals("long[]")) {
                    for (int i = 0; i < length; i++) {
                        long element = Array.getLong(arrO, i);
                        Marshaller.writeLong(element, os);
//                        System.out.print(element + " ");
                    }
                } else if (arrO.getClass().getTypeName().equals("float[]")) {
                    for (int i = 0; i < length; i++) {
                        float element = Array.getFloat(arrO, i);
                        Marshaller.writeFloat(element, os);
//                        System.out.print(element + " ");
                    }
                } else if (arrO.getClass().getTypeName().equals("double[]")) {
                    for (int i = 0; i < length; i++) {
                        double element = Array.getDouble(arrO, i);
                        Marshaller.writeDouble(element, os);
//                        System.out.print(element + " ");
                    }
                } else {
                    isPrimitiveArray = false;
//                    System.out.println();
                    for (int i = 0; i < length; i++) {
                        Object element = Array.get(arrO, i);
                        toBinary(element, level+1, os);
                    }
                }
//                if (isPrimitiveArray) {
//                    System.out.println("])");
//                } else {
//                    System.out.println(tab(level) + "])");
//                }
            } else {
                toBinary(field.get(o), level + 1, os);
                //System.out.println("====" + tab(tab) + field.getName() + " " + field.getType());
            }
        }
        return "";
    }

    String getPrimitive(Field field, Object o, int level, OutputStream os) throws IllegalAccessException, IOException {
        Class clazz = field.getType();
        if (clazz.getSimpleName().equals("int")) {
            int value = field.getInt(o);
            Marshaller.writeInt(value, os);
            //System.out.println(tab(level) + "(" + field.getName() + " " + value + ")");
        } else
        if (clazz.getSimpleName().equals("boolean")) {
            boolean value = field.getBoolean(o);
            Marshaller.writeBoolean(value, os);
            //System.out.println(tab(level) + "(" + field.getName() + " " + value + ")");
        } else
        if (clazz.getSimpleName().equals("short")) {
            short value = field.getShort(o);
            Marshaller.writeShort(value, os);
            //System.out.println(tab(level) + "(" + field.getName() + " " + value + ")");
        } else
        if (clazz.getSimpleName().equals("byte")) {
            byte value = field.getByte(o);
            Marshaller.writeByte(value, os);
            //System.out.println(tab(level) + "(" + field.getName() + " " + value + ")");
        } else
        if (clazz.getSimpleName().equals("char")) {
            char value = field.getChar(o);
            Marshaller.writeChar(value, os);
            //System.out.println(tab(level) + "(" + field.getName() + " " + value + ")");
        } else
        if (clazz.getSimpleName().equals("long")) {
            long value = field.getLong(o);
            Marshaller.writeLong(value, os);
            //System.out.println(tab(level) + "(" + field.getName() + " " + value + ")");
        } else
        if (clazz.getSimpleName().equals("float")) {
            float value = field.getFloat(o);
            Marshaller.writeFloat(value, os);
            //System.out.println(tab(level) + "(" + field.getName() + " " + value + ")");
        } else
        if (clazz.getSimpleName().equals("double")) {
            double value = field.getDouble(o);
            Marshaller.writeDouble(value, os);
            //System.out.println(tab(level) + "(" + field.getName() + " " + value + ")");
        } else {
            System.out.println(tab(level) + "(" + field.getName() + " UNEXPECTED)");
        }
        return "";
    }
}
