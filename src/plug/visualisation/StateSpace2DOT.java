package plug.visualisation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import plug.core.IFiredTransition;
import plug.statespace.IGraphAccess;


/**
 * Transformation of StateSpace into a DOT graph.
 *
 * @author Valentin Besnard (created on 07/06/2017)
 */
public class StateSpace2DOT<N, T> {
    
    public static final StateSpace2DOT instance = new StateSpace2DOT();

    public static void toDOT(IGraphAccess graphView, boolean vertexDetails, String filename) {
        try {
            instance.toDOT(graphView, vertexDetails, false, filename);
        } catch (IOException e) {e.printStackTrace();}
    }
    
    
    public void toDOT(IGraphAccess<N, T> graphView, boolean vertexDetails, boolean edgeDetails, String filename) throws IOException {
        BufferedWriter br = new BufferedWriter(new FileWriter(filename));
        toDOT(graphView, vertexDetails, edgeDetails, br);
        
        // Send the text file to dot in order to generate the graphic
        Runtime runtime = Runtime.getRuntime();
        String[] cmds = new String[] {"dot", "-Tps", filename, "-o", filename.substring(0, filename.indexOf(".")) + ".eps"};
        runtime.exec(cmds);
    }

    public void toDOT(IGraphAccess<N, T> graphView, boolean vertexDetails, boolean edgeDetails,  Writer writer) throws IOException {
        Map<N, Integer> idMap = new HashMap<>();
        writer.write("digraph G {\n");
        
        // Write nodes
        writer.write("0 [shape=box, label=\"i\", style=filled, color=dodgerblue];\n");
        
        for (N vertex : graphView.getVertices()) {
            int id = getID(vertex, idMap);
            if(vertexDetails){
                writer.write(id + " [shape=box, style=filled, color=dodgerblue, label=\"" + vertex.toString() + "\"];\n");
            } else {
                writer.write(id + " [shape=box, style=filled, color=dodgerblue];\n");
            }
        }

        // Write transitions
        for (N vertex : graphView.getInitialVertices()) {
            writer.write("0 -> " + getID(vertex, idMap) + "\n");
        }
        
        for (N source : graphView.getVertices()) {
            int sourceID = getID(source, idMap);
            for (IFiredTransition<N, T> transition : graphView.getOutgoingEdges(source)) {
                for (N target : transition.getTargets()) {
                    writer.write(sourceID + " -> " + getID(target, idMap) + "\n");
                }
            }
        }
        writer.write("}\n");
        writer.close();
    }

    int getID(N vertex, Map<N, Integer> idMap) {
        Integer id = idMap.get(vertex);
        if (id == null) {
            idMap.put(vertex, id = idMap.size() + 1); //start at 1; since we have a synthetic source with id 0
        }
        return id;
    }

}
