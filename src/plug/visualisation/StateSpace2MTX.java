package plug.visualisation;

import plug.statespace.IGraphAccess;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ciprian on 18/02/17.
 */
public class StateSpace2MTX {
    public static final StateSpace2MTX instance = new StateSpace2MTX();
    public static void toMTX(IGraphAccess graphView, String filepath) {
        try {
            instance.toMtx(graphView, filepath);
        } catch (IOException e) {e.printStackTrace();}
    }

    public void toMtx(IGraphAccess graphView, String path) throws IOException {
        BufferedWriter br = new BufferedWriter(new FileWriter(path));
        toMtx(graphView, br);
    }
    public void toMtx(IGraphAccess graphView, Writer writer) throws IOException {
        writer.write("%%MatrixMarket matrix coordinate pattern general\n");
        writer.write((graphView.getVertexCount()+1) + " " + (graphView.getVertexCount()+1) + " " + (graphView.getEdgeCount()+graphView.getInitialVertices().size()) + "\n");

        Map<Object, Integer> idMap = new HashMap<>();

        //build edges between a synthetic source an the initial configurations.
        for (Object vertex : graphView.getInitialVertices()) {
            writer.write("1 " + getID(vertex, idMap) + "\n");
        }

        for (Object source : graphView.getVertices()) {
            int sourceID = getID(source, idMap);
            for (Object target : graphView.fanout(source)) {
                writer.write(sourceID + " " + getID(target, idMap) + "\n");
            }
        }
        writer.close();
    }

    int getID(Object vertex, Map<Object, Integer> idMap) {
        Integer id = idMap.get(vertex);
        if (id == null) {
            idMap.put(vertex, id = idMap.size() + 2); // MatrixMarket index should start at 1;
        }
        return id;
    }
}
