package plug.explorer;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import plug.core.IConfiguration;
import plug.core.IFiredTransition;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.events.CloseConfigurationEvent;
import plug.events.FiredEvent;
import plug.events.OpenConfigurationEvent;
import plug.simulation.selection.FireableTransitionsEvent;
import plug.simulation.selection.ISelectionPolicy;
import plug.simulation.selection.InteractiveSelectionPolicy;

@Deprecated
public class FancySimulator<C extends IConfiguration, A> extends AbstractExplorer<C, A> {
    //The toSee is composed of two sets of configurations
    // - the first set is the source set (from which one or more configurations are selected as source)
    // - the second set is the target set (to which the results of firing transitions from sources are stored)
    // at the end of each simulation round the sets are swapped.
    Set<C> toSee[];
    int currentToSeeID = 0;

    Queue<C> selectedSourceConfigurations;

    //this records each simulation step in order, irespective of the choice of source configurations
    public LinkedList<TraceEntry<C, A>> linearTrace;
    int currentStepID = 0;
    //this stores a tree of configurations
    public Map<C, Set<C>> treeTrace;

    private ISelectionPolicy policy = new InteractiveSelectionPolicy();
    IConfigurationSelectionPolicy configurationSelectionPolicy = new MockConfigurationSelectionPolicy();

    public FancySimulator(ITransitionRelation<C, ?> runtime, IStateSpaceManager<C, A> stateSpaceManager) {
        super(runtime, stateSpaceManager);
    }

    public ISelectionPolicy getPolicy() {
        return policy;
    }

    public void setPolicy(ISelectionPolicy policy) {
        this.policy = policy;
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public void initializeExploration() {
        super.initializeExploration();
        toSee = new Set[2];
        toSee[0] = Collections.newSetFromMap(new HashMap<C, Boolean>());
        toSee[1] = Collections.newSetFromMap(new HashMap<C, Boolean>());

        //initialize the simulation with all the initial configurations
        toSee[currentToSeeID].addAll(stateSpaceManager.initialConfigurations());
    }

    @Override
    public boolean atEnd() {
        return toSee[currentToSeeID].isEmpty()
                && toSee[1-currentToSeeID].isEmpty()
                && selectedSourceConfigurations.isEmpty();
    }

    @Override
    public C nextConfiguration() {
        return selectedSourceConfigurations.remove();
    }

    @Override
    public void schedule(C conf) {
        C target = stateSpaceManager.get(conf);
        if (target == null) {
            target = stateSpaceManager.put(conf);
        }

        toSee[1-currentToSeeID].add(target);
    }

    @Override
    public void explorationStep() {
        //selectedSourceConfigurations should be empty because it was emptied during the last simulation step?
        //clear it to be sure
        selectedSourceConfigurations.clear();
        selectedSourceConfigurations.addAll(configurationSelectionPolicy.selectConfigurations(toSee[currentToSeeID]));
        //empty the toSee
        toSee[currentToSeeID].clear();

        //TODO: what should we present to the user?
        // - all transitions from all selected sources ?
        // - all the transitions grouped by source ?
        // - transitions per source as we pop them off the selectedSourceConfigurations ?
        //TODO: for now we show transitions as we pop the sources of the selected source list
        while (!selectedSourceConfigurations.isEmpty()) {
            C source = nextConfiguration();
            announcer.announce(new OpenConfigurationEvent<>(this, source));
            Collection<?> fireables = getFireableTransitions(source);

            announcer.announce(new FireableTransitionsEvent<>(fireables));

            fire(policy.chooseTargets(fireables), source);

            announcer.announce(new CloseConfigurationEvent<>(this, source));
        }

        currentToSeeID = 1-currentToSeeID;
        currentStepID++;
    }

    @SuppressWarnings( "unchecked" )
    private Collection<?> getFireableTransitions(C source) {
        return getRuntime().fireableTransitionsFrom(source);
    }

    public void fire(Collection<?> fireable, C source) {
        for (Object transition : fireable) {
            if (monitor.atEnd()) return;
            @SuppressWarnings( "unchecked" )
            IFiredTransition<C, A> fired = getRuntime().fireOneTransition(source, transition);
            announcer.announce(new FiredEvent<>(this, fired));
            stateSpaceManager.putTransition(fired);

            //we schedule it anyway so that the simulation continues
            fired.getTargets().forEach(this::schedule);
        }
    }

    //TODO: this method might not be needed
    public Set<C> currentState() {
        return toSee[currentToSeeID];
    }

    //TODO: this method might not be needed
    public void addState(C newState) {
        toSee[1-currentToSeeID].add(newState);
    }



    /**
     * Created by Ciprian TEODOROV on 24/02/17.
     */
    @Deprecated
    protected static class TraceEntry<C, A> {
        int stepID; //the step ID of this simulation step
        Set<C> sourceConfigurations; //the configuration from which this step was executed
        Set<A> actions; //the actions executed during this step
        Set<C> targetConfigurations; //the target configurations obtained after executing the actions
    }
    @Deprecated
    protected interface IConfigurationSelectionPolicy {
        <C> Set<C> selectConfigurations(Set<C> potentialSources);
    }
    @Deprecated
    protected static class MockConfigurationSelectionPolicy implements IConfigurationSelectionPolicy {

        @Override
        public <C> Set<C> selectConfigurations(Set<C> potentialSources) {
            //TODO: this returns all potentials for testing
            return potentialSources;
        }
    }
}
