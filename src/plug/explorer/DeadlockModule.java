package plug.explorer;

import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.verifiers.deadlock.DeadlockVerifier;

public class DeadlockModule {

	public String getName() {
		return "Deadlocks";
	}

	public BFSExplorer newExecutionController(ITransitionRelation runtime, IStateSpaceManager stateSpaceManager) {
		BFSExplorer explorer = new BFSExplorer(runtime, stateSpaceManager);
		new DeadlockVerifier(explorer.getAnnouncer());
		return explorer;
	}

}
