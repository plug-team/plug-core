package plug.explorer;

import java.util.Collection;
import plug.core.IConfiguration;
import plug.core.IFiredTransition;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.events.CloseConfigurationEvent;
import plug.events.FiredEvent;
import plug.events.OpenConfigurationEvent;
import plug.simulation.selection.FireableTransitionsEvent;
import plug.simulation.selection.ISelectionPolicy;
import plug.simulation.selection.InteractiveSelectionPolicy;


/**
 * Created by Ciprian TEODOROV on 16/02/16.
 * This is a dead simple simulator:
 * - no multiple transition firing
 * - no support for multiple targets (if one transition generates multiple targets, ex synchro)
 */

public class Simulator<C extends IConfiguration, A> extends AbstractExplorer<C, A> {
	C toSee;
	private ISelectionPolicy policy = new InteractiveSelectionPolicy();

	public Simulator(ITransitionRelation<C, ?> runtime, IStateSpaceManager<C, A> stateSpaceManager) {
		super(runtime, stateSpaceManager);
	}

	public ISelectionPolicy getPolicy() {
		return policy;
	}

	public void setPolicy(ISelectionPolicy policy) {
		this.policy = policy;
	}

	@Override
	public void initializeExploration() {
		super.initializeExploration();
		toSee = stateSpaceManager.initialConfigurations().stream().iterator().next();
	}

	@Override
	boolean atEnd() {
		return toSee == null;
	}

	@Override
	C nextConfiguration() {
		return toSee;
	}

	@Override
	void schedule(C conf) {
		C target = stateSpaceManager.get(conf);
		if (target == null) {
			target = stateSpaceManager.put(conf);
		}
		//this will keep the last scheduled configuration as the next target
		toSee = target;
	}

	@Override
	public void explorationStep() {
		C source = nextConfiguration();
		announcer.announce(new OpenConfigurationEvent<>(this, source));
		Collection<?> fireables = getRuntime().fireableTransitionsFrom(source);

		announcer.announce(new FireableTransitionsEvent<>(fireables));

		fire (policy.chooseTargets(fireables), source);

		announcer.announce(new CloseConfigurationEvent<>(this, source));
	}

	public void fire(Collection<?> fireable, C source) {
		for (Object transition : fireable) {
			if (monitor.atEnd()) return;
			IFiredTransition<C, A> fired = getRuntime().fireOneTransition(source, transition);
			announcer.announce(new FiredEvent<>(this, fired));
			stateSpaceManager.putTransition(fired);

			//we schedule it anyway so that the simulation continues
			fired.getTargets().forEach(this::schedule);
		}
	}

}
