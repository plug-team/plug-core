package plug.explorer.buchi.nested_dfs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import plug.core.IBuchiConfiguration;
import plug.core.IConfiguration;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.events.ExecutionEndedEvent;
import plug.events.PropertyEvent;
import plug.explorer.buchi.DefaultExecutionController;


import static plug.explorer.buchi.nested_dfs.Color.*;

/*
 *   Gaiser, Andreas, and Stefan Schwoon. "Comparison of algorithms for checking emptiness on Büchi automata." arXiv preprint arXiv:0910.3766 (2009).
 *   @url(http://www.lsv.fr/Publis/PAPERS/PDF/GS-memics09.pdf)
 *   @verify is the iterative version of the algorithm presented in the paper (Fig. 1)
 */

//TODO: Announce progress information
//TODO: how to extract the counter-example ?
//TODO: this does not care about the state-space at all, however it supposes that the states are already in. (can we offer an intermediate API doing that?)
public class BA_GaiserSchwoon_Iterative<C extends IBuchiConfiguration & IConfiguration, A> extends DefaultExecutionController<C, A> {

    protected boolean propertyFailed = false;

    protected final Stack<StackEntry<C>> redStack = new Stack<>(); //stack for acceptance check

    protected final Stack<StackEntry<C>> blueStack = new Stack<>(); //stack for reachability

    public BA_GaiserSchwoon_Iterative(ITransitionRelation<C, A> runtime, IStateSpaceManager stateSpaceManager) {
         super(runtime, stateSpaceManager);
    }

    @Override
    public Supplier getMetadataSupplier() {
        return ()->Color.WHITE;
    }

    @Override
    public void execute() {
        try {
            for (C c : getProductAutomaton().initialConfigurationsIterable()) {
                dfs_blue(c);
            }
        } finally {
            if (!propertyFailed) {
                announcer.announce(new PropertyEvent(this, true, "property", null));
            }
            announcer.announce(new ExecutionEndedEvent(this));
        }
    }

    protected void dfs_blue(C source) {
        blueStack.push(new StackEntry<>(source));

        while (!monitor.atEnd() && !blueStack.isEmpty() && !propertyFailed) {
            StackEntry<C> entry = blueStack.peek();
            Iterator<C> postIterator = entry.getPostIterator();
            if (postIterator == null) {
                entry.allRed = true;        //set allRed = true
                setColor(entry.config, CYAN);  //mark the state open
                //get the post(s)
                entry.setPostIterator(postIterator = getProductAutomaton().getPostIterable(entry.config).iterator());
            }

            C target = null;
            if (postIterator.hasNext()) {
                target = postIterator.next();

                if (getColor(target) == WHITE) {
                    blueStack.push(new StackEntry<>(target)); //start DFS Blue
                } else {
                    if (getColor(target) == CYAN && (source.isAccepting() || target.isAccepting())) {
                        //cycle found
                        propertyFailed = true;
                        announcer.announce(new PropertyEvent(this, false, "property", counterExample(target)));
                        break;
                    }
                    if (getColor(target) != RED) {
                        blueStack.peek().allRed = false;
                    }
                }
            } else {
                //all children done
                //remove child from the stack
                StackEntry<C> child = blueStack.pop();
                //if child is not red, then the parent is not red either
                if (getColor(child.config) != RED && !blueStack.isEmpty()) {
                    blueStack.peek().allRed = false;
                }

                if (entry.allRed) {
                    setColor(entry.config, RED);
                } else if (entry.config.isAccepting()) {
                    dfs_red(entry.config);
                    setColor(entry.config, RED);
                } else {
                    setColor(entry.config, BLUE);
                }
            }
        }
    }

    protected void dfs_red(C source) {
        redStack.push(new StackEntry<>(source));
        while (!monitor.atEnd() && !redStack.isEmpty()) {
            StackEntry<C> entry = redStack.peek();
            Iterator<C> postIterator = entry.getPostIterator();
            if (postIterator == null) {
                entry.setPostIterator(postIterator = getProductAutomaton().getPostIterable(entry.config).iterator());
            }

            C target = null;
            if (postIterator.hasNext()) {
                target = postIterator.next();

                if (getColor(target) == CYAN) {
                    //cycle found
                    announcer.announce(new PropertyEvent(this, false, "property", counterExample(target)));
                    propertyFailed = true;
                    break;
                } else if (getColor(target) == BLUE) {
                    setColor(target, RED);
                    redStack.push(new StackEntry<>(target));
                }
            } else {
                //all children done
                redStack.pop();
            }
        }
    }

    protected Color getColor(C configuration) {
        return (Color) configuration.getMetadata();
    }

    protected void setColor(C configuration, Color color) {
        configuration.setMetadata(color);
    }

    protected List<IConfiguration> counterExample(C target) {
        List<IConfiguration> result = new ArrayList<>();
        result.addAll(redStack.stream().map(e -> e.config).collect(Collectors.toList()));
        result.addAll(blueStack.stream().map(e -> e.config).collect(Collectors.toList()));
        result.add(target);
        return result;
    }

    protected static class StackEntry<C> {

        protected final C config;
        boolean allRed = true;
        private Iterator<C> postIterator;

        public StackEntry(C configuration) {
            this.config = configuration;
        }

        public void setPostIterator(Iterator<C> postIterator) {
            this.postIterator = postIterator;
        }

        public Iterator<C> getPostIterator() {
            return postIterator;
        }
    }
}

