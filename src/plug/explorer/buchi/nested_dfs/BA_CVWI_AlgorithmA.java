package plug.explorer.buchi.nested_dfs;

/*
* Courcoubetis, Costas, et al. "Memory-efficient algorithms for the verification of temporal properties." Formal methods in system design 1.2-3 (1992): 275-288.
* */

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.function.Supplier;
import plug.core.IBuchiConfiguration;
import plug.core.IConfiguration;
import plug.core.ITransitionRelation;
import plug.events.ExecutionEndedEvent;
import plug.events.PropertyEvent;
import plug.explorer.buchi.DefaultExecutionController;

public class BA_CVWI_AlgorithmA <C extends IBuchiConfiguration & IConfiguration, A> extends DefaultExecutionController<C, A> {

    public BA_CVWI_AlgorithmA(ITransitionRelation<C, A> runtime) {
        super(runtime);
    }

    @Override
    public Supplier getMetadataSupplier() {
        return ()->false;
    }

    @Override
    public void execute() {
        for (C initial : getProductAutomaton().initialConfigurationsIterable()) {
            theS.push(new StackEntry(initial, getProductAutomaton().getPostIterable(initial)));
            dfs();
        }

        //theS is empty at the end of the first DFS run
        secondDFS();
        announcer.announce(new ExecutionEndedEvent(this));
    }

    Stack<StackEntry> theS = new Stack<>();
    Queue<C> theQ = new LinkedList<>();

    void dfs() {
        while (!monitor.atEnd() && !theS.isEmpty()) {
            StackEntry entry = theS.peek();

            boolean childrenProcessed = true;
            C target = null;
            while (entry.post.hasNext()) {
                target = entry.post.next();
                if (!getM(target)) {
                    childrenProcessed = false;
                    break;
                }
            }

            if (childrenProcessed) {
                theS.pop();
                if (entry.state.isAccepting()) {
                    theQ.add(entry.state);
                }
            } else {
                setM(target);
                theS.push(new StackEntry(target, getProductAutomaton().getPostIterable(target)));
            }
        }
    }

    void secondDFS() {
        while (!monitor.atEnd() && !theQ.isEmpty()) {
            C f = theQ.remove();
            theS.push(new StackEntry(f, getProductAutomaton().getPostIterable(f)));

            while (!theS.isEmpty()) {
                StackEntry entry = theS.peek();

                boolean childrenProcessed = true;
                C target = null;
                while (entry.post.hasNext()) {
                    target = entry.post.next();
                    //The algorithm presented does if f ∈ succ(v), before iterating -- this is costly
                    //Here we check if one by one, if it is not f then we go to its children
                    if (f == target) {
                        //cycle found
                        System.out.println("target = [" + target + "]");
                        //TODO: where is the counter example ?
                        announcer.announce(new PropertyEvent(this, false, "property", null));
                    }

                    if (getM(target)) {
                        childrenProcessed = false;
                        break;
                    }
                }

                if (childrenProcessed) {
                    theS.pop();
                } else {
                    resetM(target);
                    theS.push(new StackEntry(target, getProductAutomaton().getPostIterable(target)));
                }
            }
        }
    }

    boolean getM(C state) {
        return (boolean)state.getMetadata();
    }

    void setM(C state) {
        state.setMetadata(true);
    }

    void resetM(C state) {
        state.setMetadata(false);
    }

    private class StackEntry {
        C state;
        Iterator<C> post;
        StackEntry(C state, Iterable<C> post) {
            this.state = state;
            this.post = post.iterator();
        }
    }
}
