package plug.explorer.buchi.nested_dfs;

import java.util.function.Supplier;
import plug.core.IBuchiConfiguration;
import plug.core.IConfiguration;
import plug.core.ITransitionRelation;
import plug.events.ExecutionEndedEvent;
import plug.events.PropertyEvent;
import plug.explorer.buchi.DefaultExecutionController;

/*
 * Geldenhuys, Jaco, and Antti Valmari. "More efficient on-the-fly LTL verification with Tarjan's algorithm."
 * Theoretical Computer Science 345.1 (2005): 60-82.
 *
 * @verify is the recursive version of the CVWI algorithm presented in the paper (Fig. 1)
 * */

//TODO: this does not care about the state-space at all, however it supposes that the states are already in. (can we offer an intermediate API doing that?)
public class BA_CVWI_Recursive <C extends IBuchiConfiguration & IConfiguration, A> extends DefaultExecutionController<C, A> {

    public BA_CVWI_Recursive(ITransitionRelation<C, A> runtime) {
        super(runtime);
    }

    @Override
    public Supplier getMetadataSupplier() {
        return ()->Color.WHITE;
    }

    @Override
    public void execute() {
        try {
            for (C initial : getProductAutomaton().initialConfigurationsIterable()) {
                dfs(initial);
            }
        } catch (RuntimeException e) {
            announcer.announce(new ExecutionEndedEvent(this));
        }
    }

    C seed;

    public void dfs(C source) {
        setColor(source, Color.RED);

        for (C target : getProductAutomaton().getPostIterable(source)) {
            if (monitor.atEnd()) break;

            if (getColor(target) != Color.RED) {
                dfs(target);
            }
        }
        if (source.isAccepting()) {
            seed = source;
            ndfs(source);
        }
    }

    public void ndfs(C source) {
        setColor(source, Color.BLUE);

        for (C target : getProductAutomaton().getPostIterable(source)) {
            if (monitor.atEnd()) break;

            if (getColor(target) != Color.BLUE) {
                ndfs(target);
            } else if (target == seed) {
                //cycle found
                System.out.println("target = [" + target + "]");
                //TODO: the counter-example is on the JAVA stack -- print it by reflection
                announcer.announce(new PropertyEvent(this, false, "property", null));
                throw new RuntimeException("Break execution flow");
            }
        }
    }

    private Color getColor(C configuration) {
        return (Color) configuration.getMetadata();
    }

    private void setColor(C configuration, Color color) {
        configuration.setMetadata(color);
    }
}
