package plug.explorer.buchi.nested_dfs;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;
import java.util.function.Supplier;
import plug.core.IBuchiConfiguration;
import plug.core.IConfiguration;
import plug.core.ITransitionRelation;
import plug.events.ExecutionEndedEvent;
import plug.events.PropertyEvent;
import plug.explorer.buchi.DefaultExecutionController;

/*
 * Courcoubetis, Costas, et al. "Memory-efficient algorithms for the verification of temporal properties." Formal methods in system design 1.2-3 (1992): 275-288.
 * */

public class BA_CVWI_AlgorithmB<C extends IBuchiConfiguration & IConfiguration, A> extends DefaultExecutionController<C, A> {

    public BA_CVWI_AlgorithmB(ITransitionRelation<C, A> runtime) {
        super(runtime);
    }

    @Override
    public Supplier getMetadataSupplier() {
        return ()->false;
    }

    @Override
    public void execute() {
        for (C initial : getProductAutomaton().initialConfigurationsIterable()) {
            theS1.push(new StackEntry(initial, getProductAutomaton().getPostIterable(initial)));
            dfs();
        }
        announcer.announce(new ExecutionEndedEvent(this));
    }

    Stack<StackEntry> theS1 = new Stack<>();
    Stack<StackEntry> theS2 = new Stack<>();
    Set<C> theM2 = new HashSet<>();

    void dfs() {
        while (!monitor.atEnd() && !theS1.isEmpty()) {
            StackEntry entry = theS1.peek();

            boolean childrenProcessed = true;
            C target = null;
            while (entry.post.hasNext()) {
                target = entry.post.next();
                if (!getM(target)) {
                    childrenProcessed = false;
                    break;
                }
            }

            if (!childrenProcessed) {
                setM(target);
                theS1.push(new StackEntry(target, getProductAutomaton().getPostIterable(target)));
            } else {
                StackEntry theX = theS1.pop();
                if (theX.state.isAccepting()) {
                    theS2.push(new StackEntry(theX.state, getProductAutomaton().getPostIterable(theX.state)));

                    while (!theS2.isEmpty()) {
                        StackEntry theV = theS2.peek();

                        boolean childrenProcessed2 = true;
                        C target2 = null;
                        while (theV.post.hasNext()) {
                            target2 = theV.post.next();
                            //The algorithm presented does if f ∈ succ(v), before iterating -- this is costly
                            //Here we check if one by one, if it is not f then we go to its children
                            if (theX.state == target2) {
                                //cycle found
                                System.out.println("target = [" + target2 + "]");
                                //TODO: where is the counter example ?
                                announcer.announce(new PropertyEvent(this, false, "property", null));
                            }

                            if (!theM2.contains(target2)) {
                                childrenProcessed2 = false;
                                break;
                            }
                        }

                        if (childrenProcessed2) {
                            theS2.pop();
                        } else {
                            theM2.add(target2);
                            theS2.push(new StackEntry(target2, getProductAutomaton().getPostIterable(target2)));
                        }
                    }
                }
            }
        }
    }

    boolean getM(C state) {
        return (boolean)state.getMetadata();
    }

    void setM(C state) {
        state.setMetadata(true);
    }

    private class StackEntry {
        C state;
        Iterator<C> post;
        StackEntry(C state, Iterable<C> post) {
            this.state = state;
            this.post = post.iterator();
        }
    }
}
