package plug.explorer.buchi.nested_dfs;

import java.util.function.Supplier;
import plug.core.IBuchiConfiguration;
import plug.core.IConfiguration;
import plug.core.ITransitionRelation;
import plug.events.ExecutionEndedEvent;
import plug.events.PropertyEvent;
import plug.explorer.buchi.DefaultExecutionController;


import static plug.explorer.buchi.nested_dfs.Color.*;

/*
 *   Gaiser, Andreas, and Stefan Schwoon. "Comparison of algorithms for checking emptiness on Büchi automata." arXiv preprint arXiv:0910.3766 (2009).
 *   @url(http://www.lsv.fr/Publis/PAPERS/PDF/GS-memics09.pdf)
 *   @verify is the recursive version of the algorithm presented in the paper (Fig. 1)
 */

//TODO: Announce progress information
//TODO: how to extract the counter-example ?
//TODO: this does not care about the state-space at all, however it supposes that the states are already in. (can we offer an intermediate API doing that?)
public class BA_GaiserSchwoon_Recursive<C extends IBuchiConfiguration & IConfiguration, A> extends DefaultExecutionController<C, A> {

    public BA_GaiserSchwoon_Recursive(ITransitionRelation<C, A> runtime) {
        super(runtime);
    }

    @Override
    public Supplier getMetadataSupplier() {
        return ()->Color.WHITE;
    }

    @Override
    public void execute() {
        for (C initial : getProductAutomaton().initialConfigurationsIterable()) {
            recursive_dfs_blue(initial);
        }
        announcer.announce(new ExecutionEndedEvent(this));
    }

    private void recursive_dfs_blue(C source) {
        boolean allRed = true;
        setColor(source, CYAN);

        for (C target : getProductAutomaton().getPostIterable(source)) {
            if (monitor.atEnd()) break;

            if (getColor(target) == WHITE) {
                recursive_dfs_blue(target);
            } else if (getColor(target) == CYAN && (source.isAccepting() || target.isAccepting())) {
                //cycle found
                System.out.println("source = [" + source + "], target =[" + target + "]");
                //TODO: the counter-example is on the JAVA stack -- print it by reflection
                announcer.announce(new PropertyEvent(this, false, "property", null));
            }
            if (getColor(target) != RED) {
                allRed = false;
            }
        }

        //all children done
        if (allRed) {
            setColor(source, RED);
        } else if (source.isAccepting()) {
            recursive_dfs_red(source);
            setColor(source, RED);
        } else {
            setColor(source, BLUE);
        }
    }

    private void recursive_dfs_red(C source) {
        for (C target : getProductAutomaton().getPostIterable(source)) {
            if (monitor.atEnd()) break;

            if (getColor(target) == CYAN) {
                //cycle found
                System.out.println("target = [" + target + "]");
                //TODO: the counter-example is on the JAVA stack -- print it by reflection
                announcer.announce(new PropertyEvent(this, false, "property", null));
            } else if (getColor(target) == BLUE) {
                setColor(target, RED);
                recursive_dfs_red(target);
            }
        }
        //all children done
    }

    private Color getColor(C configuration) {
        return (Color) configuration.getMetadata();
    }

    private void setColor(C configuration, Color color) {
        configuration.setMetadata(color);
    }
}

