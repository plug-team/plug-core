package plug.explorer.buchi.nested_dfs;

public enum Color {
    CYAN, //state is opened
    RED,  //state is checked
    BLUE, //state is explored but not checked
    WHITE //new unexplored state
}
