package plug.explorer.buchi;

import announce4j.Announcer;
import java.util.function.Supplier;
import plug.core.IConfiguration;
import plug.core.IProductAutomaton;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.core.execution.IExecutionMonitor;
import plug.statespace.LazyProductAutomaton;
import plug.statespace.SimpleStateSpaceManager;

public abstract class DefaultExecutionController<C extends IConfiguration, A> implements IExecutionController<C, A> {

    protected final IStateSpaceManager<C, A> stateSpaceManager;

    protected final IExecutionMonitor.Simple monitor = new IExecutionMonitor.Simple();

    protected final Announcer announcer = new Announcer(true);

    protected final IProductAutomaton<C, A> productAutomaton;

    public DefaultExecutionController(ITransitionRelation<C, A> runtime) {
        this(runtime, new SimpleStateSpaceManager<>());
    }

    public DefaultExecutionController(ITransitionRelation<C, A> runtime, IStateSpaceManager stateSpaceManager) {
        this.stateSpaceManager = stateSpaceManager;
        this.productAutomaton = new LazyProductAutomaton(runtime, stateSpaceManager, this.getMetadataSupplier());
    }

    @Override
    public Announcer getAnnouncer() {
        return announcer;
    }

    @Override
    public IExecutionMonitor.Simple getMonitor() {
        return monitor;
    }

    @Override
    public IStateSpaceManager<C, A> getStateSpaceManager() {
        return stateSpaceManager;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ITransitionRelation<C, A> getRuntime() {
        return (ITransitionRelation<C, A>) productAutomaton;
    }

    public IProductAutomaton<C, A> getProductAutomaton() {
        return productAutomaton;
    }

    public abstract Supplier getMetadataSupplier();
}
