package plug.explorer.buchi.scc;

/*
 *   Gaiser, Andreas, and Stefan Schwoon. "Comparison of algorithms for checking emptiness on Büchi automata." arXiv preprint arXiv:0910.3766 (2009).
 *   @url(http://www.lsv.fr/Publis/PAPERS/PDF/GS-memics09.pdf)
 *   @The Amendment of Couvreur's algorithm -- recursive
 *   @verify is the recursive version of the algorithm presented in the paper (Fig. 2)
 *   This algo works only with TGBA
 */

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.function.Supplier;
import plug.core.IConfiguration;
import plug.core.ITransitionGeneralizedBuchiConfiguration;
import plug.core.ITransitionRelation;
import plug.events.ExecutionEndedEvent;
import plug.events.PropertyEvent;
import plug.explorer.buchi.DefaultExecutionController;

//TODO: this algo needs TGBA. Can we adapt it to work with BA ?
//TODO: this does not care about the state-space at all, however it supposes that the states are already in. (can we offer an intermediate API doing that?)
public class TGBA_SCC_GaiserSchwoon_Recursive<C extends ITransitionGeneralizedBuchiConfiguration & IConfiguration, A> extends DefaultExecutionController<C, A> {

    public TGBA_SCC_GaiserSchwoon_Recursive(ITransitionRelation<C, A> runtime) {
        super(runtime);
    }

    @Override
    public Supplier getMetadataSupplier() {
        return ()->new Metadata();
    }

    private Set<Integer> theK;
    void initializeTheK(int numberOfAcceptanceSets) {
        //initialize theK set
        theK = new HashSet<>();
        for (int i = 0; i<numberOfAcceptanceSets; i++) {
            theK.add(i);
        }
    }

    int count = 0;
    Stack<RootsStackEntry> rootsStack;
    Stack<C> activeStack;

    @Override
    public void execute() {
        rootsStack = new Stack<>();
        activeStack = new Stack<>();

        for (C s : getProductAutomaton().initialConfigurationsIterable()) {
            if (theK == null) {
                //initialize theK set
                initializeTheK(s.getNumberOfAccetanceSets());
            }
            couvreur_dfs(s);
        }
        announcer.announce(new ExecutionEndedEvent(this));
    }

    public void couvreur_dfs(C source) {
        count = count + 1;
        getMetadata(source).dfsnum = count;
        getMetadata(source).current = true;

        rootsStack.push(new RootsStackEntry(source));
        activeStack.push(source);

        for (C target : getProductAutomaton().getPostIterable(source)) {
            if (monitor.atEnd()) break;

            if (getMetadata(target).dfsnum == 0) {
                couvreur_dfs(target);
            } else if (getMetadata(target).current) {
                Set<Integer> theB = new HashSet<>();

                C u = null;
                do {
                    RootsStackEntry root = rootsStack.pop();
                    u = root.state;
                    theB.addAll(root.acceptanceIndexSet);
                    if (theB.equals(theK)) {
                        //cycle found
                        System.out.println("source = [" + source + "], target =[" + target + "]");
                        //TODO: where is the counter example ?
                        announcer.announce(new PropertyEvent(this, false, "property", null));
                    }
                } while (getMetadata(u).dfsnum > getMetadata(target).dfsnum);
                rootsStack.push(new RootsStackEntry(u, theB));
            }
        }

        if (rootsStack.peek().state == source) {
            rootsStack.pop();

            C u;
            do {
                u = activeStack.pop();
                getMetadata(u).current = false;
            } while (u != source);
        }
    }

    class RootsStackEntry {
        public C state;
        public Set<Integer> acceptanceIndexSet = new HashSet<>();

        public RootsStackEntry(C state) {
            this.state = state;
            acceptanceIndexSet.add(state.getAcceptanceSetIndex());
        }

        public RootsStackEntry(C state, Set<Integer> set) {
            this.state = state;
            this.acceptanceIndexSet = set;
        }
    }

    Metadata getMetadata(C state) {
        return (Metadata) state.getMetadata();
    }

    class Metadata {
        public int dfsnum = 0;
        public boolean current = false;
    }
}
