package plug.explorer.buchi.scc;

import java.util.Stack;
import java.util.function.Supplier;
import plug.core.IBuchiConfiguration;
import plug.core.IConfiguration;
import plug.core.ITransitionRelation;
import plug.events.ExecutionEndedEvent;
import plug.events.PropertyEvent;
import plug.explorer.buchi.DefaultExecutionController;

/*
*
* Schwoon, Stefan, and Javier Esparza. "A note on on-the-fly verification algorithms." International Conference on Tools and Algorithms for the Construction and Analysis of Systems. Springer, Berlin, Heidelberg, 2005.
*
* @execute is the "Translation of Couvreur's algorithm" presented in the paper (Fig. 5)
* */

public class BA_Couvreur_Recursive<C extends IBuchiConfiguration & IConfiguration, A> extends DefaultExecutionController<C, A> {

    public BA_Couvreur_Recursive(ITransitionRelation<C, A> runtime) {
        super(runtime);
    }

    @Override
    public Supplier getMetadataSupplier() {
        return ()-> new Metadata();
    }

    @Override
    public void execute() {
        for (C initial : getProductAutomaton().initialConfigurationsIterable()) {
            couvreur_dfs(initial);
        }
        announcer.announce(new ExecutionEndedEvent(this));
    }
    int count = 0;
    Stack<C> roots = new Stack<>();

    void couvreur_dfs(C source) {
        count++;
        data(source).dfsnum = count;
        roots.push(source);
        data(source).current = true;

        for (C t : getProductAutomaton().getPostIterable(source)) {
            if (monitor.atEnd()) break;

            if (data(t).dfsnum == 0) {
                couvreur_dfs(t);
            } else if (data(t).current) {
                C u;
                do {
                    u = roots.pop();
                    if (u.isAccepting()) {
                        //cycle found
                        System.out.println("target = [" + u + "]");
                        //TODO: where is the counter example?
                        announcer.announce(new PropertyEvent(this, false, "property", null));
                    }
                } while (data(u).dfsnum > data(t).dfsnum);
                roots.push(u);
            }
        }
        if (roots.peek() == source) {
            roots.pop();
            remove(source);
        }
    }

    void remove(C s) {
        if (!data(s).current) return;
        data(s).current = false;

        for (C t : getProductAutomaton().getPostIterable(s)) {
            remove(t);
        }
    }

    class Metadata {
        public int dfsnum = 0;
        public boolean current = false;
    }

    Metadata data(C state) {
        return (Metadata) state.getMetadata();
    }
}
