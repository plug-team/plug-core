package plug.explorer;

import java.util.BitSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import plug.core.IConcurrentTransitionRelation;
import plug.core.IConfiguration;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.explorer.buchi.nested_dfs.Color;

public class ConcurrentBFSExplorer<C extends IConfiguration, A> extends AbstractExplorer<C, A> {
	protected final SharedState shared;
	public int nbThreads;

	public ConcurrentBFSExplorer(IConcurrentTransitionRelation<? extends IConcurrentTransitionRelation, C, ?> runtime, IStateSpaceManager<C,A> stateSpaceManager) {
		this(runtime, stateSpaceManager, Runtime.getRuntime().availableProcessors());
	}

	public ConcurrentBFSExplorer(IConcurrentTransitionRelation<? extends IConcurrentTransitionRelation, C, ?> runtime, IStateSpaceManager<C,A> stateSpaceManager, int nbThreads) {
		super(runtime, stateSpaceManager);
		this.nbThreads = nbThreads;
		shared = new SharedState(nbThreads);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initializeExploration() {
		shared.stateSpaceManager = this.stateSpaceManager;
		super.initializeExploration();
	}
	
	@Override
	public boolean atEnd() {
		return shared.done;
	}

	protected IConcurrentTransitionRelation copyRuntime() {
		return ((IConcurrentTransitionRelation)getRuntime()).createCopy();
	}

	@Override
	public void explorationStep() {
		Thread threads[] = new Thread[shared.N];
		for (short i = 0; i < shared.N ; i++) {
			threads[i] = new Thread(new ExplorerThread(copyRuntime(), stateSpaceManager, shared, i));
		}
		for (short i = 0; i < shared.N ; i++) {
			threads[i].start();
		}
		shared.waitTheEnd();
	}
	
	@Override
	public C nextConfiguration() { return null;}

	@Override
	public void schedule(C conf) {
		setColor(conf, Color.RED);
		shared.toSee[0][0][0].add(conf);
	}

    protected static class IdleSet {
		public final BitSet set;
		private final int size;

		public IdleSet(int size) {
			set = new BitSet(size);
			this.size = size;
		}
		public synchronized void set(int myId) {
			set.set(myId);
			if (set.cardinality() == size) {
				notifyAll();
			}
		}

		public synchronized void waitUntilFull() {
			while (set.cardinality() != size) {
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	protected static class SharedState {
		@SuppressWarnings("rawtypes")
		public Queue toSee[][][];
		public boolean done = false;
		public IdleSet idle;
		public short t = 0;
		public int N;
		@SuppressWarnings("rawtypes")
		public IStateSpaceManager stateSpaceManager;
		public Random random = new Random();


		public SharedState(int nbThreads) {
			idle = new IdleSet(nbThreads);
			toSee = new Queue[2][nbThreads][nbThreads];
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j<nbThreads; j++) {
					for (int k =0; k<nbThreads; k++) {
						toSee[i][j][k] = new LinkedList<IConfiguration>();
					}
				}
			}
			N = nbThreads;
		}


		public synchronized void waitEndRound(int myT) {
			while (t == myT && !done) {
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		public synchronized void whenAllIdle() {
			if (idle.set.cardinality() == N) {
				done = true;
				for (int i = 0; i<N; i++) {
					for (int j = 0; j < N; j++) {
						if (!toSee[1-t][i][j].isEmpty()) {
							done = false;
							break;
						}
					}
					if (!done) break;
				}
				if (!done) {
					idle.set.clear();
					t = (short) (1-t);
				}
				notifyAll();
			}
		}

		public synchronized void waitTheEnd() {
			while (!done) {
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	protected static class ExplorerThread<C extends IConfiguration, A> extends AbstractExplorer<C, A> implements Runnable {
		public final SharedState shared;
		public final short myId;

		public short myT;
		public short currentQ;

		@SuppressWarnings("unchecked")
		public ExplorerThread(ITransitionRelation<C, ?> runtime, IStateSpaceManager<C,A> stateSpaceManager, SharedState shared, short id) {
			super(runtime, stateSpaceManager);
			this.shared = shared;
			myId = id;
		}

		@Override
		public void run() {
			myT = shared.t;
			do {
				for (currentQ = 0; currentQ < shared.N; currentQ++) {
					execute();
				}
				shared.idle.set(myId);
				if (myId == 0) {
					//wait until all threads are idle
					shared.idle.waitUntilFull();
					shared.whenAllIdle();
				}
				else {
					//wait until t!=ot or done
					shared.waitEndRound(myT);
					myT = shared.t;
				}
			} while (!shared.done);
		}

		@Override
		public void initializeExploration() {}

		@Override
		public boolean atEnd() {
			return shared.toSee[shared.t][myId][currentQ].isEmpty();
		}

		@Override
		public C nextConfiguration() {
			return (C)(shared.toSee[shared.t][myId][currentQ]).remove();
		}

		@SuppressWarnings("unchecked")
		@Override
		public void schedule(C conf) {
			setColor(conf, Color.RED);
			int threadId = shared.random.nextInt(shared.N);
			(shared.toSee[1-shared.t][threadId][myId]).add(conf);
		}

	}
}

