package plug.explorer;

import java.util.Stack;

import plug.core.IConfiguration;
import plug.core.ITransitionRelation;
import plug.core.IStateSpaceManager;
import plug.explorer.buchi.nested_dfs.Color;

/**
 * Created by ciprian on 25/04/15.
 */
public class DFSExplorer<C extends IConfiguration, A> extends AbstractExplorer<C, A> {
    protected final Stack<C> toSee = new Stack<>();

    public DFSExplorer(ITransitionRelation<C, ?> runtime, IStateSpaceManager<C, A> stateSpaceManager) {
        super(runtime, stateSpaceManager);
    }

    @Override
    public boolean atEnd() {
        return toSee.isEmpty();
    }

    @Override
    public C nextConfiguration() {
        return toSee.pop();
    }

    @Override
    public void schedule(C conf) {
        setColor(conf, Color.RED);
        toSee.push(conf);
    }
}
