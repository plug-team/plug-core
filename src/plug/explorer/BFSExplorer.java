package plug.explorer;

import java.util.LinkedList;
import java.util.Queue;
import plug.core.IConfiguration;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.explorer.buchi.nested_dfs.Color;

public class BFSExplorer<C extends IConfiguration, A> extends AbstractExplorer<C, A> {

	protected final Queue<C> toSee = new LinkedList<>();

	public BFSExplorer(ITransitionRelation runtime, IStateSpaceManager<C, A> stateSpaceManager) {
		super(runtime, stateSpaceManager);
	}

	@Override
	boolean atEnd() {
		return toSee.isEmpty();
	}
	
	@Override
	C nextConfiguration() {
		return toSee.remove();
	}
	
	@Override
	void schedule(C conf) {
		setColor(conf, Color.RED);
		toSee.add(conf);
	}

}
