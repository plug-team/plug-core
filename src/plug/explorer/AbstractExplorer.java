package plug.explorer;

import announce4j.Announcer;
import java.util.Iterator;
import plug.core.IConfiguration;
import plug.core.IProductAutomaton;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.core.execution.IExecutionMonitor;
import plug.events.CloseConfigurationEvent;
import plug.events.ExecutionEndedEvent;
import plug.events.ExecutionStartedEvent;
import plug.events.FiredEvent;
import plug.events.OpenConfigurationEvent;
import plug.explorer.buchi.nested_dfs.Color;
import plug.statespace.LazyProductAutomaton;


public abstract class AbstractExplorer<C extends IConfiguration, A> implements IExecutionController<C,A> {

	protected final IExecutionMonitor.Simple monitor = new IExecutionMonitor.Simple();

	protected final IStateSpaceManager<C, A> stateSpaceManager;

	protected final IProductAutomaton productAutomaton;

	protected final Announcer announcer = new Announcer(true);

	@SuppressWarnings("unchecked")
	public AbstractExplorer(ITransitionRelation<C, ?> runtime, IStateSpaceManager<C, A> stateSpaceManager) {
		this.stateSpaceManager = stateSpaceManager;
		this.productAutomaton = new LazyProductAutomaton(runtime, stateSpaceManager, ()-> Color.WHITE);
	}

	@Override
	public Announcer getAnnouncer() {
		return announcer;
	}

	@Override
	public IStateSpaceManager<C, A> getStateSpaceManager() {
		return stateSpaceManager;
	}

	@SuppressWarnings("unchecked")
	public ITransitionRelation getRuntime() {
		return (ITransitionRelation) productAutomaton;
	}

	public IProductAutomaton getProductAutomaton() {
		return productAutomaton;
	}

	@Override
	public IExecutionMonitor.Simple getMonitor() {
		return monitor;
	}

	abstract boolean atEnd();
	abstract C nextConfiguration();
	abstract void schedule(C conf);

	@SuppressWarnings("unchecked")
	void initializeExploration() {
		for (C initial : (Iterable<C>) getProductAutomaton().initialConfigurationsIterable()) {
			schedule(initial);
		}
		announcer.announce(new ExecutionStartedEvent<>(this, stateSpaceManager.initialConfigurations()));
	}

	@Override
	public void execute() {
		initializeExploration();
		
		while (!monitor.atEnd() && !atEnd()) {
			explorationStep();
		}
		
		announcer.announce(new ExecutionEndedEvent(this));
	}

	void explorationStep() {
		C source = nextConfiguration();
		announcer.announce(new OpenConfigurationEvent<>(this, source));

		fire(source);

		announcer.announce(new CloseConfigurationEvent<>(this, source));
	}

	@SuppressWarnings({"unchecked"})
	protected void fire(C source) {
        @SuppressWarnings( "unchecked" )
		Iterator<C> iterator = getProductAutomaton().getPostIterable(source).iterator();
		while (iterator.hasNext()) {
			announcer.announce(new FiredEvent<>(this, ((LazyProductAutomaton.PostIterator)iterator).currentFiredTransition));
			if (monitor.atEnd()) {
				announcer.announce(new ExecutionEndedEvent(this));
				return;
			}
			C target = iterator.next();
			if (getColor(target) == Color.WHITE) {
				schedule(target);
			}
		}
	}

	Color getColor(C state) {
		return (Color)state.getMetadata();
	}

	void setColor(C state, Color color) {
		state.setMetadata(color);
	}
}

