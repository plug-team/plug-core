package plug.explorer;

import java.util.LinkedList;
import java.util.Queue;

import plug.core.IConfiguration;
import plug.core.ITransitionRelation;
import plug.core.IStateSpaceManager;
import plug.explorer.buchi.nested_dfs.Color;

public class BoundedBFSExplorer<C extends IConfiguration, A> extends AbstractExplorer<C, A> {

	protected final int targetDepth;
	protected int currentDepth;

	@SuppressWarnings("unchecked")
	protected final Queue<C>[] toSee = new Queue[2];
	protected int currentQueueID = 0;

	public BoundedBFSExplorer(ITransitionRelation<C, ?> runtime, IStateSpaceManager<C,A> stateSpaceManager, int targetDepth) {
		super(runtime, stateSpaceManager);
		this.targetDepth = targetDepth;
	}

	@Override
	public void initializeExploration() {
		currentDepth = 0;
		toSee[0] = new LinkedList<>();
		toSee[1] = new LinkedList<>();
		super.initializeExploration();
	}

	@Override
	public boolean atEnd() {
		if (toSee[currentQueueID].isEmpty()) {
			currentQueueID = 1-currentQueueID;
			if (toSee[currentQueueID].isEmpty()) {
				return true;
			}
			currentDepth++;
		}
		return currentDepth >= targetDepth;
	}
	
	@Override
	public C nextConfiguration() {
		return toSee[currentQueueID].remove();
	}
	
	@Override
	public void schedule(C conf) {
		setColor(conf, Color.RED);
		toSee[1 - currentQueueID].add(conf);
	}
}
