package plug.simulation.selection;



import announce4j.Announcer;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomSelectionPolicy implements ISelectionPolicy {

	private Random mRandom;

	@Override
	public Announcer getAnnouncer() {
		return null;
	}

	@Override
	public void initializePolicy() {
		mRandom = new Random();
	}
	
	@Override
	public <T>  List<T> chooseTargets(Collection<T> targets) {
		if (targets.isEmpty()) return Collections.emptyList();
		return Collections.singletonList(((List<T>)targets).get(mRandom.nextInt(targets.size())));
	}
	
}
