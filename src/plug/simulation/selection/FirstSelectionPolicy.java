package plug.simulation.selection;


import announce4j.Announcer;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class FirstSelectionPolicy implements ISelectionPolicy {

	@Override
	public Announcer getAnnouncer() {
		return null;
	}

	@Override
	public void initializePolicy() {
		//nothing to do
	}

	@Override
	public <T> List<T> chooseTargets(Collection<T> targets) {
		if (targets.isEmpty()) return Collections.emptyList();
		//always return the first index
		return Collections.singletonList(((List<T>)targets).get(0));
	}

}
