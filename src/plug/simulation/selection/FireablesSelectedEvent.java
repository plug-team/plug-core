package plug.simulation.selection;

import java.util.Collection;

/**
 * Created by ciprian on 14/02/17.
 */
public class FireablesSelectedEvent<F> {
    Collection<F> fireables;

    public FireablesSelectedEvent(Collection<F> fireables) {
        this.fireables = fireables;
    }

    public Collection<F> getFireables() {
        return fireables;
    }
}
