package plug.simulation.selection;


import announce4j.Announcer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

/**
 * Created by ciprian on 14/02/17.
 */
public class InteractiveSelectionPolicy implements ISelectionPolicy {
    Announcer announcer = new Announcer(true);
    @Override
    public Announcer getAnnouncer() {
        return announcer;
    }

    @Override
    public void initializePolicy() {

    }

    @Override
    public <T> List<T> chooseTargets(Collection<T> targets) {
        List<T> result = new ArrayList<T>();
        Semaphore sem = new Semaphore(0);
        new Thread (
                () -> announcer.when(FireablesSelectedEvent.class, (announcer, event) -> {
                    result.addAll(targets.stream().filter(t -> event.getFireables().contains(t)).collect(Collectors.toList()));
                    sem.release();
                })
        ).start();

        try {
            sem.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }
}
