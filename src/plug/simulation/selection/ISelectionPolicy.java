package plug.simulation.selection;


import announce4j.Announcer;

import java.util.Collection;
import java.util.List;

public interface ISelectionPolicy {
	Announcer getAnnouncer();
	void initializePolicy();
	<T> List<T> chooseTargets(Collection<T> targets);
	
//	default Collection<IFireableTransition<C, A>> selectTarget(Collection<? extends IFireableTransition<C, A>> targets) {
//
//		if (targets.size() < 1) return Collections.emptyList();
//
//		int index = chooseTargetIndex(targets);
//		if (targets instanceof List) {
//			List <IFireableTransition<C, A>> result = new ArrayList<>();
//			result.add(((List<IFireableTransition<C, A>>) targets).get(index));
//			return result;
//		}
//
//		Iterator<? extends IFireableTransition> iterator = targets.iterator();
//		for (int i = 0; i<index; i++) {
//			iterator.next();
//		}
//		List <IFireableTransition<C, A>> result = new ArrayList<>();
//		result.add(iterator.next());
//		return result;
//	}
}
