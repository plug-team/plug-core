package plug.simulation.trace_storage;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.function.BiFunction;
import java.util.function.Consumer;

public class TraceStore<C, A> {
    //TODO: the hasCounterExample has to be removed from here
    //TODO: it is a hack while the TraceStore is not merged in the execution
    private boolean hasCounterExample = false;
    CountDownLatch latch;

    public TraceStore() {
        latch = new CountDownLatch(1);
    }
    public boolean hasCounterExample() throws InterruptedException {
        latch.await();
        return hasCounterExample;
    }

    public void setHasCounterExample() {
        hasCounterExample = true;
        latch.countDown();
    }

    public void noCounterExample() {
        latch.countDown();
    }

    Set<TraceEntry<C, A>> roots = new HashSet<>();
    TraceEntry<C, A> lastEntry;

    //action that is executed when adding a new root
    Consumer<TraceEntry<C,A>> addRootUserCallback;

    public void setAddRootUserCallback(Consumer<TraceEntry<C, A>> addRootUserCallback) {
        this.addRootUserCallback = addRootUserCallback;
    }

    //action executed when addind a new step
    Consumer<TraceEntry<C, A>> addStepUserCallback;

    public void setAddStepUserCallback(Consumer<TraceEntry<C, A>> addStepUserCallback) {
        this.addStepUserCallback = addStepUserCallback;
    }

    public TraceEntry<C, A> getLastEntry() {
        return lastEntry;
    }

    public TraceEntry<C, A> addRoot(C rootEntry) {
        if (rootEntry == null) return null;
        TraceEntry<C, A> entry = new TraceEntry<>(rootEntry);
        roots.add(entry);
        if (addRootUserCallback != null) {
            addRootUserCallback.accept(entry);
        }
        lastEntry = entry;
        return entry;
    }

    public void addRoots(Collection<C> rootEntries) {
        rootEntries.forEach(this::addRoot);
    }

    public Collection<TraceEntry<C, A>> getRoots() {
        return roots;
    }

    public TraceEntry<C, A> addStep(TraceEntry<C, A> sourceEntry, C target, A action) {
        if (target == null) return null;
        TraceEntry<C, A> targetEntry = new TraceEntry<>(sourceEntry, target, action);
        if (sourceEntry == null) {
            roots.add(targetEntry);
        }
        if (addStepUserCallback != null) {
            addStepUserCallback.accept(targetEntry);
        }
        lastEntry = targetEntry;
        return targetEntry;
    }

    public TraceEntry<C, A> addSteps(TraceEntry<C, A> potentialSource, List<C> followingSteps, BiFunction<C, C, A> actionsProvider) {
        if (potentialSource == null) {
            return addSteps(Collections.emptyList(), followingSteps, actionsProvider);
        }
        return addSteps(Collections.singleton(potentialSource), followingSteps, actionsProvider);
    }

    public TraceEntry<C, A> addSteps(
            Collection<TraceEntry<C, A>> potentialSources,
            List<C> followingSteps,
            BiFunction<C, C, A> actionsProvider)
    {
        if (followingSteps == null || followingSteps.isEmpty()) return null;
        // If the counter-example
        // 1) does not start from the initial configuration already present in the model
        // 2) does not start from the potentialSource (corresponding to a selected TraceEntry)
        // then add a new root node to show the counter example

        C firstC = followingSteps.get(0);
        TraceEntry<C, A> initial = null;
        //if the following step start from one root, then initial is that root
        initial = getAMatchingTraceEntry(roots, firstC);

        //if no initial=root, then check if we can start from the potential source
        if ((initial == null)) {
            initial = getAMatchingTraceEntry(potentialSources, firstC);
        }

        //it still we don't have a initial node, then add a new root in the trace-store
        if (initial == null) {
            initial = addRoot(firstC);
        }

        // and finally build the trace chain
        TraceEntry<C, A> previous = initial;
        for (int i = 1; i < followingSteps.size(); i++) {
            C current = followingSteps.get(i);
            previous = addStep(previous, current, actionsProvider.apply(previous.getConfiguration(), current));
        }
        return previous;
    }

    TraceEntry<C, A> getAMatchingTraceEntry(Collection<TraceEntry<C, A>> collection, C target) {
        for (TraceEntry<C, A> traceEntry : collection) {
            if (traceEntry.getConfiguration().equals(target)) {
                return traceEntry;
            }
        }
        return null;
    }

    public void iterateOverTrace() {
        for (TraceEntry<C, A> root: roots) {
            dfs(root);
        }
    }
    void dfs(TraceEntry<C, A> entry) {
        if (entry.getParent() == null ) {
            if (addRootUserCallback != null) {
                addRootUserCallback.accept(entry);
            }
        } else {
            if (addStepUserCallback != null) {
                addStepUserCallback.accept(entry);
            }
        }
        for (TraceEntry<C, A> child : entry.getChildren()) {
            dfs(child);
        }
    }
}
