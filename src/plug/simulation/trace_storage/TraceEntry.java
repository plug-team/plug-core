package plug.simulation.trace_storage;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/** Part of a trace
 * we cannot use the IConfiguration in a trace because a trace can have multiple times the same configuration
 * */
public class TraceEntry<C, A> {

	private final TraceEntry<C, A> parent;
	private final Set<TraceEntry<C, A>> children = new HashSet<>();

	private final C configuration;
	private final A action;

	public TraceEntry(C configuration) {
		this(null, configuration);
	}

	public TraceEntry(TraceEntry<C, A> parent, C configuration) {
		this(parent, configuration, null);
	}

	public TraceEntry(TraceEntry<C, A> parent, C configuration, A action) {
		this.parent = parent;
		if (parent != null) {
			parent.addChild(this);
		}
		this.configuration = configuration;
		this.action = action;
	}

	public void addChild(TraceEntry<C, A> child) {
		children.add(child);
	}

	public Set<TraceEntry<C, A>> getChildren() {
		return children;
	}

	public TraceEntry<C, A> getParent() {
		return parent;
	}

	public C getParentConfiguration() {
		return (parent == null) ? null : parent.getConfiguration();
	}

	public C getConfiguration() {
		return configuration;
	}

	public A getAction() {
		return action;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		TraceEntry<?, ?> that = (TraceEntry<?, ?>) o;
		return     parent == that.parent
				&& Objects.equals(configuration, that.configuration);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getParentConfiguration(), configuration);
	}

	@Override
	public String toString() {
		if (getParentConfiguration() != null) {
			return Integer.toHexString(getParentConfiguration().hashCode()) + " -> " + Integer.toHexString(configuration.hashCode());
		} else {
			return Integer.toHexString(configuration.hashCode());
		}
	}
}
