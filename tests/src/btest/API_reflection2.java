package btest;

import java.util.Collection;
import java.util.List;
import java.util.Set;

interface ILanguagePlugin {
	ILanguageModule getModule();
}

interface ILanguageModule<AST, C, F, P, V> {
	ILoader<AST> 					getLoader();
	ITransitionRelation<C, F, P> 	getSemantics(AST ast);
	IEvaluator<C, F, P, V> 			getEvaluator();
	IView<C, F, P, V> 				getView();
	IMarshaller<C, F, P> 			getMarshaller();
}
interface ILanguageService {
	ILanguageModule getLanguageModule();
}
interface ILoader<AST> extends ILanguageService {
	AST 	load();
	void 	unload(AST ast);
}
interface ITransitionRelation<C, F, P> extends ILanguageService {
	Set<C> 						initialConfigurations();
	Collection<F> 				fireableTransitionsFrom(C source);
	IFiredTransition<C, F, P> 	fireOneTransition(C source, F transition);

	default boolean 			hasBlockingTransitions() {
		return false;
	}
}

interface IEvaluator<C, F, P, V> extends ILanguageService {
	V[] evaluate(String[] expressions, C configuration);
	V[] evaluate(String[] expressions, C source, F fireable, P payload, C target);

	default boolean[] getAtomicPropositionValuations(String[] propositions, C configuration) {
		//used by the synchronous composition based on the Kripke2Buchi transformation: evaluation on target
		return getAtomicPropositionValuations(propositions, null, null, null, configuration);
	}
	default boolean[] getAtomicPropositionValuations(String[] propositions, C source, F fireable, P payload, C target) {
		//used by the synchronous composition between Kripke and Transition-based Buchi: evaluation on source
		return getAtomicPropositionValuations(propositions, source);
	}
}

interface IView<C, F, P, V> extends ILanguageService {
	TreeItem configurationView(C configuration);
	TreeItem fireableView(F transition);
	TreeItem payloadView(P payload);
	TreeItem valueView(V value);
}
interface IMarshaller<C, F, P> extends ILanguageService {
	byte[] 	serializeConfiguration(C configuration);
	C 		deserializeConfiguration(byte[] data);

	byte[] 	serializeFireable(F fireable);
	F 		deserializeFireable(byte[] data);

	byte[] 	serializePayload(P payload);
	P 		deserializePayload(byte[] data);
}
interface IFiredTransition<C, F, P> {
	C 				getSource  ();
	Collection<C> 	getTargets ();
	F 				getFireableTransition();
	P 				getPayload ();
}

class TreeItem {
	String 			name;
	String 			value;
	String 			icon;
	List<TreeItem>  children;
}

