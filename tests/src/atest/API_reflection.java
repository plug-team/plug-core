package atest;


import announce4j.Announcer;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import plug.core.IConfiguration;
import plug.core.view.ConfigurationItem;
import plug.utils.Pair;

interface IFiredTransition<C, F> {
    Collection<C> getTargets();
    F getAction();
}

interface ITransitionRelation<C, F> {
    Set<C> initialConfigurations();

    Collection<F> fireableTransitionsFrom(C source);

    IFiredTransition<C, ?> fireOneTransition(C source, F transition);

    default boolean hasBlockingTransitions() {
        return false;
    }
}

interface ILanguageModule<AST, C, F> {

    ILoadingStrategy<AST> getLoadingStrategy();

    default AST compile() {
        return getLoadingStrategy().compile();
    }

    ITransitionRelation<C, F> getSemantics(AST ast);

    default Function<AST, ITransitionRelation<C, F>> getSemantics() {
        return this::getSemantics;
    }

    ConfigurationItem getConfigurationDescription(AST ast, C configuration);

    String getFireableTransitionDescription(AST ast, F transition);

    default Function<C, ConfigurationItem> getConfigurationView(AST ast) {
        return (c) -> this.getConfigurationDescription(ast, c);
    }

    default Function<F, String> getTransitionView(AST ast) {
        return (f) -> this.getFireableTransitionDescription(ast, f);
    }
}

@FunctionalInterface
interface ILoadingStrategy<AST> {
    AST compile();
}

class LoadFiacreFromFileStrategy implements ILoadingStrategy<FiacreExplorationContext> {
    File programFile;

    public LoadFiacreFromFileStrategy(File programFile) {
        this.programFile = programFile;
    }

    @Override
    public FiacreExplorationContext compile() {
        return null;
    }
}

@FunctionalInterface
interface IAtomicPropositionProvider<AST, AtomicProposition> {
    Collection<AtomicProposition> getAtomicPropositions(AST ast);
}

interface IAtomicPropositionConsumer<C, AST, AtomicProposition> {
    void registerAtomicPropositionsOn(AST ast, Collection<AtomicProposition> propositions);

    Function<C, Boolean[]> getAtomicPropositionEvaluator();
}

class IdentityTransitionRelation<C, F> implements ITransitionRelation<C, F> {
    final ITransitionRelation<C, F> operand;

    public IdentityTransitionRelation(ITransitionRelation<C, F> operand) {
        this.operand = operand;
    }

    @Override
    public Set<C> initialConfigurations() {
        return operand.initialConfigurations();
    }

    @Override
    public Collection<F> fireableTransitionsFrom(C source) {
        return operand.fireableTransitionsFrom(source);
    }

    @Override
    public IFiredTransition<C, ?> fireOneTransition(C source, F transition) {
        return operand.fireOneTransition(source, transition);
    }

    @Override
    public boolean hasBlockingTransitions() {
        return operand.hasBlockingTransitions();
    }
}


/*
* The pruned transition relation is a simple way to implement TLA state-constraints
* A simple example of this usage is bounding the size of the EventPools in UML
* */
class PrunedTransitionRelation<C, F> extends IdentityTransitionRelation<C, F> {
    final Predicate<C> pruningPredicate;
    Consumer<C> onPrunedConfiguration;

    public PrunedTransitionRelation(ITransitionRelation<C, F> operand, Predicate<C> pruningPredicate, Consumer<C> onPrunedConfiguration) {
        super(operand);
        this.pruningPredicate = pruningPredicate;
        this.onPrunedConfiguration = onPrunedConfiguration;
    }

    @Override
    public Collection<F> fireableTransitionsFrom(C source) {
        if (pruningPredicate.test(source)) {
            if (onPrunedConfiguration != null) {
                onPrunedConfiguration.accept(source);
            }
            return Collections.emptySet();
        }
        return operand.fireableTransitionsFrom(source);
    }
}

class AssertFailedEvent<C> {
    C configuration;

    public AssertFailedEvent(C configuration) {
        this.configuration = configuration;
    }
}

//The AssertingTransitionRelation can be used to :
// -- verify assertions on a system, by installing it on the system with a predicate like (c) -> assertion.test(c)
// -- verify observer automata, by installing it on the observer automaton with a predicate like (c) -> c.isRejectState
//The counter-example can be then computed by the caller, by relying on ParentTransitionStorage
class AssertingTransitionRelation<C, F> extends IdentityTransitionRelation<C, F> {
    final Predicate<C> assertPredicate;
    final Consumer<C> onAssertFailed;

    public AssertingTransitionRelation(ITransitionRelation<C, F> operand, Predicate<C> assertPredicate, Consumer<C> onAssertFailed) {
        super(operand);
        this.assertPredicate = assertPredicate;
        this.onAssertFailed = onAssertFailed;
    }

    @Override
    public Collection<F> fireableTransitionsFrom(C source) {
        if (assertPredicate.negate().test(source)) {
            if (onAssertFailed != null)
                onAssertFailed.accept(source);
        }
        return operand.fireableTransitionsFrom(source);
    }
}

class InitializedTransitionRelation<C, F> extends IdentityTransitionRelation<C, F> {
    final Set<C> initialConfigurations;

    public InitializedTransitionRelation(ITransitionRelation<C, F> operand, Set<C> initialConfigurations) {
        super(operand);
        this.initialConfigurations = initialConfigurations;
    }

    @Override
    public Set<C> initialConfigurations() {
        return initialConfigurations;
    }
}

interface IProductAutomaton<C, F> {
    Iterable<C> initialConfigurationsIterable();

    Iterable<F> fireableTransitionsIterable(C source);

    Iterable<C> getPostIterable(C source);
}

interface IStateSpaceManager<C, F> {
}

class SimpleStateSpaceManager<C, F> implements IStateSpaceManager<C, F> {
}

// TODO maybe a NoCacheProductAutomaton

class LazyProductAutomaton<C, F> implements IProductAutomaton<C, F> {

    final ITransitionRelation<C, F> runtime;
    final IStateSpaceManager<C, F> stateSpaceManager;
    final Supplier metadataSupplier;

    public LazyProductAutomaton(ITransitionRelation<C, F> runtime, IStateSpaceManager<C, F> stateSpaceManager, Supplier metadataSupplier) {
        this.runtime = runtime;
        this.stateSpaceManager = stateSpaceManager;
        this.metadataSupplier = metadataSupplier;
    }

    @Override
    public Iterable<C> initialConfigurationsIterable() {
        return null;
    }

    @Override
    public Iterable<F> fireableTransitionsIterable(C source) {
        return null;
    }

    @Override
    public Iterable<C> getPostIterable(C source) {
        return null;
    }
}

interface IFoldableTransition<C, F> {
    default F getAction() {
        return null;
    }

    default boolean isFolded() {
        return false;
    }
}

class FoldedTransition<C, F> implements IFoldableTransition<C, F>, IFiredTransition<C, F> {
    public FoldedTransition(C source, Collection<C> targets) {

    }

    @Override
    public boolean isFolded() {
        return true;
    }

    @Override
    public Collection<C> getTargets() {
        return null;
    }

    @Override
    public F getAction() {
        return null;
    }
}

class UnfoldedTransition<C, F> implements IFoldableTransition<C, F> {
    F fireable;

    public UnfoldedTransition(F fireable) {

    }

    @Override
    public F getAction() {
        return fireable;
    }
}

class FoldingTransitionRelation<C, F> implements ITransitionRelation<C, IFoldableTransition<C, F>> {
    final ITransitionRelation<C, F> operand;
    final Predicate<C> foldingPredicate;

    Function<ITransitionRelation<C, F>, MyExplorer<C, F>> analysisProvider;

    public FoldingTransitionRelation(ITransitionRelation<C, F> operand, Predicate<C> foldingPredicate, Function<ITransitionRelation<C, F>, MyExplorer<C, F>> analysisProvider) {
        this.operand = operand;
        this.foldingPredicate = foldingPredicate;
        this.analysisProvider = analysisProvider;
    }

    @Override
    public Set<C> initialConfigurations() {
        return operand.initialConfigurations();
    }

    @Override
    public Collection<IFoldableTransition<C, F>> fireableTransitionsFrom(C source) {
        if (foldingPredicate.test(source)) {
            Set<C> targets = new HashSet<>();
            MyExplorer<C,F> analysis = analysisProvider.apply(
                    new PrunedTransitionRelation<>(
                            new InitializedTransitionRelation<>(
                                    operand,
                                    Collections.singleton(source)),
                            foldingPredicate.negate(),
                            targets::add));
            analysis.execute();

            return Collections.singletonList(new FoldedTransition<C, F>(source, targets));
        }
        return operand.fireableTransitionsFrom(source).stream().map((Function<F, UnfoldedTransition<C, F>>) UnfoldedTransition::new).collect(Collectors.toList());
    }

    @Override
    public IFiredTransition<C, ?> fireOneTransition(C source, IFoldableTransition<C, F> transition) {
        if (transition.isFolded()) {
            return (FoldedTransition<C, ?>) transition;
        }
        return operand.fireOneTransition(source, transition.getAction());
    }
}

class DepthBoundedConfiguration<C> {
    C operandConfiguration;
    int depth;

    public DepthBoundedConfiguration(C configuration, int depth) {
        this.operandConfiguration = configuration;
        this.depth = depth;
    }
}

class FiredTransition<C, F> implements IFiredTransition<C, F> {

    FiredTransition(C source, Collection<C> targets, F action) {

    }

    @Override
    public Collection<C> getTargets() {
        return null;
    }

    @Override
    public F getAction() {
        return null;
    }
}
//this class could inherit from PrunedTransitionRelation,
// but we need a configuration-wrapping TransitionRelation to first wrap the configuration in a DepthBoundedConfiguration
class DepthBoundedTransitionRelation<C, F> implements ITransitionRelation<DepthBoundedConfiguration<C>, F> {

    final ITransitionRelation<C, F> operand;
    int depth;

    public DepthBoundedTransitionRelation(ITransitionRelation<C, F> operand, int depth) {
        this.operand = operand;
        this.depth = depth;
    }
    @Override
    public Set<DepthBoundedConfiguration<C>> initialConfigurations() {
        return operand.initialConfigurations().stream().map(c -> new DepthBoundedConfiguration<>(c, depth)).collect(Collectors.toSet());
    }

    @Override
    public Collection<F> fireableTransitionsFrom(DepthBoundedConfiguration<C> source) {
        //if the depth is smaller or equal to 0, then
        if (source.depth > 0) {
            return operand.fireableTransitionsFrom(source.operandConfiguration);
        }
        return Collections.emptyList();
    }

    @Override
    public IFiredTransition<DepthBoundedConfiguration<C>, ?> fireOneTransition(DepthBoundedConfiguration<C> source, F transition) {
        //each fired transition decreases the depth by one
        IFiredTransition<C, ?> fired = operand.fireOneTransition(source.operandConfiguration, transition);
        return new FiredTransition<>(
                source,
                fired.getTargets().stream().map(c-> new DepthBoundedConfiguration<>(c, source.depth--)).collect(Collectors.toList()),
                fired.getAction());
    }
}

class ConfigurationWrappingTransitionRelation<C, F, WC, T> implements ITransitionRelation<WC, F> {
    final ITransitionRelation<C, F> operand;
    BiFunction<C, T, WC> wrappingFunction;
    Supplier<T> initial;
    Function<WC, T> next;
    Function<WC, C> unwrappingFunction;

    public ConfigurationWrappingTransitionRelation(ITransitionRelation<C, F> operand, BiFunction<C, T, WC> wrappingFunction, Function<WC, C> unwrappingFunction, Supplier<T> initial, Function<WC, T> next) {
        this.operand = operand;
        this.wrappingFunction = wrappingFunction;
        this.unwrappingFunction = unwrappingFunction;
        this.initial = initial;
        this.next = next;
    }

    @Override
    public Set<WC> initialConfigurations() {
        return operand.initialConfigurations().stream().map(c -> wrappingFunction.apply(c, initial.get())).collect(Collectors.toSet());
    }

    @Override
    public Collection<F> fireableTransitionsFrom(WC source) {
        return operand.fireableTransitionsFrom(unwrappingFunction.apply(source));
    }

    @Override
    public IFiredTransition<WC, ?> fireOneTransition(WC source, F transition) {
        IFiredTransition<C, ?> fired = operand.fireOneTransition(unwrappingFunction.apply(source), transition);
        return new FiredTransition<>(
                source,
                fired.getTargets().stream().map(c-> wrappingFunction.apply(c, next.apply(source))).collect(Collectors.toList()),
                fired.getAction());
    }
}

//and now the DepthBoundedTransitionRelation using the Wrapping and Pruning operators
//NOTE: the topological order of the acyclic verification guides might be used to induce a similar sweep-line.
// In this case the same function could be used to forget the past.
class DepthBoundedTransitionRelation1<C, F> extends PrunedTransitionRelation<DepthBoundedConfiguration<C>, F> {

    public DepthBoundedTransitionRelation1(ITransitionRelation<C, F> operand, int maxDepth, Consumer<DepthBoundedConfiguration<C>> onDepthReached) {
        super(
                new ConfigurationWrappingTransitionRelation<>(
                        operand,
                        DepthBoundedConfiguration::new,
                        (c)->c.operandConfiguration,
                        ()->maxDepth,
                        (c)->c.depth - 1),
                (c) -> c.depth < 1,
                onDepthReached);
    }
}


class FiacreTransitionRelation implements ITransitionRelation<Object, Object> {

    @Override
    public Set<Object> initialConfigurations() {
        return null;
    }

    @Override
    public Collection<Object> fireableTransitionsFrom(Object source) {
        return null;
    }

    @Override
    public IFiredTransition<Object, ?> fireOneTransition(Object source, Object transition) {
        return null;
    }
}

class FiacreExplorationContext {
}

class FiacreLanguageModule implements ILanguageModule<FiacreExplorationContext, Object, Object>, IAtomicPropositionConsumer<Object, Object, Object> {
    ILoadingStrategy<FiacreExplorationContext> loadingStrategy;

    FiacreLanguageModule() {
    }

    FiacreLanguageModule(ILoadingStrategy<FiacreExplorationContext> loadingStrategy) {
        this.loadingStrategy = loadingStrategy;
    }

    @Override
    public ILoadingStrategy<FiacreExplorationContext> getLoadingStrategy() {
        return loadingStrategy;
    }

    public void setLoadingStrategy(ILoadingStrategy<FiacreExplorationContext> loadingStrategy) {
        this.loadingStrategy = loadingStrategy;
    }

    @Override
    public ITransitionRelation<Object, Object> getSemantics(FiacreExplorationContext o) {
        return null;
    }

    @Override
    public ConfigurationItem getConfigurationDescription(FiacreExplorationContext ast, Object configuration) {
        return null;
    }

    @Override
    public String getFireableTransitionDescription(FiacreExplorationContext ast, Object transition) {
        return null;
    }

    @Override
    public void registerAtomicPropositionsOn(Object o, Collection<Object> propositions) {

    }

    @Override
    public Function<Object, Boolean[]> getAtomicPropositionEvaluator() {
        return null;
    }
}

class ExpressionDeclaration {
}

class LoadBuchiFromExpressionDeclarationStrategy implements ILoadingStrategy<BuchiAutomata> {
    ExpressionDeclaration expressionDeclaration;

    public LoadBuchiFromExpressionDeclarationStrategy(ExpressionDeclaration expressionDeclaration) {
        this.expressionDeclaration = expressionDeclaration;
    }

    @Override
    public BuchiAutomata compile() {
//        LTL2Buchi convertor = new LTL2Buchi(new PrintWriter(System.out));
//        buchiDeclaration = convertor.convert(expressionDeclaration.getExpression());
//        buchiDeclaration.setName(expressionDeclaration.getName());
//        return buchiDeclaration;
        return null;
    }
}

class LoadBuchiFromFileStrategy implements ILoadingStrategy<BuchiAutomata> {
    File buchiFile;

    public LoadBuchiFromFileStrategy(File buchiFile) {
        this.buchiFile = buchiFile;
    }

    @Override
    public BuchiAutomata compile() {
        try {
            InputStream stream = new BufferedInputStream(new FileInputStream(buchiFile));
            return null;//new Parser().parse(new ANTLRInputStream(stream));
        } catch (IOException e) {
        }
        return null;
    }
}

class BuchiAutomata {
}

class BuchiLanguageModule implements ILanguageModule<BuchiAutomata, Object, Object>, IAtomicPropositionProvider<BuchiAutomata, Object> {
    ILoadingStrategy<BuchiAutomata> loadingStrategy;

    public BuchiLanguageModule(ILoadingStrategy<BuchiAutomata> loadingStrategy) {
        this.loadingStrategy = loadingStrategy;
    }

    @Override
    public ILoadingStrategy<BuchiAutomata> getLoadingStrategy() {
        return loadingStrategy;
    }

    @Override
    public ITransitionRelation<Object, Object> getSemantics(BuchiAutomata o) {
        return null;
    }

    @Override
    public ConfigurationItem getConfigurationDescription(BuchiAutomata ast, Object configuration) {
        return null;
    }

    @Override
    public String getFireableTransitionDescription(BuchiAutomata ast, Object transition) {
        return null;
    }

    @Override
    public Collection<Object> getAtomicPropositions(BuchiAutomata o) {
        return null;
    }
}

interface IExecutionControler {
    void execute();
}
class MyExplorer<C, F> implements IExecutionControler {
    public MyExplorer(IProductAutomaton<C, F> productAutomaton) {
    }

    public static Object getMetadataSupplier() {
        return null;
    }

    @Override
    public void execute() {

    }
}

class BuchiKripkeConfiguration<kConf> {
    kConf kripke;
    Object buchi;
}

class BuchiKripkeProductTransitionRelation<C, F> implements ITransitionRelation<BuchiKripkeConfiguration, Object> {
    public BuchiKripkeProductTransitionRelation(
            ITransitionRelation<C, F> kripke,
            ITransitionRelation<Object, Object> buchi,
            Function<C, Boolean[]> atomicPropositionEvaluator) {

    }

    @Override
    public Set<BuchiKripkeConfiguration> initialConfigurations() {
        return null;
    }

    @Override
    public Collection<Object> fireableTransitionsFrom(BuchiKripkeConfiguration source) {
        return null;
    }

    @Override
    public IFiredTransition<BuchiKripkeConfiguration, ?> fireOneTransition(BuchiKripkeConfiguration source, Object transition) {
        return null;
    }
}

class CompositeLoadingStrategy<A, B> implements ILoadingStrategy<Pair<A, B>> {
    Pair<ILoadingStrategy<A>, ILoadingStrategy<B>> loadingStrategyPair;

    public CompositeLoadingStrategy(ILoadingStrategy<A> strategyA, ILoadingStrategy<B> strategyB) {
        loadingStrategyPair = new Pair<>(strategyA, strategyB);
    }

    @Override
    public Pair<A, B> compile() {
        return new Pair<>(loadingStrategyPair.a.compile(), loadingStrategyPair.b.compile());
    }
}

class BuchiKripkeLanguageModule<kAST, kConf, kTrans> implements ILanguageModule<Pair<kAST, BuchiAutomata>, BuchiKripkeConfiguration<kConf>, Object> {

    ILanguageModule<kAST, kConf, kTrans> kripkeModule;
    BuchiLanguageModule buchiModule;

    public BuchiKripkeLanguageModule(ILanguageModule<kAST, kConf, kTrans> kripkeModule, BuchiLanguageModule buchiModule) {
        this.kripkeModule = kripkeModule;
        this.buchiModule = buchiModule;
    }

    @Override
    public ILoadingStrategy<Pair<kAST, BuchiAutomata>> getLoadingStrategy() {
        return new CompositeLoadingStrategy<>(kripkeModule.getLoadingStrategy(), buchiModule.getLoadingStrategy());
    }

    @Override
    public ITransitionRelation<BuchiKripkeConfiguration<kConf>, Object> getSemantics(Pair<kAST, BuchiAutomata> o) {
        Collection<Object> propositions = buchiModule.getAtomicPropositions(o.b);
        if (!(kripkeModule instanceof IAtomicPropositionConsumer)) {
        }

        IAtomicPropositionConsumer atomicPropositionConsumer = (IAtomicPropositionConsumer) kripkeModule;
        atomicPropositionConsumer.registerAtomicPropositionsOn(o.a, propositions);


        return new BuchiKripkeProductTransitionRelation<>(
                kripkeModule.getSemantics(o.a),
                buchiModule.getSemantics(o.b),
                atomicPropositionConsumer.getAtomicPropositionEvaluator());
    }

    @Override
    public ConfigurationItem getConfigurationDescription(Pair<kAST, BuchiAutomata> ast, BuchiKripkeConfiguration<kConf> configuration) {
        List<ConfigurationItem> items = new ArrayList<>();
        ConfigurationItem item = kripkeModule.getConfigurationDescription(ast.a, configuration.kripke);
        //item.setName("kripke");
        items.add(item);
        item = buchiModule.getConfigurationDescription(ast.b, configuration.buchi);
        //item.setName("buchi");
        items.add(item);
        items.add(buchiModule.getConfigurationDescription(ast.b, configuration.buchi));
        return new ConfigurationItem("c", "c", null, items);
    }

    @Override
    public String getFireableTransitionDescription(Pair<kAST, BuchiAutomata> ast, Object transition) {
        return null;
    }
}

class Test {

    public void foldingExploration() {
        //Explorer with Folding
        Function<ITransitionRelation<Object, Object>, MyExplorer<Object, Object>> analysisProvider
                = (transitionRelation) ->
                new MyExplorer<>(
                        new LazyProductAutomaton<>(
                                transitionRelation,
                                new SimpleStateSpaceManager<>(),
                                MyExplorer::getMetadataSupplier));

        MyExplorer<Object, IFoldableTransition<Object, Object>> explorer
                = new MyExplorer<>(
                        new LazyProductAutomaton<>(
                                new FoldingTransitionRelation<>(
                                        new FiacreTransitionRelation(),
                                        (c) -> true,
                                        analysisProvider),
                                new SimpleStateSpaceManager<>(),
                                MyExplorer::getMetadataSupplier));
    }

    public void buchiVerificationDirect() {
        //LTL verification
        //The exchange of atomic proposition should be contextualized ?
        // probably not, the atomic proposition evaluation is a service of the module, and it might be used for debug.
        BuchiLanguageModule buchiModule = new BuchiLanguageModule(new LoadBuchiFromFileStrategy(new File("")));
        BuchiAutomata propertyAST = buchiModule.compile();
        Collection<Object> propositions = buchiModule.getAtomicPropositions(propertyAST);

        FiacreLanguageModule kripkeModule = new FiacreLanguageModule(new LoadFiacreFromFileStrategy(new File("")));
        FiacreExplorationContext kripkeAST = kripkeModule.compile();
        kripkeModule.registerAtomicPropositionsOn(kripkeAST, propositions);

        new MyExplorer<>(
                new LazyProductAutomaton<>(
                        new BuchiKripkeProductTransitionRelation<>(
                                kripkeModule.getSemantics(kripkeAST),
                                buchiModule.getSemantics(propertyAST),
                                kripkeModule.getAtomicPropositionEvaluator()),
                        new SimpleStateSpaceManager<>(),
                        MyExplorer::getMetadataSupplier));
    }

    public void buchiVerificationUsingModule() {
        FiacreLanguageModule kripkeModule = new FiacreLanguageModule(new LoadFiacreFromFileStrategy(new File("")));
        BuchiLanguageModule buchiModule = new BuchiLanguageModule(new LoadBuchiFromExpressionDeclarationStrategy(new ExpressionDeclaration()));
        BuchiKripkeLanguageModule<FiacreExplorationContext, Object, Object> buchiKripkeLanguageModule = new BuchiKripkeLanguageModule<>(kripkeModule, buchiModule);
        Pair<FiacreExplorationContext, BuchiAutomata> buchiKripkeAST = buchiKripkeLanguageModule.compile();
        ITransitionRelation<BuchiKripkeConfiguration<Object>, Object> buchiKripkeTransitionRelation = buchiKripkeLanguageModule.getSemantics(buchiKripkeAST);

        new MyExplorer<>(
                new LazyProductAutomaton<>(
                        buchiKripkeTransitionRelation,
                        new SimpleStateSpaceManager<>(),
                        MyExplorer::getMetadataSupplier));
        Function<BuchiKripkeConfiguration<Object>, ConfigurationItem> configurationView = buchiKripkeLanguageModule.getConfigurationView(buchiKripkeAST);

    }

    public void assertVerificationExample() {
        //assert verification
        Collection<Object> assertions = null;
        FiacreLanguageModule fiacreLanguageModule = new FiacreLanguageModule(new LoadFiacreFromFileStrategy(new File("")));
        FiacreExplorationContext fiacreExplorationContext = fiacreLanguageModule.compile();
        fiacreLanguageModule.registerAtomicPropositionsOn(fiacreExplorationContext, assertions);

        Announcer announcer = new Announcer();
        new MyExplorer<>(
                new LazyProductAutomaton<>(
                        new AssertingTransitionRelation<>(
                                fiacreLanguageModule.getSemantics(fiacreExplorationContext),
                                (conf) -> Arrays.stream(fiacreLanguageModule.getAtomicPropositionEvaluator().apply(conf)).anyMatch(test -> test),
                                (c)->announcer.announce(new AssertFailedEvent<>(c))),
                        new SimpleStateSpaceManager<>(),
                        MyExplorer::getMetadataSupplier));

    }
}

class PORTransitionRelation<C, F> extends IdentityTransitionRelation<C, F> {

    public PORTransitionRelation(ITransitionRelation<C, F> operand) {
        super(operand);
    }

    @Override
    public Collection<F> fireableTransitionsFrom(C source) {
        return super.fireableTransitionsFrom(source);
    }
}
