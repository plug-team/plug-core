# Plug Verification Language Specification

# Generic Property Specification Language

----------
The **Generic Property Specification Language (GPSL)** is the language used by plug for specifying the properties that should be verified during the analysis.
**GPSL** should not be used for state-space pruning, environment description, etc.

In GPSL each property is associated with a named variable. All these variables form the *property set*
Any subset of the *property set* can be verified during an analysis run.

## Deadlocks
Deadlock verification is supported through the definition of a property variable **deadlockFree**.
v = deadlockFree

## Assertions: Syntax & Semantics

### Boolean Constants
There are two Boolean constants predefined in the language: **true** and **false**

### Atomic Propositions
Atomic proposition in GPSL are strings of characters enclosed between pairs of '|' or '"'. Each atomic proposition can be prefixed with two identifiers specifying the atomic proposition language and the 'verification' module on which it should be interpreted.

```
[module:language]| string of characters |
```

```java
[module:language]" string of characters "
```

### Boolean Operators 
Two expressions f and g can be combined using the following Boolean operators
|operation  | syntax0       | syntax1   | syntax2   | syntax3   | syntax4 |
|---------  |:-------------:|:---------:|:---------:|:---------:|:-------:|
|negation   | not f         | !f        | ~f        |           |         |
|disjunction| f or g        | f \| g    | f \|\| g  | f \/ g    | f + g   | 
|conjunction| f and g       | f & g     | f && g    | f /\ g    | f * g   |
|exclusion  | f xor g       | f ^ g     |           |           |         |
|implication| f implies g   | f -> g    | f => g    |           |         |
|equivalence| f iff g       | f <-> g   | f <=> g   |           |         |

### Let Expressions
To simplify the expression of large formula GPSL uses Let expression forms to 
introduce variables.

```
let
    v1 = expr
    v2 = expr
    ...
    vn = expr
in
    v1 op v2
```


## Observers: Syntax & Semantics

One interesting point about the observer implementation in plug is that we can observe the observers.
This is done by building an acyclic dependency graph between the observers and then evaluating them according to the topological ordering of this dependency graph (All parents are before their children).

**Important:** 
* Observers with *accept* states are not part of this language
* Observers with *cut* states are not part of this language -- they will be part of a different language enabling *state-space pruning*
* Another way to do state-space pruning is through the use of *folding predicates* which again should not be part of this language

## Buchi Automata

## Temporal Logic: Syntax & Semantics

The support for LTL is enabled using LTL3Buchi conversion.

### Temporal Logic Operators
Two expressions f and g can be combined using the following Boolean operators
|operation      | syntax0   | syntax1       | syntax2   | 
|---------------|:---- ----:|:-------------:|:---------:|
| Next          | X f       | next f        | () f      |
| Eventually    | F f       | eventually f  | <> f      |
| Always        | G f       | globally f    | [] f      |
| Strong Until  | f U g     | f until g     |           |
| Weak Until    | f W g     |               |           |
| Weak Release  | f R g     | f release g   | f V g     |
| Strong Release| f M g     |               |           |

## SERE
**SERE** expressions might be supported in the future, probably through the SPOT library.

## Property patterns
**In the Future** we might support properties specified through Dwyer property patterns or CDL property patterns. 
If that is the case probably it is better to support those through another language which is converted to this one



