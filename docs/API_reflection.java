package atest;


import announce4j.Announcer;
import plug.core.view.ConfigurationItem;
import plug.utils.Pair;

import java.io.File;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

interface IFiredTransition<C, F> { }

interface ITransitionRelation<C, F> {
    Set<C> initialConfigurations();

    Collection<F> fireableTransitionsFrom(C source);

    IFiredTransition<C, ?> fireOneTransition(C source, F transition);

    default boolean hasBlockingTransitions() {
        return false;
    }
}

interface ILanguageModule<Program, C, F, AST> {
    AST compileProgram(Program program);

    ITransitionRelation<C, F> getSemanticsOfAST(AST ast);
    default Function<AST, ITransitionRelation<C, F>> getSemantics() { return this::getSemanticsOfAST; }

    ConfigurationItem getConfigurationDescription(AST ast, C configuration);
    String getFireableTransitionDescription(AST ast, F transition);

    default Function<C, ConfigurationItem> getConfigurationView(AST ast) {
        return (c) -> this.getConfigurationDescription(ast, c);
    }

    default Function<F, String> getTransitionView(AST ast) {
        return (f) -> this.getFireableTransitionDescription(ast, f);
    }
}

interface IAtomicPropositionProvider<AST, AtomicProposition> {
    Collection<AtomicProposition> getAtomicPropositions(AST ast);
}

interface IAtomicPropositionConsumer<C, AST, AtomicProposition> {
    void registerAtomicPropositionsOn(AST ast, Collection<AtomicProposition> propositions);
    Function<C, Boolean[]> getAtomicPropositionEvaluator();
}

class IdentityTransitionRelation<C, F> implements ITransitionRelation<C, F> {
    final ITransitionRelation<C, F> operand;

    public IdentityTransitionRelation(ITransitionRelation<C, F> operand) {
        this.operand = operand;
    }

    @Override
    public Set<C> initialConfigurations() {
        return operand.initialConfigurations();
    }

    @Override
    public Collection<F> fireableTransitionsFrom(C source) {
        return operand.fireableTransitionsFrom(source);
    }

    @Override
    public IFiredTransition<C, ?> fireOneTransition(C source, F transition) {
        return operand.fireOneTransition(source, transition);
    }

    @Override
    public boolean hasBlockingTransitions() {
        return operand.hasBlockingTransitions();
    }
}

class PrunedTransitionRelation<C, F> extends IdentityTransitionRelation<C, F> {
    final Predicate<C> pruningPredicate;

    public PrunedTransitionRelation(ITransitionRelation<C, F> operand, Predicate<C> pruningPredicate) {
        super(operand);
        this.pruningPredicate = pruningPredicate;
    }

    @Override
    public Collection<F> fireableTransitionsFrom(C source) {
        if (pruningPredicate.test(source)) {
            return Collections.emptySet();
        }
        return operand.fireableTransitionsFrom(source);
    }
}

class AssertFailedEvent<C> {
    C configuration;
    public AssertFailedEvent(C configuration) {
        this.configuration = configuration;
    }
}

//The AssertingTransitionRelation can be used to :
// -- verify assertions on a system, by installing it on the system with a predicate like (c) -> assertion.test(c)
// -- verify observer automata, by installing it on the observer automaton with a predicate like (c) -> c.isRejectState
//The counter-example can be then computed by the caller, by relying on ParentTransitionStorage
class AssertingTransitionRelation<C, F> extends IdentityTransitionRelation<C, F> {
    final Predicate<C> assertPredicate;
    final Announcer announcer;

    public AssertingTransitionRelation(ITransitionRelation<C, F> operand, Predicate<C> assertPredicate, Announcer announcer) {
        super(operand);
        this.assertPredicate = assertPredicate;
        this.announcer = announcer;
    }

    @Override
    public Collection<F> fireableTransitionsFrom(C source) {
        if (assertPredicate.test(source)) {
            announcer.announce(new AssertFailedEvent<>(source));
        }
        return operand.fireableTransitionsFrom(source);
    }
}

class InitializedTransitionRelation<C, F> extends IdentityTransitionRelation<C, F> {
    final Set<C> initialConfigurations;

    public InitializedTransitionRelation(ITransitionRelation<C, F> operand, Set<C> initialConfigurations) {
        super(operand);
        this.initialConfigurations = initialConfigurations;
    }

    @Override
    public Set<C> initialConfigurations() {
        return initialConfigurations;
    }
}

interface IProductAutomaton<C, F> {
    Iterable<C> initialConfigurationsIterable();

    Iterable<F> fireableTransitionsIterable(C source);

    Iterable<C> getPostIterable(C source);
}

interface IStateSpaceManager<C, F> { }

class SimpleStateSpaceManager<C, F> implements IStateSpaceManager<C, F> { }

class LazyProductAutomaton<C, F> implements IProductAutomaton<C, F> {

    final ITransitionRelation<C, F> runtime;
    final IStateSpaceManager<C, F> stateSpaceManager;
    final Supplier metadataSupplier;

    public LazyProductAutomaton(ITransitionRelation<C, F> runtime, IStateSpaceManager<C, F> stateSpaceManager, Supplier metadataSupplier) {
        this.runtime = runtime;
        this.stateSpaceManager = stateSpaceManager;
        this.metadataSupplier = metadataSupplier;
    }

    @Override
    public Iterable<C> initialConfigurationsIterable() {
        return null;
    }

    @Override
    public Iterable<F> fireableTransitionsIterable(C source) {
        return null;
    }

    @Override
    public Iterable<C> getPostIterable(C source) {
        return null;
    }
}

interface IFoldableTransition<C, F> {
    default F getAction() { return  null; }
    default boolean isFolded() { return false; }
}

class FoldedTransition<C, F> implements IFoldableTransition<C, F>, IFiredTransition<C, F> {
    public FoldedTransition(C source, Collection<C> targets) {

    }

    @Override
    public boolean isFolded() {
        return true;
    }
}

class UnfoldedTransition<C, F> implements  IFoldableTransition<C, F> {
    F fireable;
    public UnfoldedTransition(F fireable) {

    }

    @Override
    public F getAction() {
        return fireable;
    }
}

class FoldingTransitionRelation<C, F> implements ITransitionRelation<C, IFoldableTransition<C, F>> {
    final ITransitionRelation<C, F> operand;
    final Predicate<C> foldingPredicate;
    final Supplier metadataSupplier;
    final Supplier<IStateSpaceManager<C,F>> stateSpaceManagerSupplier;
    Consumer<IProductAutomaton<C, F>> consumer;

    public FoldingTransitionRelation(
            ITransitionRelation<C, F> operand,
            Predicate<C> foldingPredicate,
            Supplier<IStateSpaceManager<C, F>> stateSpaceManagerSupplier,
            Supplier metadataSupplier,
            Consumer<IProductAutomaton<C, F>> consumer) {
        this.operand = operand;
        this.foldingPredicate = foldingPredicate;
        this.stateSpaceManagerSupplier = stateSpaceManagerSupplier;
        this.consumer = consumer;
        this.metadataSupplier = metadataSupplier;
    }

    @Override
    public Set<C> initialConfigurations() {
        return null;
    }

    @Override
    public Collection<IFoldableTransition<C, F>> fireableTransitionsFrom(C source) {
        if (foldingPredicate.test(source)) {
            consumer.accept(
                    new LazyProductAutomaton<>(
                            new PrunedTransitionRelation<>(
                                    new InitializedTransitionRelation<>(
                                            operand,
                                            Collections.singleton(source)),
                                    (c) -> true),
                            stateSpaceManagerSupplier.get(),
                            metadataSupplier));


            return Collections.singletonList(new FoldedTransition<C, F>(source, Collections.emptySet()));
        }
        return operand.fireableTransitionsFrom(source).stream().map((Function<F, UnfoldedTransition<C, F>>) UnfoldedTransition::new).collect(Collectors.toList());
    }

    @Override
    public IFiredTransition<C, ?> fireOneTransition(C source, IFoldableTransition<C, F> transition) {
        if (transition.isFolded()) {
            return (FoldedTransition<C, ?>)transition;
        }
        return operand.fireOneTransition(source, transition.getAction());
    }
}

class FiacreTransitionRelation implements ITransitionRelation<Object, Object> {

    @Override
    public Set<Object> initialConfigurations() {
        return null;
    }

    @Override
    public Collection<Object> fireableTransitionsFrom(Object source) {
        return null;
    }

    @Override
    public IFiredTransition<Object, ?> fireOneTransition(Object source, Object transition) {
        return null;
    }
}

class FiacreExplorationContext {}
class FiacreLanguageModule implements ILanguageModule<File, Object, Object, FiacreExplorationContext>, IAtomicPropositionConsumer<Object, Object, Object> {

    @Override
    public FiacreExplorationContext compileProgram(File program) {
        return null;
    }

    @Override
    public Function<FiacreExplorationContext, ITransitionRelation<Object, Object>> getSemantics() {
        return null;
    }

    @Override
    public ITransitionRelation<Object, Object> getSemanticsOfAST(FiacreExplorationContext o) {
        return null;
    }

    @Override
    public ConfigurationItem getConfigurationDescription(FiacreExplorationContext ast, Object configuration) {
        return null;
    }

    @Override
    public String getFireableTransitionDescription(FiacreExplorationContext ast, Object transition) {
        return null;
    }

    @Override
    public void registerAtomicPropositionsOn(Object o, Collection<Object> propositions) {

    }

    @Override
    public Function<Object, Boolean[]> getAtomicPropositionEvaluator() {
        return null;
    }
}
class ExpressionDeclaration {}
class BuchiAutomata {}
class BuchiLanguageModule implements ILanguageModule<ExpressionDeclaration, Object, Object, BuchiAutomata>, IAtomicPropositionProvider<BuchiAutomata, Object> {

    @Override
    public BuchiAutomata compileProgram(ExpressionDeclaration program) {
        return null;
    }

    @Override
    public Function<BuchiAutomata, ITransitionRelation<Object, Object>> getSemantics() {
        return null;
    }

    @Override
    public ITransitionRelation<Object, Object> getSemanticsOfAST(BuchiAutomata o) {
        return null;
    }

    @Override
    public ConfigurationItem getConfigurationDescription(BuchiAutomata ast, Object configuration) {
        return null;
    }

    @Override
    public String getFireableTransitionDescription(BuchiAutomata ast, Object transition) {
        return null;
    }

    @Override
    public Collection<Object> getAtomicPropositions(BuchiAutomata o) {
        return null;
    }
}
class MyExplorer<C, F> {
    public MyExplorer(IProductAutomaton<C, F> productAutomaton) {}

    public static Object getMetadataSupplier() {return null;}
}

class BuchiKripkeConfiguration<kConf> {
    kConf kripke;
    Object buchi;
}
class BuchiKripkeProductTransitionRelation<C, F> implements ITransitionRelation<BuchiKripkeConfiguration, Object> {
    public BuchiKripkeProductTransitionRelation(ITransitionRelation<C, F> kripke, ITransitionRelation<Object, Object> buchi, Function<C, Boolean[]> atomicPropositionEvaluator) {

    }

    @Override
    public Set<BuchiKripkeConfiguration> initialConfigurations() {
        return null;
    }

    @Override
    public Collection<Object> fireableTransitionsFrom(BuchiKripkeConfiguration source) {
        return null;
    }

    @Override
    public IFiredTransition<BuchiKripkeConfiguration, ?> fireOneTransition(BuchiKripkeConfiguration source, Object transition) {
        return null;
    }
}
class BuchiKripkeLanguageModule<kProgram, kConf, kTrans, kAST> implements ILanguageModule<Pair<kProgram, ExpressionDeclaration>, BuchiKripkeConfiguration<kConf>, Object, Pair<kAST, BuchiAutomata>>{

    ILanguageModule<kProgram, kConf, kTrans, kAST> kripkeModule;
    BuchiLanguageModule buchiModule;

    public BuchiKripkeLanguageModule(ILanguageModule<kProgram, kConf, kTrans, kAST> kripkeModule, BuchiLanguageModule buchiModule) {
        this.kripkeModule = kripkeModule;
        this.buchiModule = buchiModule;
    }
    @Override
    public Pair<kAST, BuchiAutomata> compileProgram(Pair<kProgram, ExpressionDeclaration> program) {
        kAST kripkeAST = kripkeModule.compileProgram(program.a);
        BuchiAutomata buchiAST = buchiModule.compileProgram(program.b);
        return new Pair<>(kripkeAST, buchiAST);
    }

    @Override
    public ITransitionRelation<BuchiKripkeConfiguration<kConf>, Object> getSemanticsOfAST(Pair<kAST, BuchiAutomata> o) {
        Collection<Object> propositions = buchiModule.getAtomicPropositions(o.b);
        if (! (kripkeModule instanceof IAtomicPropositionConsumer)) {}

        IAtomicPropositionConsumer atomicPropositionConsumer = (IAtomicPropositionConsumer) kripkeModule;
        atomicPropositionConsumer.registerAtomicPropositionsOn(o.a, propositions);


        return new BuchiKripkeProductTransitionRelation<>(
                kripkeModule.getSemanticsOfAST(o.a),
                buchiModule.getSemanticsOfAST(o.b),
                atomicPropositionConsumer.getAtomicPropositionEvaluator());
    }

    @Override
    public ConfigurationItem getConfigurationDescription(Pair<kAST, BuchiAutomata> ast, BuchiKripkeConfiguration<kConf> configuration) {
        List<ConfigurationItem> items = new ArrayList<>();
        ConfigurationItem item = kripkeModule.getConfigurationDescription(ast.a, configuration.kripke);
        item.setName("kripke");
        items.add(item);
        item = buchiModule.getConfigurationDescription(ast.b, configuration.buchi);
        item.setName("buchi");
        items.add(item);
        items.add(buchiModule.getConfigurationDescription(ast.b, configuration.buchi));
        return new ConfigurationItem("c", "c", null, items);
    }

    @Override
    public String getFireableTransitionDescription(Pair<kAST, BuchiAutomata> ast, Object transition) {
        return null;
    }
}
class Test {

    public void foldingExploration() {
        //Explorer with Folding
        new MyExplorer<>(
                new LazyProductAutomaton<>(
                        new FoldingTransitionRelation<>(
                                new FiacreTransitionRelation(),
                                (c) -> true,
                                SimpleStateSpaceManager::new,
                                MyExplorer::getMetadataSupplier,
                                MyExplorer::new),
                        new SimpleStateSpaceManager<>(),
                        MyExplorer::getMetadataSupplier));

    }
    public void buchiVerificationDirect() {
        //LTL verification
        //The exchange of atomic proposition should be contextualized ?
        // probably not, the atomic proposition evaluation is a service of the module, and it might be used for debug.
        BuchiLanguageModule buchiModule = new BuchiLanguageModule();
        BuchiAutomata propertyAST = buchiModule.compileProgram(new ExpressionDeclaration());
        Collection<Object> propositions = buchiModule.getAtomicPropositions(propertyAST);

        FiacreLanguageModule kripkeModule = new FiacreLanguageModule();
        FiacreExplorationContext kripkeAST = kripkeModule.compileProgram(new File(""));
        kripkeModule.registerAtomicPropositionsOn(kripkeAST, propositions);

        new MyExplorer<>(
                new LazyProductAutomaton<>(
                        new BuchiKripkeProductTransitionRelation<>(
                                kripkeModule.getSemanticsOfAST(kripkeAST),
                                buchiModule.getSemanticsOfAST(propertyAST),
                                kripkeModule.getAtomicPropositionEvaluator()),
                        new SimpleStateSpaceManager<>(),
                        MyExplorer::getMetadataSupplier));
    }
    public void buchiVerificationUsingModule() {
        FiacreLanguageModule kripkeModule = new FiacreLanguageModule();
        BuchiLanguageModule buchiModule = new BuchiLanguageModule();
        BuchiKripkeLanguageModule<File, Object, Object, FiacreExplorationContext> buchiKripkeLanguageModule = new BuchiKripkeLanguageModule<>(kripkeModule, buchiModule);
        Pair<FiacreExplorationContext, BuchiAutomata> buchiKripkeAST = buchiKripkeLanguageModule.compileProgram(new Pair<>(new File(""), new ExpressionDeclaration()));
        ITransitionRelation<BuchiKripkeConfiguration<Object>, Object> buchiKripkeTransitionRelation = buchiKripkeLanguageModule.getSemanticsOfAST(buchiKripkeAST);

        new MyExplorer<>(
                new LazyProductAutomaton<>(
                        buchiKripkeTransitionRelation,
                        new SimpleStateSpaceManager<>(),
                        MyExplorer::getMetadataSupplier));
        Function<BuchiKripkeConfiguration<Object>, ConfigurationItem> configurationView = buchiKripkeLanguageModule.getConfigurationView(buchiKripkeAST);

    }
    public void assertVerificationExample() {
        //assert verification
        Collection<Object> assertions = null;
        FiacreLanguageModule fiacreLanguageModule = new FiacreLanguageModule();
        FiacreExplorationContext fiacreExplorationContext = fiacreLanguageModule.compileProgram(new File(""));
        fiacreLanguageModule.registerAtomicPropositionsOn(fiacreExplorationContext, assertions);

        Announcer announcer = new Announcer();
        new MyExplorer<> (
                new LazyProductAutomaton<>(
                    new AssertingTransitionRelation<>(
                            fiacreLanguageModule.getSemanticsOfAST(fiacreExplorationContext),
                            (conf) -> Arrays.stream(fiacreLanguageModule.getAtomicPropositionEvaluator().apply(conf)).anyMatch(test -> test),
                            announcer),
                        new SimpleStateSpaceManager<>(),
                        MyExplorer::getMetadataSupplier));

    }
}
