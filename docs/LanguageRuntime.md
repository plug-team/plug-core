# Language Runtime Implementation Notes

## Execution Context

### Compiled Model
The compiled model is obtained after the compilation of the input model.
Typically all references are compiled down to load instructions that 
refer to the memory address of the element referenced.

 For "persistent" constants that will be a ConstantAddress, which is an index in the
Environment.

For "persistent" variables that will be a VariableAddress, which is an index in the 
Store.

Equally the references to function parameters, local variables and block variables 
 should be replaced with loads from the stack (loads with StackAddress).

##### What does "persistent" mean?
Persistent elements are elements that should survive an atomic execution step.

##### Heap memory
We do not really have a heap for now, because we do not allow dynamic memory allocation.
For any practical purposes in the appContext of critical systems stack allocation should be enough.
The difficulties with heap come from memory reallocation, which for model-checking should be done using either:
 - a functional heap (immutable heap)
 - a versioning data structure, which differentiates different objects allocated at different point in time at the same memory address.

#### Values
##### Primitive values
Primitive values are directed stored in the related memory zone.

##### Composite values
Composite values have a static memory representation based on primitive values.
For example a Point record defined by:

    record Point is
    int x;
    int y;
    end record
    
which represent the point(x:2 y:3) Will have the following memory value :

    2 3

The address of the point value coincides with the index of the first element.

##### Pointer linked data structures
If they are statically created and do not change shape during execution we could allow that since they have a fixed memory representation.
If, however, they are dynamically created and/or change shape during execution we need a **Heap**.
In the cases of UML event-pool and message queues we can have a *exceptional* handling. 
We can have for example a **memory pocket** mechanism which is something like a separate heap for each one of these, so there is no problem with re-allocation.
A **memory pocket** is pointer to a complex data-structure from a particular memory-zone. 

### Environment
The environment encapsulates an array of "persistent" constants.
The array consists of "primitive" values. 
A constant has an address of type ConstantAddress. A ConstantAddress x means the value in the array at index x.
For composite values the ConstantAddress is the index of the first element of that value.

### Store
The store encapsulates an array of "persistent" values.
The array consists of "primitive" values.
A variable has an address of type VariableAddress. A VariableAddress x means the value in the array at index x.
For composite values the VariableAddress is the index of the first element of that value.

### Execution stack
 Typically the execution stack is a stack of values, the frames are marked using the stack registers.
SP - current stack top
FP - frame pointer

![Call stack layout](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Call_stack_layout.svg/294px-Call_stack_layout.svg.png "title")
 
 For "blocks" we have a block frame which is pushed on the stack.
 
 For simplicity however it might be easier to have Frame objects that are pushed on the stack.
 This way we do not need to handle the SP/FP indirections.
 
 Beware though that Frame objects might render complex (if not impossible) the implementation of functions in functions.
 In this case the inner function has lexically scoped visibility to its enclosing function, which means that it can reference its variables.
 If the stack is only an array of values that is not a problem because the inner function can directly reference a variable from the stack by using its StackAddress.
 If however the stack is an array of Frames then the StackAddress has two or even three levels of indirection (level 1 - frame address, level 2 - the list (parameters or locals), level 3 - index in the list)
   
   
## Interpreter
The interpreter basically walks the compiled model in the current appContext and executes an atomic step.

``` java 
    class Interpreter {
        ExecutionContext executionContext;
        public Store getStore() {
            executionContext.getStore();
        }
        public void  setStore(Store store) {
            executionContext.setStore(store);
        }
        //initializes the executionContext: 
        //computes the initial values for the constants and the variables
        public void initiliaze();
        
        //returns a list of execution possibilities.
        //- this list has only one element in the case of a deterministic language
        //- this list has one or more elements in the case of languages with non-determinism
        public List<IFireableTransition> getFireableTransitions();
        
        //fire the transition in the current execution appContext. this modifies the store
        public void fire(IFireableTransition);
    }

    class SimpleExecutionController {
        Interpreter interpreter;
        //sequential execution until the end (no more transitions to fire)
        //For languages with non-determinism this execution is non-deterministic because of **findAny**
        public void evaluateUntilEnd() {
            interpreter.initialize();

            List<IFireableTransition> fireables;
            while (!(fireables = interpreter.getFireableTransitions()).isEmpty()) {
                IFireableTransition transitionToFire = fireables.stream().findAny();
                interpreter.fire(transitionToFire);
            }
        }

        public static void main(String args[]) {
            SimpleExecutionController sEC = new SimpleExecutionController();
            sEC.interpreter = new Interpreter();
            sEC.evaluateUntilEnd();
        }
    }
```

From the exterior the Interpreter API will be called as follows (reachability example):

        
``` java
    class ReachabilityExecutionController {
        Interpreter interpreter;
        Set<Store> known;
        Queue<Store> toSee;
        
        
        public void reachability() {
            interpreter.initialize();
            Store currentStore = interpreter.getStore();
            known.add(currentStore);
            toSee.add(currentStore);
            
            while (!toSee.isEmpty()) {
                Store sourceStore = toSee.remove();
                interpreter.setStore(sourceStore);
                
                for (IFireableTransitions transitionToFire : interpreter.getFireableTransitions()) {
                    interpreter.setStore(sourceStore); //make sure that we fire from the same source store
                    interpreter.fire(transitionToFire);
                    
                    Store targetStore = interpreter.getStore(); //get the resulting store and register it
                    if (known.add(targetStore)) {
                        toSee.add(targetStore);
                    }
                }
            }
        }
    }
```

## The diagnosis module
For any given language the diagnosis module is responsible for providing the means of interacting with a running program from the outside.
The diagnosis module would enable :
- querying the program state at any time.
- displaying the program state by mapping the raw values in the environment, store and the stack to the symbol table.
- breaking encapsulation to enable seeing any values of the program even though some things might be defined as private in a module (usage of hierarchical names)
- changing values in the environment, store and stack -- for debugging : change a value and continue execution with the new value
- in a fully live-programming environment the diagnosis runtime would also be able of changing the shape of program. This might break the semantics of the language

It is very important for the diagnosis module to be independent of the interpreter and the execution appContext. This way the diagnosis module can be removed when deploying the runtime in the production environment.

The diagnosis module would ideally enable full reflection.

[Towards Structural Decomposition of Reflection with Mirrors](https://hal.inria.fr/inria-00629175/en/)

This would push for the isolation of the meta-object protocol from the base-language. Mirrors do that, however they are supported through the interpreter, how can we isolate the reflection support from the interpreter and put it in a diagnosis module?

#### Changing the shape of a program
Changing the shape of the program might imply updating : 
- the environment - adding a constant
- the store 
- the execution stack
- the symbol table
- the compiled model

The diagnosis module is composed of a number of different sub-modules :
- evaluator : Evaluate arbitrary **correct** or **diagnosis** language expressions without side-effects.
- interactors : Evaluate **correct** or **diagnosis** expressions with side effects.

### Diagnosis Evaluators
The evaluators provide a very restricted form of diagnosis, observability.
The evaluators are the most important brick for property verification.

### Diagnosis Interactors
A special case of **interactors** are the ones enabling communication between different languages. The would expose part of the symbol table, and can add means of communicating with a system outside its semantics boundaries.
Take for instance Fiacre language, in its specification it does not allow any kind of interaction from outside (except the FFI) which is too low level.
Using the diagnosis support we can have interactors that enable:
- changing an arbitrary variable from outside (IL1 - interactor level 1)
- dispatch interactions through a message-queue (connected to the exposed arguments of a process) (IL2)
- synchronize on an exposed synchronous channel from the outside (IL3)
- add an outside synchronization partner (IL4)

Briefly a level-1 interactor will enable Fiacre-CDL interactions through a clean well defined interface.

The "semantically-correct" interactors would ensure that the semantics of the changed value is respected. For instance if we want to modify a fiacre queue through the interactor we will get the "enqueue, dequeue" functions.

#### Correct vs diagnosis expressions
While the correct expressions follow the syntax and the semantics of the underlying language, the **diagnosis** expressions extend the syntax and the semantics of the language. They can break encapsulation, include support for function definition and call in languages that do not support it, etc.






